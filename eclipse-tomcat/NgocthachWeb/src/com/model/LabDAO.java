package com.model;

import java.util.ArrayList;

public class LabDAO {
public static ArrayList<Labs> getLabs(){
	ArrayList<Labs> labs = new ArrayList<Labs>();
	labs.add(new Labs(1, "Bài tập 1", "ShareFolder/exercises/1. Lab1.pdf", "#"));
	labs.add(new Labs(2, "Bài tập 2", "ShareFolder/exercises/2.-Lab2.pdf", "lab2/register.jsp"));
	labs.add(new Labs(3, "Bài tập 3", "ShareFolder/exercises/3.-Lab3.pdf", "lab3/index.jsp"));
	labs.add(new Labs(4, "Bài tập 4", "ShareFolder/exercises/4.-Lab4.pdf", "lab4/index.jsp"));

	return labs;
	
}

}
