<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="UTF-8">
<title>forgot password</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="css/loginstyle.css">
<!--===============================================================================================-->
<link
	href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200"
	rel="stylesheet">
<link
	href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css"
	rel="stylesheet">
<!--  <link href="css/aos.css" rel="stylesheet"> chua import vao -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
</head>
<body style="background-color: #666666;">
	<header>
		<jsp:include page="../menu.jsp"></jsp:include>
	</header>
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form validate-form">
					<span class="login100-form-title p-b-43"> Enter your email </span>


					<div class="wrap-input100 validate-input"
						data-validate="Valid email is required: ex@abc.xyz">
						<input class="input100" type="text" name="email"> <span
							class="focus-input100"></span> <span class="label-input100">Email</span>
					</div>

					<div class="container-login100-form-btn">
						<button class="login100-form-btn">Send code</button>
					</div>
				</form>
				<div class="login100-more"
					style="background-image: url('images/forgot.jpg');"></div>
			</div>
		</div>
	</div>

<footer class="footer">
<jsp:include page="../footer.jsp"></jsp:include>
</footer>



	<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
	<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>