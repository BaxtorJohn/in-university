<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page import="java.util.ArrayList" %>
        <%@page import="lab.model.Person" %>
            <%@page import="DAO.PersonDAO" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bài tập bootstrap</title>

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
</head>
<style>
body {
  font-family:  Arial;
  line-height: 1.5;
  font-size: 13px;
}

.searchform {
  outline: 0;
  float: left;
  -webkit-box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
  -webkit-border-radius: 4px;
  border-radius: 4px;
    margin-left: 
}

form > .textbox {
  outline: 0;
  height: 42px;
  width: 500px;
  line-height: 42px;
  padding: 0 16px;
  background-color: rgba(255, 255, 255, 0.8);
  color: #212121;
  border: 0;
  float: left;
  -webkit-border-radius: 4px 0 0 4px;
  border-radius: 4px 0 0 4px;
}

.searchform > .textbox:focus {
  outline: 0;
  background-color: #FFF;
}

.searchform > .button {
	content: "\f002"
  outline: 0;
  background: none;
  background-color: #357ebd;
  float: left;
  height: 42px;
  width: 100px;
  text-align: center;
  line-height: 42px;
  border: 0;
  color: white;
  font: normal normal normal 14px/1 FontAwesome;
  font-size: 16px;
  text-rendering: auto;
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);
  -webkit-transition: background-color .4s ease;
  transition: background-color .4s ease;
  -webkit-border-radius: 0 4px 4px 0;
  border-radius: 0 4px 4px 0;
}

.search{
	margin-left: 400px;
	margin-top: 100px;
}
a.btn:hover {
     -webkit-transform: scale(1.1);
     -moz-transform: scale(1.1);
     -o-transform: scale(1.1);
 }
 a.btn {
     -webkit-transform: scale(0.8);
     -moz-transform: scale(0.8);
     -o-transform: scale(0.8);
     -webkit-transition-duration: 0.5s;
     -moz-transition-duration: 0.5s;
     -o-transition-duration: 0.5s;
 }
 .container {
 	padding: 100px
 }
</style>
<body>
<div class="search">
<h1 style="margin-left: 100px">Quản lý nhân viên</h1>
<form method="post" class="searchform">
  <input type="text" class="textbox" placeholder="Nhập giá trị tìm kiếm ...">
  <input title="Search" value="Search" type="submit" class="button" >
  <input title="Add" value="Thêm mới" type="submit" class="button" style="margin-left: 5px">
</form>
</div>
<form action="#" method="get">
	<%
		//Tạo danh sách person - sau này sẽ lấy từ database lên
		ArrayList<Person> lstPeople = PersonDAO.getPeople();
		
	%>
	<div class="container" >
		<div class="table-responsive">
			<table class="table">
				<thead>
					<tr>
						<th>Số thứ tự</th>
						<th>Mã NV</th>
						<th>Họ tên</th>
						<th>Ngày sinh</th>
						<th>Quê quán</th>
						<th></th>
					</tr>
				</thead>
				<%
				int count = 0;
				for(Person p : lstPeople){
						count++;
				%>
				<tbody>
					<tr>
						<td><%=count%></td>
						<td><%=p.getId()%></td>
						<td><%=p.getName()%></td>
						<td><%=p.getDate()%></td>
						<td><%=p.getCountry() %></td>
						<td><a href="#" class="btn btn-primary a-btn-slide-text" style="background-color: #33ccff;">
        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>
        <a href="#" class="btn btn-primary a-btn-slide-text">
        <span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
        <a href="#" class="btn btn-primary a-btn-slide-text" style="background-color: #cc0000;">
       <span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
       </td>
					</tr>
				</tbody>
				<%
					}
				%>
			</table>
		</div>
	
</body>
</form>
</div>
</body>
</html>