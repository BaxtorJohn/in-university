<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<html lang="en-US">
<head>
<title>Đăng ký</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- register -->
<link rel="stylesheet" type="text/css" href="css/register.css">
<!--===============================================================================================-->
<link
	href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200"
	rel="stylesheet">
<link
	href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css"
	rel="stylesheet">
<!--  <link href="css/aos.css" rel="stylesheet"> chua import vao -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
<%
	Map<String, String> s = (Map<String, String>) request.getAttribute("errors");
	if (s == null) {
		s = new HashMap<>();
	}
%>

</head>
<body style="background-color: #999999;">
	<header>
		<jsp:include page="../menu.jsp"></jsp:include>
	</header>
	<form action="RegisterIndex" class="register" method="post">
		<h1>Đăng ký tài khoản </h1>
		<fieldset class="row1">
			<legend>Thông tin tài khoản</legend>
			<p>
				<label>Email * </label> <input type="text" name="email" /> 
				
				
				<label>Nhập lại email * </label> <input type="text" name="reEmail" />
				
			</p>
			
			<p style="margin-left: 150px">
				<font
					color="red"> <%
 	if (s.containsKey("email_err")) {
 %> <%=s.get("email_err").toString()%> <%
 	}
 %>
				</font>
				<span style="margin-left: 190px">
				<font color="red"> <%
 	if (s.containsKey("reEmail_err")) {
 %> <%=s.get("reEmail_err").toString()%> <%
 	}
 %>
				</font></span>
			</p>
			
			
			<p>
				<label>Mật khẩu* </label> <input type="password" name="pass" /> 
				<label>Nhập lại mật khẩu* </label> <input type="password"
					name="rePass" />  
			</p>
			
			
			<p style="margin-left: 150px">
			<font
					color="red"> <%
 	if (s.containsKey("pass_err")) {
 %> <%=s.get("pass_err").toString()%> <%
 	}
 %>
				</font> 
				<span style="margin-left: 190px">
				<font color="red"> <%
 	if (s.containsKey("rePass_err")) {
 %> <%=s.get("rePass_err").toString()%> <%
 	}
 %>
				</font>
				</span>
			</p>
		</fieldset>
		<fieldset class="row2">
			<legend>Chi tiết cá nhân </legend>
			<p>
				<label>Họ tên * </label> <input type="text" class="long" name="name" />
				
			</p>
			
			<p style="margin-left: 110px">
			<font color="red"> <%
 	if (s.containsKey("name_err")) {
 %> <%=s.get("name_err").toString()%> <%
 	}
 %>
				</font></p>
			<p>
				<label>Số điện thoại * </label> <input type="text" maxlength="10"
					name="phone" /> 
			</p>
			<p style="margin-left: 110px"><font color="red"> <%
 	if (s.containsKey("phone_err")) {
 %> <%=s.get("phone_err").toString()%> <%
 	}
 %>
				</font>
			</p>
			<p>
				<label class="optional">Đại chỉ</label> <input type="text"
					class="long" name="street" />
			</p>
			<p>
				<label>Thành phố * </label> <input type="text" class="long" name="city" />
				
			</p>
			<p style="margin-left: 110px"><font color="red"> <%
 	if (s.containsKey("city_err")) {
 %> <%=s.get("city_err").toString()%> <%
 	}
 %>
				</font>
			</p>
			<p>
				<label>Quốc gia * </label> <select>
					<option value="0">Viet Nam</option>
					<option value="1">United States</option>
					<option value="2">Chinese</option>
					<option value="3">Japan</option>
					<option value="4">Korean</option>
					<option value="5">England</option>
					<option value="6">Campuchia</option>
					<option value="7">Irak</option>
				</select>
			</p>
			<p>
				<label class="optional">Website </label> <input class="long"
					name="web" type="text" value="http://" />

			</p>
		</fieldset>
		<fieldset class="row3">
			<legend>Thêm thông tin </legend>
			<p>
				<label>Giới tính *</label> 
				<input type="radio" value="radio"
					name="gender" checked="checked"/>
					 <label class="gender">Nam</label> 
					 <input	type="radio" value="radio" name="gender" />
					  <label class="gender">Nữ</label>
			</p>
			<p>
				<label>Ngày sinh * </label><select class="date" name="date">
				<% 
				for(int i=1;i <=31;i++)
					out.print("<option value=\""+i+"\">" +i +"</option>");
					%>
				</select> <select name="month">
					<% 
				for(int i=1;i <=12;i++)
					out.print("<option value=\""+i+"\">" +i +"</option>");
					%>
				</select> <select class="year" name="year">
				<%
					for(int i=1990; i < 2010; i++)
						out.print("<option value=\""+i+"\">"+i+"</option>");
				%>
					
				</select>
			</p>

			<p>
				<label>Sinh viên  </label> <input type="checkbox" value=""
					name="student" />
			</p>
			<div class="infobox">
				<h4>Lưu ý</h4>
				<p>- Những nơi có dấu * là bắt buộc.</p>
			</div>
		</fieldset>
		<fieldset class="row4">
			<legend>Điều khoản và Gửi thư </legend>
			<p class="agreement">
				<input type="checkbox" value="" /> <label>* Tôi chấp nhận các điều khoản và điều kiện</label>
			</p>
			<p class="agreement">
				<input type="checkbox" value="" /> <label>Tôi muốn nhận
phiếu mua hàng cá nhân theo trang web của bạn</label>
			</p>
			<p class="agreement">
				<input type="checkbox" value="" /> <label>Cho phép đối tác
gửi cho tôi phiếu mua hàng cá nhân và dịch vụ liên quan</label>
			</p>
		</fieldset>
		<div>
			<button class="button">Đăng ký &raquo;</button>
		</div>
	</form>
<footer class="footer">
	<jsp:include page="../footer.jsp"></jsp:include>
</footer>
</body>
</html>