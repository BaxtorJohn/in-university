package week3;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Exercise5 {
//viet phuong trinh nhap vao date co dang 21/12/2000 va in ra dang 21 December 2000
	public static void exchange(String date) {
		String input = date.toString();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date d = new Date();
		try {
			d= sdf.parse(input);
			System.out.println(" " + d);
		}catch(ParseException ex) {
			
		}
		sdf.applyPattern("dd MMMM yyyy");
		System.out.println(sdf.format(d));
		
	}
	
	public static void main(String[] args) {
		exchange("11/2/2000") ;
	}
}
