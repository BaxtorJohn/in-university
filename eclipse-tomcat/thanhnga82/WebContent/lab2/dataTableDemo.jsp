<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.model.Person"%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Trần Thị Thanh Nga - Datatable demo</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
	<%
		//Tạo danh sách person - sau này sẽ lấy từ database lên
		ArrayList<Person> lstPeople = new ArrayList<Person>();
		lstPeople.add(new Person("Phạm Văn Tính", "09xxxxxx", "pvtinh@hcmuaf.edu.vn"));
		lstPeople.add(new Person("Lê Phi Hùng", "09xxxxxx", "hunglephi@hcmuaf.edu.vn"));
		lstPeople.add(new Person("Mai Anh Thơ", "09xxxxxx", "tho@hcmuaf.edu.vn"));
		lstPeople.add(new Person("Trần Thị Thanh Nga", "09xxxxxx", "ngattt@hcmuaf.edu.vn"));
	%>
	<div class="container">
		<h2>Table</h2>
		<div class="table-responsive">
			<table class="table">
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Phone Number</th>
						<th>Email</th>
						<th>Operation</th>
					</tr>
				</thead>
				<%
					int count = 0;
					//Hiển thị dữ liệu lên table, ở đây dùng for each
					//Trong ví dụ này thì nên dùng for vì có cột số thứ tự
					for (Person p : lstPeople) {
						count++;
				%>
				<tbody>
					<tr>
						<td><%=count%></td>
						<td><%=p.getName()%></td>
						<td><%=p.getPhoneNumber()%></td>
						<td><%=p.getEmail()%></td>
						<td><img src="images/noimage.jpg"></td>
					</tr>
				</tbody>
				<%
					}
				%>
			</table>
		</div>
	</div>
</body>
</html>