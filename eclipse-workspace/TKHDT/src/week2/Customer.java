package week2;

import java.util.List;

public abstract class Customer {
	protected static final double TAX_RATE = 0.1;
	private String id;
	private String name;
	private String address;

	public abstract List<? extends Reading> getReading();

	public abstract double charge();

	public abstract String statement();

	public Customer(String id, String name, String address) {
		super();
		this.name = name;
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	public Reading getNewReading() {
		return getReading().get(getReading().size() - 1);
	}

	public Reading getOldReading() {
		return getReading().get(getReading().size() - 2);
	}
}
