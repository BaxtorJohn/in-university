package DAO;

import java.sql.Date;

public class Product {
private int id;
private String name;
private float price;
private String discription;
private Date date;
private int category_id;
public Product(int id, String name, float price, String discription, Date date, int category_id) {
	super();
	this.id = id;
	this.name = name;
	this.price = price;
	this.discription = discription;
	this.date = date;
	this.category_id = category_id;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public float getPrice() {
	return price;
}
public void setPrice(float price) {
	this.price = price;
}
public String getDiscription() {
	return discription;
}
public void setDiscription(String discription) {
	this.discription = discription;
}
public Date getDate() {
	return date;
}
public void setDate(Date date) {
	this.date = date;
}
public int getCategory_id() {
	return category_id;
}
public void setCategory_id(int category_id) {
	this.category_id = category_id;
}
@Override
public String toString() {
	return id +"\t" + name + "\t" + price + "\t" + discription + "\t" + date + "\t" + category_id + "\n";
}

}
