package week3.duck2;

public class FlyNoWay implements FlyBehavior{

	@Override
	public void fly() {

		System.out.println("I can not fly");
	}

}
