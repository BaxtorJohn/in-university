<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Load dữ liệu từ MySql</title>
</head>
<style>
	.part1 img {
		width: 100px;
		margin-left: 50px;
		
	}
	.part2 img{
		width: 800px;
		height: auto;
		margin-left: 200px;
	}
	.part3 img {
		width: auto;
		
	}
</style>
<body>
<h1>Load dữ liệu từ MySQL</h1>
<h2>I / Tạo dữ liệu trong CSDL </h2>
<p>Khi các bạn đã kết nối CSDL MySQL với Eclipse rồi thì sau đây mh sẽ hướng dẫn các bạn cách load dữ liệu từ MySQL v2 hiện thị ra màn hình Consolve đơn giản nhất</p>
<p> Đầu tiên các bạn cần nhập dữ liệu trên MySQL, mình sẽ lấy dữ liệu mẫu để demo:
	<span class="part2"><img alt="hinhanh" src="../../images/connectmysql/13.png"></span>
	<span class="part2"><img alt="hinhanh" src="../../images/connectmysql/14.png"></span>
<h2>II / Tiến hành lấy dữ liệu</h2>
<div>
	<h4> 1/ Tạo 1 lớp Product trong pakage DAO </h4>
	<span class="part2"><img alt="hinhanh" src="../../images/connectmysql/15.png"></span>
	<p>Lưu ý : các biến của lớp Product phải tương ứng với các cột trong bảng Product trên CSDL MySQL</p>
	<p> Sử dụng getter để lấy các giá trị và toString() để hiện thị nó lên màn hình Consolve.</p>
</div>
<div>
	<h4>2/ Tạo 1 lớp QueryDataDAO để tiến hành load dữ liệu</h4>
	<span class="part2"><img alt="hinhanh" src="../../images/connectmysql/16.png">
	<p>Các đối tượng Connection, Statement, ResultSet là các đối tượng java định nghĩa sẵn để mình thao tác với CSDL nên các bạn chỉ cần nhớ và thao tác thôi nhé.</p>
	<p> Quan trọng là phần lấy dữ liệu từ các cột :</p>
	<p>+ Phần " rs.getInt(1) " sẽ tương ứng với lấy dữ liệu id kiểu int trong cột 1 của CSDL. </p>
	<p>+ Phần " rs.getStrirng(2) " sẽ tương ứng với lấy dữ liệu name kiểu String trong cột 2 của CSDL. </p>
	<p>+ Phần " rs.getFload(3) " sẽ tương ứng với lấy dữ liệu price kiểu float trong cột 3 của CSDL. </p>
	<p>+ Phần " rs.getString(4) " sẽ tương ứng với lấy dữ liệu description kiểu String trong cột 4 của CSDL. </p>
	<p>+ Phần " rs.getDate(5) " sẽ tương ứng với lấy dữ liệu date kiểu date trong cột 5 của CSDL. </p>
	<p>+ Phần " rs.getInt(6) " sẽ tương ứng với lấy dữ liệu category_id  kiểu int trong cột 6 của CSDL. </p>
	
</div>
<div>
	<h4>3/ Sau đó chúng ta viết hàm main và Test :</h4>
	<span class="part2"><img alt="hinhanh" src="../../images/connectmysql/17.png">
	
</div>

</body>
</html>