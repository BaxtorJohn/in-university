package week3;

public class Exercise2 {
	// viet phuong thuc tach so bat ky
	public String separate(int num) {
		String result = "";
		String tmp = Integer.toString(num);
		int count = 0;
		for (int i = tmp.length() - 1; i >= 0; i--) {
			result = tmp.charAt(i) + result;
			count++;
			if (count == 3) {
				if (i > 0)
					result = "," + result;

				count = 0;
			}
		}
		return result;
	}

	// referrence viet phuong thuc tach so ra khoi chuoi
	public static void saparateIntToString(String chuoi) {
		String tmp = chuoi.replaceAll("[^0-9,-\\.]", ",");
		String[] item = tmp.split(",");
		for (int i = 0; i < item.length; i++) {
			try {
				Double.parseDouble(item[i]);
				System.out.println(item[i]);
			} catch (NumberFormatException e) {

			}
		}
	}

	public static void main(String[] args) {
		Exercise2 e2 = new Exercise2();
		int num = 932736;
		System.out.println("test phuong thuc tach so nguyen bat ky: " + e2.separate(num));

		// test referrence
		String str = "-1.223 %^& fsf 0.234 56.65 fsf 9 f";
		e2.saparateIntToString(str);

	}

}
