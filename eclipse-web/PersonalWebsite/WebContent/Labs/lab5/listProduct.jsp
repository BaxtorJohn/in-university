<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="lab.model.ProductDAO"%>
<%@page import="lab.model.Product"%>
<%@page import="com.model.Menu"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Danh sách sản phẩm</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<style>
img {
	width: 70px;
	height: auto
}

h1 {
	text-align: center;
}

td .chooseImage {
	border-radius: 5px;
	background-color: black;
	padding: 5px 10px;
	color: white;
	text-decoration: none;
}
</style>
<body>
	<h1>Danh sách các loại Laptop bán chạy nhất quý 1 năm 2018</h1>
	<form action="<%=request.getContextPath()%>/EditProduct" method="get">
		<div class="container">
			<%
		
				ArrayList<Product> listP = ProductDAO.getListProduct();
			%>
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Số thứ tự</th>
						<th>Mã sản phẩm</th>
						<th>Tên sản phấm</th>
						<th>Hình ảnh</th>
						<th>Giá</th>
						<th></th>
					</tr>
				</thead>
				<%
					for (int i = 0; i < listP.size(); i++) {
				%>
				<tbody>
					<tr>
						<td><%=i%></td>
						<td><%=listP.get(i).getId()%></td>
						<td><%=listP.get(i).getName()%>
						<td><img alt="hình ảnh" src="<%=listP.get(i).getImage()%>"></td>
						<td><%=listP.get(i).getPrice()%></td>
						<td><button>
								<a href="<%=request.getContextPath()%>/EditProduct?index=<%=i%>">Xóa</a>
							</button></td>
					</tr>
				</tbody>
				<%
					}%>
			</table>
	</form>
	 <div>
		<a href="#" onclick="myFunction('Demo1')"
			class="w3-button w3-block w3-black w3-left-align">+ Thêm sản phẩm</a>
		<div id="Demo1" class="w3-hide w3-animate-zoom">
			<form action="<%=request.getContextPath()%>/EditProduct"
				method="post">
				<div class="container">

					<table class="table table-hover">
						<%
							ArrayList<Product> listInsertP = ProductDAO.getListProduct();
						%>
						<thead>
							<tr>
								<th>Số thứ tự</th>
								<th>Mã sản phẩm</th>
								<th>Tên sản phấm</th>
								<th>Hình ảnh</th>
								<th>Giá</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<%
								for (int i = 0; i < 10; i++) {
							%>

							<tr>
								<td><%=i%></td>
								<td><input type="text" name="codeP"
									placeholder="nhập mã sản phẩm"></td>
								<td><input type="text" name="nameP"
									placeholder="nhập tên sản phẩm"></td>
								<td><a href="#" class="chooseImage">Chọn hình<img
										alt="" src="#" name="img"></td>
								<td><input type="text" name="price"
									placeholder="nhập mã sản phẩm"></td>
								<td><input type="submit" name="insert" value="Thêm"></td>
							</tr>
						</tbody>
						<%
							}
						%>
					</table>

				</div>
			</form>
		</div>
	</div>
	<!-- scripts drop accordions -->
	<script>
		function myFunction(id) {
			var x = document.getElementById(id);
			if (x.className.indexOf("w3-show") == -1) {
				x.className += " w3-show";
			} else {
				x.className = x.className.replace(" w3-show", "");
			}
		}
	</script>
	</div>
</body>
</html>