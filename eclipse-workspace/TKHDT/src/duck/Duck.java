package duck;

public abstract class Duck {
	protected QuackBehavior quackBehavior;
	protected FlyBehavior flyBehavior;
	public void performQuack() {
		quackBehavior.quack();
		}
		public void performFly() {
		flyBehavior.fly();
		}
}
