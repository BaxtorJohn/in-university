package DAO;

import java.util.ArrayList;
import java.util.Set;

import model.Product;

public interface ProductDAO {
public ArrayList<Product> getListProduct();

public ArrayList<Product> getListProductByCategory(String sub_category_id);

public Product getProduct(String product_id);

//tim kiem theo product_id
public Set<String> getProductByName();//list P with name
public ArrayList<Product> filterProductByName(String name); //filter P by name

//them , sua , xoa san pham
public Product addProduct(Product product);
public Product editProduct(Product product, String product_id);
public boolean deleteProduct(String product_id);

}
