<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.model.Menu"%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Menu</title>

<link
	href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200"
	rel="stylesheet">
<link
	href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css"
	rel="stylesheet">
<link href="css/aos.css" rel="stylesheet">
<link href="<%=request.getContextPath()%>/css/bootstrap.min.css" rel="stylesheet">
<link href="<%=request.getContextPath()%>/styles/main.css" rel="stylesheet">

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="profile-page sidebar-collapse">
		<nav
			class="navbar navbar-expand-lg fixed-top navbar-transparent bg-primary"
			color-on-scroll="400">
		<div class="container">
			<div class="navbar-translate">
				<a class="navbar-brand" href="<%=request.getContextPath() %>/index.jsp" rel="tooltip">Thạch</a>
				<button class="navbar-toggler navbar-toggler" type="button"
					data-toggle="collapse" data-target="#navigation"
					aria-controls="navigation" aria-expanded="false"
					aria-label="Toggle navigation">
					<span class="navbar-toggler-bar bar1"></span><span
						class="navbar-toggler-bar bar2"></span><span
						class="navbar-toggler-bar bar3"></span>
				</button>
			</div>
			<div class="collapse navbar-collapse justify-content-end"
				id="navigation">
				<%
					ArrayList<Menu> listMN = new ArrayList<Menu>();
					listMN.add(new Menu("home", "index.jsp"));
					listMN.add(new Menu("about", "about.jsp"));
					listMN.add(new Menu("GT Người-Máy","GTNM/hci.jsp"));
					listMN.add(new Menu("LT Web","LTW/ltw.jsp"));
					listMN.add(new Menu("PTTK HTTT","PTTKHTTT/pttk.jsp"));
					listMN.add(new Menu("Lập trình mạng","LTM/ltm.jsp"));
					
					//tao menu dong
					for (int i = 0; i < listMN.size(); i++) {
						Menu mnu = listMN.get(i);
				%>

				<ul class="navbar-nav" >
					<li class="nav-item"><a href="<%=request.getContextPath() %>/<%=mnu.getLink()%>"
						class="nav-link smooth-scroll"><%=mnu.getName()%></a></li>
					<%
						}
					%>
				</ul>
				
					
						
			</div>
		</div>
		</nav>
	</div>
</body>
</html>