package week1;


public class Lecturer {
	private String name;
	private Date dayOfBir;
	private String degree;
	public Lecturer(String name, Date dayOfBir, String degree) {
		super();
		this.name = name;
		this.dayOfBir = dayOfBir;
		this.degree = degree;
	}
	public String getName() {
		return name;
	}
	public Date getDayOfBir() {
		return dayOfBir;
	}
	public String getDegree() {
		return degree;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setDayOfBir(Date dayOfBir) {
		this.dayOfBir = dayOfBir;
	}
	public void setDegree(String degree) {
		this.degree = degree;
	}
	@Override
	public String toString() {
		return getDegree() + " " + getName();
	}
	
	
}
