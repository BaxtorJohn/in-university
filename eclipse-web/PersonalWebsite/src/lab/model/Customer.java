package lab.model;

import java.util.ArrayList;

public class Customer {
	private String user;
	private String name;
	ArrayList<Product> shoppingCart;

	public Customer(String user, String name, ArrayList<Product> shoppingCart) {
		super();
		this.user = user;
		this.name = name;
		this.shoppingCart = new ArrayList<Product>();
	}

	public void addShoppingCart(Product a) {
		this.shoppingCart.add(a);

	}

	public ArrayList<Product> getShoppingCart() {
		return shoppingCart;
	}

}
