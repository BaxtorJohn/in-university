<%@page import="DAO.ProductDAOImpl"%>
<%@page import="DAO.CategoryDAOImpl"%>
<%@page import="model.Category" %>
<%@page import="model.Account" %>
<%@page import="java.util.Set" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>menu</title>

<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js"></script>

<script>

// Safari reports success of list attribute, so doing ghetto detection instead
yepnope({
  test : (!Modernizr.input.list || (parseInt($.browser.version) > 400)),
  yep : [
      'https://raw2.github.com/CSS-Tricks/Relevant-Dropdowns/master/js/jquery.relevant-dropdown.js',
      'https://raw2.github.com/CSS-Tricks/Relevant-Dropdowns/master/js/load-fallbacks.js'
  ]
});
</script>

</head>
<body>
<%
	CategoryDAOImpl categoryDAO = new CategoryDAOImpl();
		 
%>
<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="index.html"><img src="images/home/logo.png" alt="" /></a>
						</div>
						<div class="btn-group pull-right">
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
									USA
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#">Canada</a></li>
									<li><a href="#">UK</a></li>
								</ul>
							</div>
							
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
									DOLLAR
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#">Canadian Dollar</a></li>
									<li><a href="#">Pound</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<li><a href="CheckLogin?command=logout"><i class="fa fa-user"></i><%=session.getAttribute("username")%></a></li>
								<li><a href="#"><i class="fa fa-star"></i> Wishlist</a></li>
								<li><a href="checkout.jsp"><i class="fa fa-crosshairs"></i> Checkout</a></li>
								<li><a href="cart.jsp"><i class="fa fa-shopping-cart"></i> Cart</a></li>
								<li><a href="login.jsp"><i class="fa fa-lock"></i> Login</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
		<div class="header-bottom"><!--header-bottom-->
		
		

			<div class="container">
				<div class="row">
					<div class="col-sm-9">
					
				<%
	for(Category categoryFarther : categoryDAO.getCategoryFarther()){
%>
					
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li class="dropdown"><a href="#"><%=categoryFarther.getName() %><i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                    <%
											for(Category categoryChild : categoryDAO.getCategoryChild(categoryFarther.getCategory_id())){
										%>
                                        <li><a href="index.jsp?category_id=<%=categoryChild.getCategory_id()%>"><%=categoryChild.getName() %></a></li>
										<%} %>
                                    </ul>
                                </li> 
							</ul>
						</div>
						<%} %>
					</div>
					
					<form action="SearchServlet" method="post">
					<div class="col-sm-3">
						<div class="search_box pull-right">
							<input type="text" placeholder="Tìm kiếm" list="searchProduct" name="search"/>
						</div>
					</div>
							<input type="submit" value="Tìm" style="position: absolute; color: white; background-color:orange; border: 0 none; height: 35px ">					
							</form>

				</div>
			</div>
		</div><!--/header-bottom-->
</body>
</html>