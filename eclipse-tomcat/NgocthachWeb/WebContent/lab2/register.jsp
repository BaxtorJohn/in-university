<!DOCTYPE html>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="thach.servlet.RegisterIndex" %>
<html lang="en-US">
<head>
<title>register</title>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><meta name="viewport" content="width=device-width, initial-scale=1">
	
<!-- register -->
<link rel="stylesheet" type="text/css" href="css/register.css">
<%
	Map<String, String> s = (Map<String, String>) request.getAttribute("errors");
	if (s == null) {
		s = new HashMap<>();
	}
%>

</head>
<body style="background-color: #999999;">
	<header>
<jsp:include page="../menu.jsp"></jsp:include>
	</header>
	<form action="../RegisterIndex" class="register" method="post">
		<h1 style="font-size:50px">Registration</h1>
		<fieldset class="row1">
			<legend>Account Details </legend>
			<p>
				<label>Email * </label> <input type="text" name="email" /> 
				
				
				<label>Repeat email * </label> <input type="text" name="reEmail" />
				
			</p>
			
			<p style="margin-left: 150px">
				<font
					color="red"> <%
 	if (s.containsKey("email_err")) {
 %> <%=s.get("email_err").toString()%> <%
 	}
 %>
				</font>
				<span style="margin-left: 190px">
				<font color="red"> <%
 	if (s.containsKey("reEmail_err")) {
 %> <%=s.get("reEmail_err").toString()%> <%
 	}
 %>
				</font></span>
			</p>
			
			
			<p>
				<label>Password* </label> <input type="password" name="pass" /> 
				<label>Repeat Password* </label> <input type="password"
					name="rePass" />  
			</p>
			
			
			<p style="margin-left: 150px">
			<font
					color="red"> <%
 	if (s.containsKey("pass_err")) {
 %> <%=s.get("pass_err").toString()%> <%
 	}
 %>
				</font> 
				<span style="margin-left: 190px">
				<font color="red"> <%
 	if (s.containsKey("rePass_err")) {
 %> <%=s.get("rePass_err").toString()%> <%
 	}
 %>
				</font>
				</span>
			</p>
		</fieldset>
		<fieldset class="row2">
			<legend>Personal Details </legend>
			<p>
				<label>Name * </label> <input type="text" class="long" name="name" />
				
			</p>
			
			<p style="margin-left: 110px">
			<font color="red"> <%
 	if (s.containsKey("name_err")) {
 %> <%=s.get("name_err").toString()%> <%
 	}
 %>
				</font></p>
			<p>
				<label>Phone * </label> <input type="text" maxlength="10"
					name="phone" /> 
			</p>
			<p style="margin-left: 110px"><font color="red"> <%
 	if (s.containsKey("phone_err")) {
 %> <%=s.get("phone_err").toString()%> <%
 	}
 %>
				</font>
			</p>
			<p>
				<label class="optional">Street </label> <input type="text"
					class="long" name="street" />
			</p>
			<p>
				<label>City * </label> <input type="text" class="long" name="city" />
				
			</p>
			<p style="margin-left: 110px"><font color="red"> <%
 	if (s.containsKey("city_err")) {
 %> <%=s.get("city_err").toString()%> <%
 	}
 %>
				</font>
			</p>
			<p>
				<label>Country * </label> <select>
					<option value="0">Viet Nam</option>
					<option value="1">United States</option>
					<option value="2">Chinese</option>
					<option value="3">Japan</option>
					<option value="4">Korean</option>
					<option value="5">England</option>
					<option value="6">Campuchia</option>
					<option value="7">Irak</option>
				</select>
			</p>
			<p>
				<label class="optional">Website </label> <input class="long"
					name="web" type="text" value="http://" />

			</p>
		</fieldset>
		<fieldset class="row3">
			<legend>Further Information </legend>
			<p>
				<label>Gender *</label> 
				<input type="radio" value="radio"
					name="gender" checked="checked"/>
					 <label class="gender">Male</label> 
					 <input	type="radio" value="radio" name="gender" />
					  <label class="gender">Female</label>
			</p>
			<p>
				<label>Birthdate * </label> <select class="date" name="date">
				<% 
				for(int i=1;i <=31;i++)
					out.print("<option value=\""+i+"\">" +i +"</option>");
					%>
				</select>
					 <select name="month">
					<option value="1">January</option>
					<option value="2">February</option>
					<option value="3">March</option>
					<option value="4">April</option>
					<option value="5">May</option>
					<option value="6">June</option>
					<option value="7">July</option>
					<option value="8">August</option>
					<option value="9">September</option>
					<option value="10">October</option>
					<option value="11">November</option>
					<option value="12">December</option>
				</select> <select class="year" name="year">
				<%
					for(int i=1990; i < 2010; i++)
						out.print("<option value=\""+i+"\">"+i+"</option>");
				%>
					
				</select>
			</p>

			<p>
				<label>Student * </label> <input type="checkbox" value=""
					name="student" />
			</p>
			<div class="infobox">
				<h4>Helpful Information</h4>
				<p>Here comes some explaining text, sed ut perspiciatis unde
					omnis iste natus error sit voluptatem accusantium doloremque
					laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
					veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
			</div>
		</fieldset>
		<fieldset class="row4">
			<legend>Terms and Mailing </legend>
			<p class="agreement">
				<input type="checkbox" value="" /> <label>* I accept the <a
					href="#">Terms and Conditions</a></label>
			</p>
			<p class="agreement">
				<input type="checkbox" value="" /> <label>I want to receive
					personalized offers by your site</label>
			</p>
			<p class="agreement">
				<input type="checkbox" value="" /> <label>Allow partners to
					send me personalized offers and related services</label>
			</p>
		</fieldset>
		<div>
			<button class="button">Register &raquo;</button>
		</div>
	</form>
	
<div class="">
	<jsp:include page="../footer.jsp"></jsp:include>
</div>
</body>
</html>