package lab.model;

public class Product {
private String id;
private String name;
private String image;
private long price;
private String gia;
public Product( String id, String name, String image, long price) {
	super();
	this.id = id;
	this.name = name;
	this.image = image;
	this.price = price;
}
public Product( String id, String name, String image, String gia) {
	super();
	this.id = id;
	this.name = name;
	this.image = image;
	this.gia = gia;
}
public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getImage() {
	return image;
}
public void setImage(String image) {
	this.image = image;
}
public long getPrice() {
	return price;
}
public void setPrice(long price) {
	this.price = price;
}



}
