package week1;

public class Exercise3 {
	// phuong thuc cong 2 mang so nguyen bat ky, co do dai bat ky
	public int[] arrayPlus(int[] arr1, int[] arr2) {
		int[] sum = new int[arr1.length];
		if (arr1.length != arr2.length)
			return null;
		else {
			for (int i = 0; i < arr1.length; i++) {
				sum[i] = arr1[i] + arr2[i];
				System.out.print(sum[i] + " ");
			}
		}
		return sum;
	}

	public static void main(String[] args) {
		Exercise3 e3 = new Exercise3();
		int[] arr1 = { 1, 2, 3, 10, 11 };
		int[] arr2 = { 4, 5, 6, 8, 9 };
		System.out.println("sum two array is : ");
		e3.arrayPlus(arr1, arr2);
		System.out.println();

		int[] arr3 = new int[0];
		int[] arr4 = new int[0];
		System.out.println("sum two array is : ");
		e3.arrayPlus(arr3, arr4);
	}
}
