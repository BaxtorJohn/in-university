package com.model;

public class Sanpham {
private String maSP;
private String tenSP;
private String hinhAnh;
private String gia;
public Sanpham( String maSP, String tenSP, String hinhAnh, String gia) {
	super();
	this.maSP = maSP;
	this.tenSP = tenSP;
	this.hinhAnh = hinhAnh;
	this.gia = gia;
}

public String getMaSP() {
	return maSP;
}
public String getTenSP() {
	return tenSP;
}
public String getHinhAnh() {
	return hinhAnh;
}
public String getGia() {
	return gia;
}


}
