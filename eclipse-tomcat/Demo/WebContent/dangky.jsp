<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%
	Map<String, String> s = (HashMap<String, String>) request.getAttribute("errors");
	if (s == null) {
		s = new HashMap();
	}
%>
</head>
<body>
	<form action="XuLyFormDangKy" method="post">
		<h1>Thong tin dang nhap</h1>
		<div>
			*Email: <input type="text" name="email"> <font color="red">
				<%
					if (s.containsKey("email_err")) {
				%> <%=s.get("email_err").toString()%> <%
 	}
 %>
			</font>
		</div>
		<div>
			*Mat khau: <input type="password" name="password"> <font
				color="red"> <%
 	if (s.containsKey("pass_err")) {
 %> <%=s.get("pass_err").toString()%> <%
 	}
 %>
			</font>
		</div>
		<div>
			*Nhap lai mat khau: <input type="password" name="passwordAgaint">
			<font color="red"> <%
 	if (s.containsKey("passwordAgaint_err")) {
 %> <%=s.get("passwordAgaint_err").toString()%> <%
 	}
 %>


			</font> <font color="red"> <%
 	if (s.containsKey("repass_err")) {
 %> <%=s.get("repass_err").toString()%> <%
 	}
 %>


			</font>
		</div>
		<h1>Tong tin ca nhan</h1>

		<div>
			*Ho va ten: <input type="text" name="hoten"> <font
				color="red"> <%
 	if (s.containsKey("name_err")) {
 %> <%=s.get("name_err").toString()%> <%
 	}
 %>
			</font>
		</div>
		<div>
			Phai: <input type="radio" name="phai" value="nam" checked="checked">Nam
			<span> <input type="radio" name="phai" value="nu">Nu
			</span>
			
		</div>
		<div>
			Ngay sinh: <select name="ngay">
				<%
					for (int i = 1; i < 32; i++)
						out.print("<option value=\"" + i + "\">" + i + "</option>");
				%>
			</select> <select name="thang">
				<%
					for (int i = 1; i < 13; i++)
						out.print("<option value=\"" + i + "\">" + i + "</option>");
				%>
			</select> <select name="nam">
				<%
					for (int i = 1970; i < 2000; i++)
						out.print("<option value=\"" + i + "\">" + i + "</option>");
				%>
			</select>
		</div>
		<div>
			*So dien thoai: <input type="text" name="sdt"> <font
				color="red"> <%
 	if (s.containsKey("phoneNums_err")) {
 %> <%=s.get("phoneNums_err").toString()%> <%
 	}
 %>
			</font> <font color="red"> <%
 	if (s.containsKey("phoneInt_err")) {
 %> <%=s.get("phoneInt_err").toString()%> <%
 	}
 %>
			</font>
		</div>
		<div>
			So dien thoai di dong: <input type="text" name="dtdd">
		</div>

		<h1>Dia chi</h1>
		<div>
			Quoc gia: <select name="quocgia">
				<%
					String[] quocgia = { "Viet Nam", "Trung Quoc", "Campuchia", "Lao", "Han Quoc", "Nhat Ban" };
					for (String qg : quocgia)
						out.print("<option value=\"" + qg + "\">" + qg + "</option>");
				%>
			</select>
			<div>
				Tinh thanh: <select name="tinhthanh">
					<%
						String[] tinhthanh = { "TPHCM", "Dong Nai", "Binh Duong", "Can Tho", "Ha Noi", "Da Nang" };
						for (String tt : tinhthanh)
							out.print("<option value=\"" + tt + "\">" + tt + "</option>");
					%>
				</select>
			</div>
			<div>
				Quan: <select name="quan">
					<%
						String[] quan = { "1", "2", "3", "4", "Thu Duc", "Tan Binh" };
						for (String q : quan)
							out.print("<option value=\"" + q + "\">" + q + "</option>");
					%>
				</select>
			</div>
			<div>
				Dia chi nha: <input type="textarea" name="diachi">
			</div>
			<div>
				<input type="submit" value="Dang ky"> <span><input
					type="submit" value="Xoa form"> <pan>
			</div>
	</form>
</body>
</html>