package model;

public class DetailBill {
	private String detail_id;
	private Bill bill;
	private Product product;
	private int quantum;
	private float price;
	private float sales;

	public DetailBill(String detail_id, Bill bill, Product product, int quantum, float price, float sales) {
		super();
		this.detail_id = detail_id;
		this.bill = bill;
		this.product = product;
		this.quantum = quantum;
		this.price = price;
		this.sales = sales;
	}

	public String getDetail_id() {
		return detail_id;
	}

	public void setDetail_id(String detail_id) {
		this.detail_id = detail_id;
	}

	public Bill getBill() {
		return bill;
	}

	public void setBill(Bill bill) {
		this.bill = bill;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getQuantum() {
		return quantum;
	}

	public void setQuantum(int quantum) {
		this.quantum = quantum;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public float getSales() {
		return sales;
	}

	public void setSales(float sales) {
		this.sales = sales;
	}

}
