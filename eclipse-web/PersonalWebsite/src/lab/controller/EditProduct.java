package lab.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lab.model.Product;
import lab.model.ProductDAO;

/**
 * Servlet implementation class EditProduct
 */
@WebServlet("/EditProduct")
public class EditProduct extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditProduct() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			int index = Integer.parseInt(request.getParameter("index"));
			
			//delete
			ProductDAO product = new ProductDAO();
			product.removeToCart(index);
			response.sendRedirect("Labs/lab5/listProduct.jsp");
		} catch (Exception e) {
			}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("codeP");
		String nameP = request.getParameter("nameP");
		String img = request.getParameter("img");
		String price = request.getParameter("price");
		Product product = new Product(id, nameP, img, price);
		try {
			ProductDAO dao = new ProductDAO();
			dao.insertToCart(product);
			response.sendRedirect("Labs/lab5/listProduct.jsp");
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
