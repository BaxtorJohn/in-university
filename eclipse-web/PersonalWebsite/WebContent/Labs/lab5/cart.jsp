<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page import="lab.model.Customer" %>
    <%@page import="lab.model.Product" %>
    <%@page import="java.util.ArrayList" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Giỏ hàng</title>
</head>
<body>
<h1>Giỏ hàng của quý khách</h1>
<%
	Customer customer =null;
	if(session.getAttribute("customer") == null){
		response.sendRedirect("Labs/lab5/login.jsp");
	}else {
		 customer = (Customer) session.getAttribute("customer");
		 ArrayList<Product> shoppingCart = customer.getShoppingCart(); 
	}

%>

<div class="container">
<table class="table table-hover">
<thead>
					<tr>
						<th>Số thứ tự</th>
						<th>Mã sản phẩm</th>
						<th>Tên sản phấm</th>
						<th>Hình ảnh</th>
						<th>Giá</th>
						<th></th>
					</tr>
				</thead>
				
				<%
					if(customer !=null){
						ArrayList<Product> shoppingCart = customer.getShoppingCart();
						for(int i =0; i < shoppingCart.size(); i ++){
						%>	
			<tbody>
					<tr>
						<td><%=i%></td>
						<td><%=shoppingCart.get(i).getId()%></td>
						<td><%=shoppingCart.get(i).getName()%>
						<td><img alt="hình ảnh" src="<%=shoppingCart.get(i).getImage()%>"></td>
						<td><%=shoppingCart.get(i).getPrice()%></td>
						<td><button>
								<a href="<%=request.getContextPath()%>/EditProduct?index=<%=i%>">Xóa</a>
							</button></td>
					</tr>
				</tbody>				
					<%		
						}
					}
				%>
</body>
</html>