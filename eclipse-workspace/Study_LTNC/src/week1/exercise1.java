package week1;

import java.util.Scanner;

public class exercise1 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		// enter the coordinate
		System.out.println("enter a coordinates of conner: ");
		double x = sc.nextInt();
		double radian;

		// transfrom to radian
		System.out.printf("transform %s to radian is:" ,x);
		System.out.print((x * 3.14 / 180)
				+ "radian" );
		System.out.println();
		radian = Math.toRadians(x);// else
		System.out.println("transform : " + radian);
		System.out.println();

		// sin
		double sin;
		sin = Math.sin(radian);
		System.out.println("sin: " + sin);

		// cos
		System.out.println("cos of x is: " + Math.cos(radian));

		// tan
		System.out.println("tan of x is : " + Math.tan(radian));

		// cot
		System.out.println("cotang of x is: " + 1 / (Math.tan(radian)));
		
	}
}
