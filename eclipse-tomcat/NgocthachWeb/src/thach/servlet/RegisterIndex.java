package thach.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import thach.model.CheckRegister;

/**
 * Servlet implementation class RegisterIndex
 */
@WebServlet("/RegisterIndex")
public class RegisterIndex extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterIndex() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String email = request.getParameter("email");
		String reEmail = request.getParameter("reEmail");
		String pass = request.getParameter("pass");
		String rePass = request.getParameter("rePass");
		String name = request.getParameter("name");
		String phone= request.getParameter("phone");
		String street = request.getParameter("street");
		String city = request.getParameter("city");
		String web = request.getParameter("web");
		String gender = request.getParameter("gender");
		String day = request.getParameter("date");
		String month = request.getParameter("thang");
		String year = request.getParameter("nam");
		String country = request.getParameter("country");
		String student = request.getParameter("student");
		
		CheckRegister checkRegis = new CheckRegister(email, reEmail, pass, rePass, name, phone, street, city, country, web, gender, day, month, year, student);
		if(!checkRegis.checkError()) {
			request.setAttribute("errors", checkRegis.getErrors());
			RequestDispatcher rd = getServletContext().getRequestDispatcher("/lab2/register.jsp");
			rd.forward(request, response);
			
		}else {
			
			HttpSession session = request.getSession();
			session.setAttribute("CheckRegister", checkRegis);
			response.sendRedirect("lab2/registerSuccess.jsp");
		}
	}

}
