package com.model.dao;

import java.util.ArrayList;

import com.model.Example;

public class ExampleDAO {
	public static ArrayList<Example> getExampleList() {
		ArrayList<Example> lstExamples = new ArrayList<Example>();
		lstExamples.add(new Example("Cách tạo menu động - Phần 1", "https://drive.google.com/file/d/1vMdBkV4IYrLZ6n7I9Adyja05-UFFN29I/view?usp=sharing"));
		lstExamples.add(new Example("Cách tạo menu động - Phần 2", "https://drive.google.com/file/d/1vCVOibU66WmCH-VyPquk5k2IvKZDvUbU/view?usp=sharing"));
		lstExamples.add(new Example("Sử dụng datatable của bootstrap", "https://drive.google.com/file/d/1vExCgRoL7fYgo6VWlvoASkte9K7nZAFm/view?usp=sharing"));
		lstExamples.add(new Example("...loading...", "#"));
		lstExamples.add(new Example("...loading...", "#"));

		return lstExamples;
	}
}
