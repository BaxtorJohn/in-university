package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAO.ProductDAOImpl;
import model.Product;

/**
 * Servlet implementation class EditProduct
 */
@WebServlet("/EditProduct")
public class EditProduct extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final ProductDAOImpl productDAO = new ProductDAOImpl();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EditProduct() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");

		String command = request.getParameter("command");
		String product_id = request.getParameter("product_id");
		String category_id = request.getParameter("category_id");
		String name = request.getParameter("name");
		String images = request.getParameter("images");
		String price = request.getParameter("price");

	
		String url = "";
		try {
			switch (command) {
			case "add":
				productDAO.addProduct(
						new Product(product_id, name, images, Float.parseFloat(price), Integer.parseInt(category_id)));
				url = "/admin.jsp";
				break;
			case "edit":
				productDAO.editProduct(
						new Product(product_id, name, images, Float.parseFloat(price), Integer.parseInt(category_id)),
						product_id);
				url = "/admin.jsp";
				break;
			case "delete":
				productDAO.deleteProduct(product_id);
				url = "/admin.jsp";
				break;
			default:
				break;
			}

			RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(url);
			requestDispatcher.forward(request, response);
		} catch (Exception e) {
			url = "/error.jsp";
		}
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(url);
		requestDispatcher.forward(request, response);
	}

}
