package week3;

public class Exercise7 {
	// ma hoa
	public static String maHoa(String cbd, byte pass) {
		String ketQua = " ";
		byte tmp = Byte.parseByte(cbd);
		byte i = (byte) (tmp ^ pass);
		ketQua = Byte.toString(i);
		return ketQua;
	}

	// giai ma
	public static String giaiMa(String chuoiMH, byte key) {
		return maHoa(chuoiMH, key);
	}

	// referece
	public static String stringMH(String cbd, String pass) {
		String ketQua = " ";
		char[] chuoi = cbd.toCharArray(); // doi ve char
		char[] key = pass.toCharArray();// doi ve char
		for (int i = 0, j = 0; i < chuoi.length; i++, j++) {
			// for (int j = 0; j < key.length; j++) {
			if (j == key.length - 1) {
				j = 0;
			}

			char tmp = (char) (chuoi[i] ^ key[j]);
			ketQua = ketQua + tmp;

		}

		return ketQua;

	}//giai ma referrence
	public static String stringGM(String chuoi,String key) {
		return stringMH(chuoi, key);
	}
	

	public static void main(String[] args) {
//		 chuoi > 255 thi chay ko dc
		 String cbd = "123";
		 byte a = 1;
		 System.out.println(maHoa(cbd, a));
		
		 // test giai ma
		 byte key = 1;
		 System.out.println("giai ma: " + giaiMa(maHoa(cbd, a), key));

		// test referrence
		String chuoi = "i am a student";
		String keyy = "17";
		System.out.println("test referrence: " + stringMH(chuoi, keyy));
	
		//test giai ma referrence
		System.out.println("giai ma referrence: " + stringGM(stringMH(chuoi, keyy), keyy));
	}
}