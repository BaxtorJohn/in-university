<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Insert title here</title>
</head>
<body>
<h1>Sequence diagram</h1>
<p>1. Lượt đồ sequence login </p>
<img alt="sequence login" src="<%=request.getContextPath() %>/images/seq-login.PNG">
<p>2. Lượt đồ sequence Check in  </p>
<img alt="sequence check in" src="<%=request.getContextPath() %>/images/seq-checkin.PNG">
<p>3. Lượt đồ sequence Check in  </p>
<img alt="sequence check in" src="<%=request.getContextPath() %>/images/seq-checkin.PNG">
<p>4. Lượt đồ sequence chat with customer </p>
<img alt="sequence chat with customer" src="<%=request.getContextPath() %>/images/seq-chat.PNG">
<p>5. Lượt đồ sequence filter user </p>
<img alt="sequence filter user" src="<%=request.getContextPath() %>/images/seq-filter.PNG">
<p>6. Lượt đồ sequence find user </p>
<img alt="sequence find user" src="<%=request.getContextPath() %>/images/seq-find.PNG">
<p>7. Lượt đồ sequence create user </p>
<img alt="sequence create user" src="<%=request.getContextPath() %>/images/seq-create.PNG">
<p>8. Lượt đồ sequence edit user </p>
<img alt="sequence edit user" src="<%=request.getContextPath() %>/images/seq-edit.PNG">
<p>9. Lượt đồ sequence delete user </p>
<img alt="sequence delete user" src="<%=request.getContextPath() %>/images/seq-delete.PNG">
<p>10. Lượt đồ sequence register course </p>
<img alt="sequence register course" src="<%=request.getContextPath() %>/images/seq-register.PNG">
<p>11. Lượt đồ sequence shopping online </p>
<img alt="sequence shopping online" src="<%=request.getContextPath() %>/images/seq-shopping.PNG">
<p>12. Lượt đồ sequence load schedule </p>
<img alt="sequence load shedule" src="<%=request.getContextPath() %>/images/seq-schedule.PNG">
<p>13. Lượt đồ sequence feedback </p>
<img alt="sequence feedback" src="<%=request.getContextPath() %>/images/seq-feedback.PNG">
<p>14. Lượt đồ sequence recieve notification </p>
<img alt="sequence recieve notification" src="<%=request.getContextPath() %>/images/seq-recievenoti.PNG">
<p>15. Lượt đồ sequence send notification </p>
<img alt="sequence send notification" src="<%=request.getContextPath() %>/images/seq-sendnoti.PNG">

</body>
<style>
p {
margin-top: 20px;}
</style>
</html>