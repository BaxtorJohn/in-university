package week1;

public class Register {
	private Course course;
	private float grade;
	public Register(Course course) {
		super();
		this.course = course;
	}
	public Course getCourse() {
		return course;
	}
	public float getGrade() {
		return grade;
	}
	public void setCourse(Course course) {
		this.course = course;
	}
	public void setGrade(float grade) {
		this.grade = grade;
	}
	
}
