package week4;

import java.util.Stack;

public class Exercise4 {
	// phuong trinh chuyen doi co so 10 - > 2
	public static void translateTwo(int n) {
		Stack<Integer> stack = new Stack<>();
		while (n != 0) {
			stack.push(n % 2);
			n = n / 2;
		}
		while (!(stack.isEmpty())) {
			System.out.print(stack.pop());
		}
	}

	// phuong thuc chuyen doi co so 2 -> 10
//	public static void translateTen(int n) {
//		int result = 1;
//		String m = String.valueOf(n);
//		for (int i = m.length(); i > 0; i--) {
//			if(m.length()==1) {
//				result =1;
//			}
//			result =  result + translateTen(result*2);
//		}
//		System.out.println(result);
//	}

	public static void main(String[] args) {
		System.out.println("Translate co so 10 -> 2: ");
		translateTwo(1142535);
	}
}
