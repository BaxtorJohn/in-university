package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import model.Category;

public class CategoryDAOImpl implements CategoryDAO{

	@Override
	public ArrayList<Category> getCategoryFarther() {
		Connection connection = (Connection) DatabaseConnection.getConnection();
		String sql = "SELECT * FROM CATEGORY ";
		
		ArrayList<Category> list = new ArrayList<Category>();
		try {
			PreparedStatement ps = (PreparedStatement) connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				Category category = new Category();
				category.setCategory_id(rs.getInt("ID"));
				category.setName(rs.getString("NAME"));
				list.add(category);
			}
			connection.close();
		} catch (Exception e) {
//			Logger.getLogger(CategoryDAOImpl.class.getName(), null).log(Level.SEVERE,null,e);
			System.out.println("Error");
			e.printStackTrace();
		}
		
		return list;
	}

	@Override
	public ArrayList<Category> getCategoryChild(int category_id) {
		Connection connection = (Connection) DatabaseConnection.getConnection();
		String sql = "SELECT * FROM SUB_CATEGORY WHERE ID = '" + category_id +"'";
		
		ArrayList<Category> list = new ArrayList<Category>();
		try {
			PreparedStatement ps = (PreparedStatement) connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				Category category = new Category();
				category.setCategory_id(rs.getInt("SUB_ID"));
				category.setName(rs.getString("NAME"));
				category.setCategory_farther(rs.getInt("ID"));
				list.add(category);
			}
			connection.close();
		} catch (Exception e) {
//			Logger.getLogger(CategoryDAOImpl.class.getName(), null).log(Level.SEVERE,null,e);
			System.out.println("Error");
			e.printStackTrace();
		}
		
		return list;
	}

	public static void main(String[] args) {
		CategoryDAOImpl dao = new CategoryDAOImpl();
		System.out.println(dao.getCategoryFarther().size());
		System.out.println(dao.getCategoryChild(200).size());
	}
}
