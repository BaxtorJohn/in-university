package week4;

import java.util.ArrayList;
import java.util.Queue;

public class Exercise5 {
//viet phuong thuc in ra day so fibocaci thu n
	
	//phuong phap khong dung de quy
	public static int fibocaci(int n) {
		int f0=0;
		int f1 =1;
		int fn=1;
		if(n < 0) {
			return -1;
		}else if( n ==0 || n== 1) {
			return n;
		}else {
			for (int i =2 ; i < n; i++) {
				f0=f1;
				
				f1 = fn;
				fn = f0 + f1;
			}
		}
		return fn;
		
	}
	
	//phuong thu in day so fibocaci dung de quy
	public static int recursiveFibocaci(int n ) {
		
		if(n < 0) {
			return -1;
		}else if(n ==0 || n==1){
			return n;
		}else {
			return fibocaci(n-1) + fibocaci(n-2);
		}
	}
	
	public static void main(String[] args) {
		//test dibocaci
		System.out.println("day so fibocaci theo logic: ");
		for(int i = 0 ; i < 7;i++){
			System.out.print(fibocaci(i) + " ");
			
			
		}
		
		//test dung de quqy
		System.out.println();
		System.out.println("Day so fibocaci theo de quy: ");
		for(int i =0; i < 20; i ++) {
		System.out.print(recursiveFibocaci(i) + " ");
	}
		}
	
}
