package thach.model;

import java.util.HashMap;
import java.util.Map;

public class CheckLogin {
	private String email;
	private String pass;

	Map<String, String> errors;

	public CheckLogin(String email, String pass) {
		super();
		this.email = email;
		this.pass = pass;
		errors = new HashMap<>();
		checkEmail();
		checkPass();
		checkErrors();
		checkUser();

	}

	public String getEmail() {
		return email;
	}

	public String getPass() {
		return pass;
	}

	public Map<String, String> getErrors() {
		return errors;
	}

	// check email not null
	public void checkEmail() {
		if ((email == null) || (email == "")) {
			errors.put("email_err", "Please enter email !");
		}else if(!email.equals("admin")) {
			errors.put("email_inconrrect", "email inconrrect!");
		}
	}

	// check pass not null
	public void checkPass() {
		if ((pass == null) || (pass == "")) {
			errors.put("pass_err", "Please enter password !");
		}else if(!pass.equals("admin")) {
			errors.put("pass_incorrect", "Password inconrrect!");
		}
	}

	// check xem co loi khong?
	public boolean checkErrors() {
		return this.errors.isEmpty();
	}

	// check tai khoan dang nhap
	public void checkUser() {
		if (email.equals("admin") && pass.equals("admin")) {

		}

	}
}
