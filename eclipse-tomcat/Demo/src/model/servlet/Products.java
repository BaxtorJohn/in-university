package model.servlet;

import java.util.ArrayList;

public class Products {
	private int nums;
private int id;
private String name;
private int price;
public Products(int nums, int id, String name, int price) {
	super();
	this.nums = nums;
	this.id = id;
	this.name = name;
	this.price = price;
}
public int getNums() {
	return nums;
}
public int getId() {
	return id;
}
public String getName() {
	return name;
}
public int getPrice() {
	return price;
}

}
