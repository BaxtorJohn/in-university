package week1;

public class Exercise2 {
	// Xac dinh so chu so cua 1 so nguyen bat ky
	public int countDigit(int num) {
		int count = 0;
		if (num == 0)
			return count = 1;
		else {
			while (num != 0) {
				num = num / 10;
				count++;
			}
			return count;
		}
	}

	public static void main(String[] args) {
		Exercise2 e2 = new Exercise2();
		System.out.println("so chu so cua 2913 la : " + e2.countDigit(2913));
		System.out.println("so chu so cua 873613 la: " + e2.countDigit(873613));
		System.out.println("so chu so cua 0 la: " + e2.countDigit(0));
		System.out.println("so chu so cua -21124 la: " + e2.countDigit(-21124));

	}
}
