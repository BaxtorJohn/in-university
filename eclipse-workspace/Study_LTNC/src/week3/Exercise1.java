package week3;

public class Exercise1 {
//viet phuong thuc ma hoa 1 so nguyen bang phuong thuc XOR
	public String intMH(String num,int pass) {
		String result="";
		int tmp = Integer.parseInt(num);
		int i = (Integer) (tmp ^ pass);
		result = Integer.toString(i);
		return result;
	}
	//giai ma
	public String giaimaintMH(String range,int key) {
//		String result="";
//		int tmp = Integer.parseInt(range);
//		result= intMH(tmp, key);
//		result=result.toString();
//		return result;
		return intMH(range, key);
	}
	
	//viet phuong thuc ma hoa 1 chuoi String bang thuat toan XOR
	public String stringMH(String range,String pass) {
		String result = "";
		char[] a = range.toCharArray();
		char[] b = pass.toCharArray();
		for(int i=0,j=0; i < a.length;i++,j++) {
			if(j== b.length-1) {
				j=0;
			}
				char tmp = (char) (a[i] ^ b[j]);
				result = result + tmp;
			
			
		}
		return result;
	}
	//giaima
	public String giaimaStringMH(String stringRange, String key) {
		return stringMH(stringRange, key);
	}
	public static void main(String[] args) {
		Exercise1 e1 = new Exercise1();
		//test xor 1 so nguyen
		String num = "3273821";
		int pass = 23;
		System.out.println("test xor 1 so nguyen: " + e1.intMH(num,pass));
		System.out.println("test giai ma 1 so nguyen: "+e1.giaimaintMH(e1.intMH(num,pass), 23));
		
		//test xor 1 chuoi
		String range = "nguyen ngoc thach";
		String passs = "han";
		System.out.println("test xor 1 chuoi: "+e1.stringMH(range, passs));
		System.out.println("Giai ma 1 chuoi: "+e1.giaimaStringMH(e1.stringMH(range, passs), passs));
		
	}
}
