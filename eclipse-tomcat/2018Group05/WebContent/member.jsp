<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<head>

<meta charset="utf-8" />
<title>Member</title>
<meta name="description" content="" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
<!--Favicon -->
<link rel="icon" type="image/png" href="images/favicon.jpg" />

<!-- CSS Files -->
<link rel="stylesheet" href="css/reset.css" />
<link rel="stylesheet" href="css/animate.min.css" />
<link rel="stylesheet" href="css/bootstrap.min.css" />
<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="css/font-awesome.css" />
<link rel="stylesheet" href="css/owl.carousel.css" />
<link rel="stylesheet" href="css/responsive.css" />
<link rel="stylesheet" href="css/player/YTPlayer.css" />
<link href="css/pro-bars.css" rel="stylesheet" />

<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
	$(function() {
		$("#datepicker").datepicker();
	});
</script>
<!-- End CSS Files -->

</head>

<body>
	<!-- Navigation -->
	<section id="navigation" class="dark-nav">
		<!-- Navigation Inner -->
		<div class="nav-inner">
			<!-- Site Logo -->
			<div class="site-logo fancy">
				<a href="#" id="logo-text" class="scroll logo">Fitness </a>
			</div>
			<!-- End Site Logo -->
			<a class="mini-nav-button gray2"><i class="fa fa-bars"></i></a>
			<!-- Navigation Menu -->
			<div class="nav-menu">
				<ul class="nav uppercase">
					<li><a href="#home" class="scroll">Home</a></li>
					<li><a href="#about" class="scroll">About us</a></li>
					<li><a href="#features" class="scroll">Features</a></li>
					<li><a href="#clients" class="scroll">Gymnasts</a></li>
					<li><a href="#testimonial" class="scroll">Testimonial</a></li>
					<li><a href="#contact" class="scroll">Contact</a></li>
				</ul>
			</div>
			<!-- End Navigation Menu -->
		</div>
		<!-- End Navigation Inner -->
	</section>
	<!-- End Navigation Section -->


	<!-- Home Section -->
	<section id="home" class="relative">
		<div id="slides">
			<div class="slides-container relative">
				<!-- Slider Images -->
				<div class="image2"></div>
				<div class="image1"></div>
				<div class="image3"></div>
				<div class="image4"></div>
				<!-- End Slider Images -->
			</div>
			<!-- Slider Controls -->
			<nav class="slides-navigation">
				<a href="#" class="next"></a> <a href="#" class="prev"></a>
			</nav>
		</div>
		<!-- End Home Slides -->
		<div class="v2 absolute">
			<!-- Auto Typocraphic Texts -->
			<div class="typographic">
				<!-- Your Logo -->
				<div class="logo">
					<img src="images/logo-icon.png" width="200" alt="Logo" />
				</div>
				<%
					if (session.getAttribute("username") == null) {
						response.sendRedirect("index.jsp");
					}
				%>
				<h2 class=" condensed uppercase no-padding no-margin bold gray1">Welcome
					to Fitness</h2>
				<form action="member.jsp" method="get">
					<h3
						class="condensed uppercase no-padding no-margin bold colored slabtextdone">
						<table class="center"
							style="background-color: black; border: 5px solid grey; margin-left: auto; margin-right: auto; text-align: center; font-family: monospace, Serif; font-style: italic;">
							<tbody>
							</tbody>
						</table>
				</form>

			</div>
			<!--End Auto Typocraphic Texts -->
		</div>
		<!-- End V2 area -->
	</section>
	<!-- End Home Section -->
	<!-- Footer -->
	<footer id="footer" class="footer">
		<!-- Your Company Name -->
		<img src="images/logo-icon.png" width="200" alt="Logo" />
		<!-- Copyright -->
		<p class="copyright normal">
			© 2014 <span class="colored">Fitness.</span> All Rights Reserved.
		</p>
	</footer>
	<!-- End Footer -->

	<!-- JS Files -->


	<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/jquery.appear.js"></script>
	<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
	<script type="text/javascript" src="js/modernizr-latest.js"></script>
	<script type="text/javascript" src="js/SmoothScroll.js"></script>
	<script type="text/javascript" src="js/jquery.parallax-1.1.3.js"></script>
	<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="js/jquery.superslides.js"></script>
	<script type="text/javascript" src="js/jquery.flexslider.js"></script>
	<script type="text/javascript" src="js/jquery.mb.YTPlayer.js"></script>
	<script type="text/javascript" src="js/jquery.fitvids.js"></script>
	<script type="text/javascript" src="js/jquery.slabtext.js"></script>
	<script type="text/javascript" src="js/plugins.js"></script>


</body>

</html>