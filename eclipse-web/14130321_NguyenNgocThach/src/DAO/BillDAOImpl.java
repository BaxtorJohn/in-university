package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;

import model.Bill;

public class BillDAOImpl implements BillDAO{

	@Override
	public void addBill(Bill bill) {
		Connection connection = DatabaseConnection.getConnection();
		String sql = "INSERT INTO BILL VALUES (?,?,?,?,?,?)";
		
		try {
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setString(1, bill.getBill_id());
			ps.setString(2, bill.getAccount().getAccount_id());
			ps.setString(3, bill.getAddress_order());
			ps.setString(4, bill.getPayment_methods());
			ps.setTimestamp(5, bill.getDate_buy());
			ps.setInt(6, bill.getState_order());
			
			ps.executeUpdate();
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
}
