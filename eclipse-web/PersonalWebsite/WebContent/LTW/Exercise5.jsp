<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description"
	content="Creative CV is a HTML resume template for professionals. Built with Bootstrap 4, Now UI Kit and FontAwesome, this modern and responsive design template is perfect to showcase your portfolio, skils and experience." />
<title>Exercise5</title>
</head>

<body>
	<header><jsp:include page="../menu.jsp"></jsp:include></header>
		<div style="margin: 100px 0 0 100px; text-align: center;">
	
	<h1>Bài tập tuần 5</h1>
	
	<div style="margin-top: 50px">
	<a href="Labs/lab5/listProduct.jsp" target="_blank"><button class="btn btn-warning"> Danh sách sản phẩm</button></a>
	<a href="Labs/lab5/product.jsp" target="_blank"><button class="btn btn-warning"> Bài tập click mua hàng</button></a>
	<a href="Labs/lab5/EXbootstrap.jsp" target="_blank"><button class="btn btn-warning"> Bài tập bootstrap</button></a>
	<a href="Labs/lab5/EXbootstrap2.jsp" target="_blank"><button class="btn btn-warning"> Bài tập bootstrap 2</button></a>
	</div></div>

</body>
</html>