/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: Apache Tomcat/9.0.6
 * Generated at: 2018-04-09 00:51:33 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class about_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent,
                 org.apache.jasper.runtime.JspSourceImports {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  private static final java.util.Set<java.lang.String> _jspx_imports_packages;

  private static final java.util.Set<java.lang.String> _jspx_imports_classes;

  static {
    _jspx_imports_packages = new java.util.HashSet<>();
    _jspx_imports_packages.add("javax.servlet");
    _jspx_imports_packages.add("javax.servlet.http");
    _jspx_imports_packages.add("javax.servlet.jsp");
    _jspx_imports_classes = null;
  }

  private volatile javax.el.ExpressionFactory _el_expressionfactory;
  private volatile org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public java.util.Set<java.lang.String> getPackageImports() {
    return _jspx_imports_packages;
  }

  public java.util.Set<java.lang.String> getClassImports() {
    return _jspx_imports_classes;
  }

  public javax.el.ExpressionFactory _jsp_getExpressionFactory() {
    if (_el_expressionfactory == null) {
      synchronized (this) {
        if (_el_expressionfactory == null) {
          _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
        }
      }
    }
    return _el_expressionfactory;
  }

  public org.apache.tomcat.InstanceManager _jsp_getInstanceManager() {
    if (_jsp_instancemanager == null) {
      synchronized (this) {
        if (_jsp_instancemanager == null) {
          _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
        }
      }
    }
    return _jsp_instancemanager;
  }

  public void _jspInit() {
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
      throws java.io.IOException, javax.servlet.ServletException {

    if (!javax.servlet.DispatcherType.ERROR.equals(request.getDispatcherType())) {
      final java.lang.String _jspx_method = request.getMethod();
      if ("OPTIONS".equals(_jspx_method)) {
        response.setHeader("Allow","GET, HEAD, POST, OPTIONS");
        return;
      }
      if (!"GET".equals(_jspx_method) && !"POST".equals(_jspx_method) && !"HEAD".equals(_jspx_method)) {
        response.setHeader("Allow","GET, HEAD, POST, OPTIONS");
        response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "JSPs only permit GET, POST or HEAD. Jasper also permits OPTIONS");
        return;
      }
    }

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n");
      out.write("<html class=\"no-js\">\n");
      out.write("<!--<![endif]-->\n");
      out.write("<head>\n");
      out.write("<meta charset=\"utf-8\">\n");
      out.write("<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">\n");
      out.write("<title>about me</title>\n");
      out.write("<meta name=\"description\" content=\"\">\n");
      out.write("<meta name=\"viewport\" content=\"width=device-width\">\n");
      out.write("<link rel='shortcut icon' type='image/x-icon' href='favicon.ico' />\n");
      out.write("<link rel=\"apple-touch-icon-precomposed\"\n");
      out.write("\thref=\"apple-touch-icon-precomposed.png\">\n");
      out.write("<link rel=\"apple-touch-icon-precomposed\"\n");
      out.write("\thref=\"apple-touch-icon-72x72-precomposed.png\">\n");
      out.write("<link rel=\"apple-touch-icon-precomposed\"\n");
      out.write("\thref=\"apple-touch-icon-114x114-precomposed.png\">\n");
      out.write("<link rel=\"apple-touch-icon-precomposed\"\n");
      out.write("\thref=\"apple-touch-icon-144x144-precomposed.png\">\n");
      out.write("<link\n");
      out.write("\thref=\"//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css\"\n");
      out.write("\trel=\"stylesheet\">\n");
      out.write("\n");
      out.write("<link rel=\"stylesheet\" href=\"css/cv.normalize.min.css\">\n");
      out.write("<link rel=\"stylesheet\" href=\"css/cv.css\">\n");
      out.write("<script\n");
      out.write("\tsrc=\"//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js\"></script>\n");
      out.write("<script>\n");
      out.write("\twindow.jQuery\n");
      out.write("\t\t\t|| document\n");
      out.write("\t\t\t\t\t.write('<script src=\"js/vendor/jquery-1.10.1.min.js\"><\\/script>')\n");
      out.write("</script>\n");
      out.write("<script src=\"js/vendor/jquery.hashchange.min.js\"></script>\n");
      out.write("<script src=\"js/vendor/jquery.easytabs.min.js\"></script>\n");
      out.write("\n");
      out.write("<script src=\"js/main.js\"></script>\n");
      out.write("\n");
      out.write("<!--[if lt IE 9]>\n");
      out.write("      <script src=\"//html5shiv.googlecode.com/svn/trunk/html5.js\"></script>\n");
      out.write("      <script>window.html5 || document.write('<script src=\"js/vendor/html5shiv.js\"><\\/script>')</script>\n");
      out.write("      <![endif]-->\n");
      out.write("\n");
      out.write("<link href=\"css/aos.css\" rel=\"stylesheet\">\n");
      out.write("<link href=\"css/bootstrap.min.css\" rel=\"stylesheet\">\n");
      out.write("<link href=\"css/main.css\" rel=\"stylesheet\">\n");
      out.write("</head>\n");
      out.write("<body class=\"bg-fixed bg-1\">\n");
      out.write("\t<!--[if lt IE 7]>\n");
      out.write("    <p class=\"chromeframe\">You are using an <strong>outdated</strong> browser. Please <a href=\"http://browsehappy.com/\">upgrade your browser</a> or <a href=\"http://www.google.com/chromeframe/?redirect=true\">activate Google Chrome Frame</a> to improve your experience.</p>\n");
      out.write("    <![endif]-->\n");
      out.write("\t<div class=\"main-container\">\n");
      out.write("\t\t<div class=\"main wrapper clearfix\">\n");
      out.write("\t\t\t<!-- Header Start -->\n");
      out.write("\t\t\t<header> ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "menu.jsp", out, false);
      out.write(" </header>\n");
      out.write("\t\t\t<header id=\"header\">\n");
      out.write("\t\t\t<div id=\"logo\">\n");
      out.write("\t\t\t\t<h2>nguyen ngoc thach</h2>\n");
      out.write("\t\t\t\t<h4>Animator / Photographer</h4>\n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t\t</header>\n");
      out.write("\t\t\t<!-- Header End -->\n");
      out.write("\t\t\t<!-- Main Tab Container -->\n");
      out.write("\t\t\t<div id=\"tab-container\" class=\"tab-container\">\n");
      out.write("\n");
      out.write("\t\t\t\t<div id=\"tab-data-wrap\">\n");
      out.write("\t\t\t\t\t<!-- About Tab Data -->\n");
      out.write("\t\t\t\t\t<div id=\"about\">\n");
      out.write("\t\t\t\t\t\t<section class=\"clearfix\">\n");
      out.write("\t\t\t\t\t\t<div class=\"g2\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"photo\">\n");
      out.write("\t\t\t\t\t\t\t\t<img src=\"images/avatar.jpg\" alt=\"nguyen ngoc thach\">\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<div class=\"info\">\n");
      out.write("\t\t\t\t\t\t\t\t<h2>Nguyễn Ngọc Thạch</h2>\n");
      out.write("\t\t\t\t\t\t\t\t<p>\n");
      out.write("\t\t\t\t\t\t\t\t\t\"Cuộc sống vốn dĩ không công bằng. Hãy tập làm quen với điều\n");
      out.write("\t\t\t\t\t\t\t\t\tđó.\" - Bill Gates <br> \"Chạy thật nhanh và phá vỡ tất cả.\n");
      out.write("\t\t\t\t\t\t\t\t\tNếu bạn chưa phá vỡ đựơc gì chứng tỏ bạn chạy chưa đủ nhanh.\" -\n");
      out.write("\t\t\t\t\t\t\t\t\tMark Zuckerberg<br> \"Người thành công là người bình thường\n");
      out.write("\t\t\t\t\t\t\t\t\tlàm những việc bình thường 1 cách thường xuyên đều đặn\" - do\n");
      out.write("\t\t\t\t\t\t\t\t\tnot remember<br> \"Nếu bạn muốn đi nhanh hãy dđi 1 mình.\n");
      out.write("\t\t\t\t\t\t\t\t\tNếu bạn muốn đi xa hãy đi với nhiều người.\" - not remember <br>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t\t\t\t\"Tôi làm chủ cuộc đời tôi: D\" - Nguyễn Ngọc Thạch <br>\n");
      out.write("\t\t\t\t\t\t\t\t</p>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<div class=\"g1\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"main-links sidebar\">\n");
      out.write("\t\t\t\t\t\t\t\t<h3>Tìm hiểu về tôi</h3>\n");
      out.write("\t\t\t\t\t\t\t\t<ul class=\"no-list work\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<h5>Sinh nhật:</h5> <span>07/01/1996</span>\n");
      out.write("\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<h5>Giớ tính:</h5> <span>Nam</span>\n");
      out.write("\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<h5>Số điện thoại:</h5> <span>0981669705</span>\n");
      out.write("\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<div class=\"break\"></div>\n");
      out.write("\t\t\t\t\t\t<div class=\"contact-info\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"g1\">\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"item-box clearfix\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<i class=\"icon-envelope\"></i>\n");
      out.write("\t\t\t\t\t\t\t\t\t<div class=\"item-data\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<h3>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<a href=\"mailto:sunny@mrova.com\">14130321@st.hcmuaf.edu.vn</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</h3>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<p>Địa chỉ email</p>\n");
      out.write("\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<div class=\"g1\">\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"item-box clearfix\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<i class=\"icon-facebook\"></i>\n");
      out.write("\t\t\t\t\t\t\t\t\t<div class=\"item-data\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<h3>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<a href=\"http://fb.me/luthra.sunny\">nguyenngocthach.Jade/</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</h3>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<p>Facebook</p>\n");
      out.write("\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<div class=\"g1\">\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"item-box clearfix\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<i class=\"icon-twitter\"></i>\n");
      out.write("\t\t\t\t\t\t\t\t\t<div class=\"item-data\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<h3>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<a href=\"http://twitter.com/mRovaSolutions\">@nguyenngocthach</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</h3>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<p>Twitter</p>\n");
      out.write("\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t</section>\n");
      out.write("\t\t\t\t\t\t<!-- content -->\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t<!-- End About Tab Data -->\n");
      out.write("\t\t\t\t\t<!-- Resume Tab Data -->\n");
      out.write("\t\t\t\t\t<div id=\"resume\">\n");
      out.write("\t\t\t\t\t\t<section class=\"clearfix\">\n");
      out.write("\t\t\t\t\t\t<h3>Tiêu chí nghề nghiệp</h3>\n");
      out.write("\t\t\t\t\t\t<p>Tận dụng những kiến thức về Công nghệ thông tin để mang đến\n");
      out.write("\t\t\t\t\t\t\tdịch vụ với sự tiện ích tốt nhất cho khách hàng.</p>\n");
      out.write("\t\t\t\t\t\t<p>Đồng thời phát triển tất cả các khả năng khác như: kinh\n");
      out.write("\t\t\t\t\t\t\tdoanh, kế toán, bán hàng, tương tác xã hội, ... để kiếm được\n");
      out.write("\t\t\t\t\t\t\tnhiều tiền để cài đặt cuộc sống tốt hơn và tốt hơn.</p>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t<div class=\"g2\">\n");
      out.write("\t\t\t\t\t\t\t<h3>Kinh nghiệp làm việc</h3>\n");
      out.write("\t\t\t\t\t\t\t<ul class=\"no-list work\">\n");
      out.write("\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t<h5>Developer</h5> <span class=\"label label-info\">2015-2018</span>\n");
      out.write("\t\t\t\t\t\t\t\t\t<p>Lập trình phần mềm, ứng dụng.</p>\n");
      out.write("\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t<h5>Web Programmer</h5> <span class=\"label label-info\">2017-2018</span>\n");
      out.write("\t\t\t\t\t\t\t\t\t<p>Thiết kế và xây dựng website bán hàng, website cá nhân,\n");
      out.write("\t\t\t\t\t\t\t\t\t\twebsite doanh nghiệp, mạng xã hội..</p>\n");
      out.write("\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t<h5>Kinh doanh</h5> <span class=\"label label-info\">2014-2018</span>\n");
      out.write("\t\t\t\t\t\t\t\t\t<p>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t- Sales<br> - Thuyết trình <br> - Đàm phán<br>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t- Khảo sát thị trường<br> - Thiết lập kế hoạch, mục tiêu\n");
      out.write("\t\t\t\t\t\t\t\t\t\tcho công việc.\n");
      out.write("\t\t\t\t\t\t\t\t\t</p>\n");
      out.write("\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t<h5>Kỹ năng xã hội</h5> <span class=\"label label-danger\">2010-now</span>\n");
      out.write("\t\t\t\t\t\t\t\t\t<p>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t- Có khả năng thích nghi với nhiều môi trường.<br> - Xây\n");
      out.write("\t\t\t\t\t\t\t\t\t\tdựng mối quan hệ.<br> - Learn to upgrade yourself.<br>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t- Tìm cách giải quyết vấn đề.<br> - Tổ chức kế hoạch cho\n");
      out.write("\t\t\t\t\t\t\t\t\t\tcuộc sống cũng như công việc. Đảm bảo hoàn thành trên 80%.\n");
      out.write("\t\t\t\t\t\t\t\t\t</p>\n");
      out.write("\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t\t\t<h3>Học vấn</h3>\n");
      out.write("\t\t\t\t\t\t\t<ul class=\"no-list work\">\n");
      out.write("\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t<h5>Đại học Nông Lâm TPHCM</h5> <span\n");
      out.write("\t\t\t\t\t\t\t\t\tclass=\"label label-success\">2014-2018</span>\n");
      out.write("\t\t\t\t\t\t\t\t\t<p>Học Công nghệ thông tin tại trường dda9i5 học Nông Lâm.</p>\n");
      out.write("\t\t\t\t\t\t\t\t</li>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<div class=\"g1\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"sidebar\">\n");
      out.write("\t\t\t\t\t\t\t\t<h3>Skills</h3>\n");
      out.write("\t\t\t\t\t\t\t\t<h5>Software</h5>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"meter asbestos\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<span style=\"width: 90%\"><span>Linux</span></span>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"meter pomengrate\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<span style=\"width: 90%\"><span>Microsoft Office</span></span>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"meter emerald\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<span style=\"width: 33.3%\"><span>Photoshop</span></span>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"meter carrot\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<span style=\"width: 70%\"><span>Database</span></span>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"meter wisteria\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<span style=\"width: 70%\"><span>Website</span></span>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"meter sunflower\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<span style=\"width: 40%\"><span>Firework</span></span>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"meter midnight\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<span style=\"width: 70%\"><span>Dreamweaver</span></span>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"meter midnight\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<span style=\"width: 73%\"><span>Manage data</span></span>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"break\"></div>\n");
      out.write("\t\t\t\t\t\t\t\t<h5>Design</h5>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"meter emerald\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<span style=\"width: 53.3%\"><span>User Interface</span></span>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"meter carrot\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<span style=\"width: 90%\"><span>Typography</span></span>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"meter wisteria\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<span style=\"width: 60%\"><span>Web Applications</span></span>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"break\"></div>\n");
      out.write("\t\t\t\t\t\t\t\t<h5>Programming Language</h5>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"meter carrot\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<span style=\"width: 70%\"><span>Java</span></span>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"meter emerald\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<span style=\"width: 73.3%\"><span>HTML/CSS</span></span>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"meter carrot\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<span style=\"width: 70%\"><span>JSP/Servlet</span></span>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"meter carrot\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<span style=\"width: 50%\"><span>PHP</span></span>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"meter wisteria\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<span style=\"width: 95%\"><span>jQuery/JavaScript</span></span>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"meter sunflower\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<span style=\"width: 75%\"><span>Ruby</span></span>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"meter asbestos\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<span style=\"width: 82%\"><span>Python</span></span>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t</section>\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t<!-- End Resume Tab Data -->\n");
      out.write("\t\t\t\t\t<!-- Portfolio Tab Data -->\n");
      out.write("\t\t\t\t\t<h2>Video do tôi tự sáng tác</h2>\n");
      out.write("\t\t\t\t\t<div id=\"portfolio\">\n");
      out.write("\t\t\t\t\t\t<section class=\"clearfix\">\n");
      out.write("\t\t\t\t\t\t<div class=\"g1\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"image\">\n");
      out.write("\t\t\t\t\t\t\t\t<img src=\"images/me.jpg\" alt=\"\">\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"image-overlay\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<div class=\"image-link\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<a href=\"https://www.youtube.com/watch?v=i322LJ5aDzU\"\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\tclass=\"btn\">Full View</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<div class=\"g1\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"image\">\n");
      out.write("\t\t\t\t\t\t\t\t<img src=\"images/han.jpg\" alt=\"\">\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"image-overlay\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<div class=\"image-link\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<a href=\"https://www.youtube.com/watch?v=weAmxtsCcfY&t=5s\"\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\tclass=\"btn\">Full View</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<div class=\"g1\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"image\">\n");
      out.write("\t\t\t\t\t\t\t\t<img src=\"images/danang.jpg\" alt=\"\">\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"image-overlay\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<div class=\"image-link\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<a href=\"https://www.youtube.com/watch?v=uQo3Qvp622E\"\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\tclass=\"btn\">Full View</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<div class=\"break\"></div>\n");
      out.write("\t\t\t\t\t\t<div class=\"g1\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"image\">\n");
      out.write("\t\t\t\t\t\t\t\t<img src=\"images/vungtau.png\" alt=\"\">\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"image-overlay\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<div class=\"image-link\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<a href=\"https://www.youtube.com/watch?v=z6JapiaLTFc&t=118s\"\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\tclass=\"btn\">Full View</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<div class=\"g1\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"image\">\n");
      out.write("\t\t\t\t\t\t\t\t<img src=\"images/sqlmap.png\" alt=\"\">\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"image-overlay\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<div class=\"image-link\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<a href=\"https://www.youtube.com/watch?v=Fuyz8O03Un0\"\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\tclass=\"btn\">Full View</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<div class=\"g1\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"image\">\n");
      out.write("\t\t\t\t\t\t\t\t<img src=\"images/sql_injection.jpg\" alt=\"\">\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"image-overlay\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<div class=\"image-link\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<a href=\"https://www.youtube.com/watch?v=hk4vZDrvtYk\"\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\tclass=\"btn\">Full View</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t</section>\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t<!-- End Portfolio Data -->\n");
      out.write("\t\t\t\t\t<!-- Contact Tab Data -->\n");
      out.write("\t\t\t\t\t<div id=\"contact\">\n");
      out.write("\t\t\t\t\t\t<section class=\"clearfix\">\n");
      out.write("\t\t\t\t\t\t<div class=\"g1\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"sny-icon-box\">\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"sny-icon\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<i class=\"icon-globe\"></i>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"sny-icon-content\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<h4>Địa chỉ hiện nay</h4>\n");
      out.write("\t\t\t\t\t\t\t\t\t<p>KTX B DHQG TPHCM</p>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<div class=\"g1\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"sny-icon-box\">\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"sny-icon\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<i class=\"icon-phone\"></i>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"sny-icon-content\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<h4>SỐ điện thoại</h4>\n");
      out.write("\t\t\t\t\t\t\t\t\t<p>0981669705</p>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<div class=\"g1\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"sny-icon-box\">\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"sny-icon\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<i class=\"icon-user\"></i>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"sny-icon-content\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<h4>Lời ngõ</h4>\n");
      out.write("\t\t\t\t\t\t\t\t\t<p>Hạnh phúc là khi chinh phục được đam mê và niềm khác\n");
      out.write("\t\t\t\t\t\t\t\t\t\tvọng cùng với những người mình yêu quý.</p>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<div class=\"break\"></div>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t</section>\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t<!-- End Contact Data -->\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t\t<!-- End Tab Container -->\n");
      out.write("\t\t\t<footer class=\"footer\"%> ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "footer.jsp", out, false);
      out.write("\n");
      out.write("\t\t\t</footer>\n");
      out.write("\t\t</div>\n");
      out.write("\t\t<!-- #main -->\n");
      out.write("\t</div>\n");
      out.write("\t<!-- #main-container -->\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("</body>\n");
      out.write("</html>");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try {
            if (response.isCommitted()) {
              out.flush();
            } else {
              out.clearBuffer();
            }
          } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
