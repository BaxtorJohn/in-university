package week2;

import java.util.Random;
import java.util.Scanner;

public class Exercise666 {
	public static int diem = 5;
	   public static void startGame(){
		        Scanner sc = new Scanner(System.in);
		        System.out.print("Nhap 1 so tu 0-5: ");
		        int input = 0;
		        do {
		            try {
		                input = sc.nextInt();
		            } catch (Exception e) {
		                System.out.println(e.toString());
		            }
		        } while (input < 0 || input > 5);

		        Random r = new Random();
		        if (input == r.nextInt(6)) {
		            diem++;
		            System.out.println("Dung, So diem cua ban la: " + diem);
		        } else {
		            diem--;
		            System.out.println("Sai, So diem cua ban la: " + diem);
		        }

		        if (diem == 0) {
		            System.out.println("Trò chơi kết thúc, bạn đã trắng tay");
		            System.exit(0);
		        } else {
		            if (diem == 10) {
		                System.out.println("Win");
		                System.exit(0);
		            }
		        }
	   }
	   public static void main(String args[]){
		   Scanner sc = new Scanner(System.in);
	        int input = 0;
	        System.out.println("-----------------Game Doan So-----------------");
	        System.out.println("1.Start");
	        System.out.println("2.Exit");
	        while (true) {
	            System.out.print("Nhap 1 hoac 2: ");
	            try {
	                input = sc.nextInt();
	                if (input == 1 || input == 2) {
	                    break;
	                }
	            } catch (Exception e) {
	                //                return;
	            }
	            System.out.println("Chương trình chỉ chấp nhập 1 hoặc 2");
	            sc.nextLine(); //để loại bỏ ký tự còn thừa trong sc, như vậy sc sẽ yêu cầu nhập lại, nếu không nó sẽ tự lấy ký tự đó mà không yêu cầu nhập
	        }



	        if (input == 1) {
	            while (Exercise666.diem > 0 && Exercise666.diem < 10) {
	            	Exercise666.startGame();
	            }
	        } else {
	            System.exit(0);
	        }
	    }
	}

