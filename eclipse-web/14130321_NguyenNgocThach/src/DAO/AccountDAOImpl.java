package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;

import model.Account;

public class AccountDAOImpl implements AccountDAO {

	@Override
	public boolean checkAccount(String email) {
		Connection connection = DatabaseConnection.getConnection();
		String sql = "SELECT * FROM ACCOUNT WHERE USERS = '" + email + "'";
		
		try {
			PreparedStatement ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				return true;
			}
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		return false;
	}
	
	@Override
	public Account addAccount(Account account) {
		Connection connection = DatabaseConnection.getConnection();
		String sql = "INSERT INTO ACCOUNT VALUES (?,?,?,?,?,?)";
		
		try {
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setString(1, account.getAccount_id());
			ps.setString(2, account.getAccount_name());
			ps.setString(3, account.getUser());
			ps.setString(4, account.getPass());
			ps.setInt(5, account.getAccesion());
			ps.setInt(6, account.getStates());
			
			ps.executeUpdate();
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return account;
	}

@Override
public boolean checkLogin(String email, String pass) {
	Connection connection = DatabaseConnection.getConnection();
	String sql ="SELECT * FROM ACCOUNT WHERE USERS= '"+email+ "' AND PASSWORD= '"+pass+ "'";
	
	try {
		PreparedStatement ps= connection.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		
		while (rs.next()) {
			return true;
		}
		return false;
			
	} catch (Exception e) {
		e.printStackTrace();
	}
		return true;
}

public static void main(String[] args) {
	AccountDAOImpl ad = new AccountDAOImpl();
//	System.out.println(ad.checkAccount("thach@d.com"));
//	System.out.println(ad.checkLogin("admin@thach.com", "1234567"));
//	System.out.println(ad.getAccount("han").toString());
}

@Override
public Account getAccount(String user) {
	Connection connection = DatabaseConnection.getConnection();
	String sql ="SELECT * FROM ACCOUNT WHERE USERS= '"+user+ "'";
	
	try {
		
		
		PreparedStatement ps= connection.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		
		while (rs.next()) {
			Account account = new Account();
			account.setAccount_id(rs.getString("ACCOUNT_ID"));
			return account;
		}
			
	} catch (Exception e) {
		e.printStackTrace();
	}
		return null;
}


}
