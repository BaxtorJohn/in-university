<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="lab.model.ProductDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@page import="java.util.ArrayList" %>
		<%@page import="lab.model.Product" %>
		<%@page import="lab.model.ProductDAO" %>
	
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sản phẩm</title>

<link href="css/bootstrap.css" rel="stylesheet">
      <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,700,500italic,100italic,100' rel='stylesheet' type='text/css'>
      <link href="css/font-awesome.min.css" rel="stylesheet">
      <link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen"/>
      <link href="css/sequence-looptheme.css" rel="stylesheet" media="all"/>
      <link href="css/style.css" rel="stylesheet">
</head>
<body>
<form action="<%=request.getContextPath() %>/BuyProduct" method="get">
<div class="container_fullwidth">
            <div class="container">
               <div class="hot-products">
                  <h3 class="title"><strong>Hot</strong> Products</h3>
                  <div class="control"><a id="prev_hot" class="prev" href="#">&lt;</a><a id="next_hot" class="next" href="#">&gt;</a></div>
                  <ul id="hot">
                     <li>
                     
                     <%
                     	ArrayList<Product> listP = ProductDAO.showProduct();
                     	for(int i = 0 ; i < listP.size() ; i++){
                     		
                     	
                     %>
                        <div class="row">
                           <div class="col-md-3 col-sm-6">
                              <div class="products">
                              	<div><%=i %></div>
                                 <div class="offer"><%=listP.get(i).getId() %></div>
                                 <div class="thumbnail"><a href="details.html"><img src="<%=listP.get(i).getImage() %>" alt="Product Name"></a></div>
                                 <div class="productname"><%=listP.get(i).getName() %></div>
                                 <h4 class="price"><%=listP.get(i).getPrice() %></h4>
                                 <div class="button_group"><button class="button add-cart" type="button"><a href="<%=request.getContextPath() %>/BuyProduct?index=<%=i%>">Mua ngay</button></div>
                              </div>
                           </div>
                           <%} %>
                       
                     </li>
                  </ul>
               </div>
               </form>
</body>
</html>