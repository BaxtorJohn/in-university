package com.model;

public class Menu {
private String name;
private String link;
public Menu(String name, String link) {
	super();
	this.name = name;
	this.link = link;
}
public String getName() {
	return name;
}
public String getLink() {
	return link;
}

}
