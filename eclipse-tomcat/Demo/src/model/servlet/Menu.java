package model.servlet;

import java.util.ArrayList;

public class Menu {
	private String name;
	private String link;

	public Menu(String name, String link) {
		super();
		this.name = name;
		this.link = link;
	}

	public String getName() {
		return name;
	}

	public String getLink() {
		return link;
	}

	public static ArrayList<Menu> getList() {
		ArrayList<Menu> menuList = new ArrayList<>();
		menuList.add(new Menu("home", "homepage.jsp"));
		menuList.add(new Menu("about", "about.jsp"));
		menuList.add(new Menu("lab", "aboutpage.jsp"));
		return menuList;
	}
}
