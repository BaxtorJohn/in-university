<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="com.model.SanPhamDAO"%>
<%@page import="com.model.Sanpham"%>
<%@page import="com.model.Menu"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Danh sách sản phẩm</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<style>
img{
	width: 70px;
	height: auto
}
h1{
text-align: center;
}
</style>
<body>
<h1>Danh sách các loại Laptop bán chạy nhất quý 1 năm 2018</h1>
	<div class="container">
		<%
			ArrayList<Sanpham> listSP = SanPhamDAO.getSanPham();
			
		%>
		<table class="table table-hover">
		<thead>
			<tr>
				<th>Số thứ tự</th>
				<th>Mã sản phẩm </th>
				<th>Tên sản phẩm </th>
				<th> Hình ảnh </th>
				<th> Giá </th>
				<th></th>
			</tr>
			</thead>
			<%
			int count=0;
				for(Sanpham l : listSP){
					count++;
			%>
			<tbody>
			<tr>
				<td><%=count%></td>
				<td><%=l.getMaSP() %></td>
				<td><%=l.getTenSP() %></td>
				<td><img alt="hình ảnh" src="<%=l.getHinhAnh()%>"></td>
				<td><%=l.getGia() %></td>
				<td><button>Xóa</button></td>
			</tr>
			</tbody>
			<%} %>
		</table>
	</div>
</body>
</html>