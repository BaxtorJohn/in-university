<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="com.model.SanPhamDAO"%>
<%@page import="com.model.Sanpham"%>
<%@page import="com.model.Menu"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>products</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<header>
		<jsp:include page="../Menu/menu.jsp"></jsp:include>
	</header>
	<div class="container">
		<%
			ArrayList<Sanpham> listSP = SanPhamDAO.getSanPham();
			
		%>
		<table class="table table-hover">
		<thead>
			<tr>
				<th>So thu tu</th>
				<th>Ma SP </th>
				<th>Ten SP </th>
				<th> Hinh anh </th>
				<th> Gia </th>
				<th></th>
			</tr>
			</thead>
			<%
			int count=0;
				for(Sanpham l : listSP){
					count++;
			%>
			<tbody>
			<tr>
				<td><%=count%></td>
				<td><%=l.getMaSP() %></td>
				<td><%=l.getTenSP() %></td>
				<td><%=l.getHinhAnh() %></td>
				<td><%=l.getGia() %></td>
				<td>Xoa</td>
			</tr>
			</tbody>
			<%} %>
		</table>
	</div>
	<footer>
		<jsp:include page="../Menu/footer.jsp"></jsp:include>
	</footer>
</body>
</html>