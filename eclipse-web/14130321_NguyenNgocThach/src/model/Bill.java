package model;

import java.sql.Timestamp;

public class Bill {
	private String bill_id;
	private Account account;
	private String address_order;
	private String payment_methods;
	private Timestamp date_buy;
	private int state_order;

	public Bill() {}
	public Bill(String bill_id, Account account, String address_order, String payment_methods, Timestamp date_buy,
			int state_order) {
		super();
		this.bill_id = bill_id;
		this.account = account;
		this.address_order = address_order;
		this.payment_methods = payment_methods;
		this.date_buy = date_buy;
		this.state_order = state_order;
	}

	public String getBill_id() {
		return bill_id;
	}

	public void setBill_id(String bill_id) {
		this.bill_id = bill_id;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public String getAddress_order() {
		return address_order;
	}

	public void setAddress_order(String address_order) {
		this.address_order = address_order;
	}

	public String getPayment_methods() {
		return payment_methods;
	}

	public void setPayment_methods(String payment_methods) {
		this.payment_methods = payment_methods;
	}

	public Timestamp getDate_buy() {
		return date_buy;
	}

	public void setDate_buy(Timestamp date_buy) {
		this.date_buy = date_buy;
	}

	public int getState_order() {
		return state_order;
	}

	public void setState_order(int state_order) {
		this.state_order = state_order;
	}

}
