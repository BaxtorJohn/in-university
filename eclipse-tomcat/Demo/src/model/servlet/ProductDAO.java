package model.servlet;

import java.util.ArrayList;

public class ProductDAO {
	
	public static ArrayList<Products> getProList(){
		ArrayList<Products> products = new ArrayList<>();
		products.add(new Products(1, 101, "laptop", 3000));
		products.add(new Products(2, 202, "iphone", 2000));
		products.add(new Products(3, 303, "samsung", 900));
		return products;
	}
	
}
