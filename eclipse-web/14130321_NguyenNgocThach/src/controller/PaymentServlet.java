package controller;

import java.io.IOException;
import java.sql.Timestamp;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.AccountDAOImpl;
import DAO.BillDAOImpl;
import DAO.DetailBillDAOImpl;
import model.Account;
import model.Bill;
import model.Cart;
import model.DetailBill;
import model.Product;
import tool.SendMail;

/**
 * Servlet implementation class PaymentServlet
 */
@WebServlet("/PaymentServlet")
public class PaymentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       private final BillDAOImpl billDAO = new BillDAOImpl();
       private final DetailBillDAOImpl detailbillDAO = new DetailBillDAOImpl();
       private final AccountDAOImpl accountDAO = new AccountDAOImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PaymentServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		HttpSession session = request.getSession();
		Cart cart = (Cart) session.getAttribute("cart");
		
		String address_order = request.getParameter("address_order");
		String payment_methods = request.getParameter("payment_methods");
		String account_id = (String) session.getAttribute("username");
		
		String url ="";
		try {
			Date date = new Date();
			String bill_id = ""+ date.getTime();
			Account a = new Account();
			a.setAccount_id(accountDAO.getAccount(account_id).getAccount_id());
			
			Bill bill = new Bill(bill_id, a, address_order, payment_methods, 
					new Timestamp(new Date().getTime()), 0);
			
			billDAO.addBill(bill);
			TreeMap<Product,Integer> list = cart.getListProduct();
			for(Map.Entry<Product, Integer> listDetails : list.entrySet()) {
				String detailill_id = "" + date.getTime() + listDetails.getKey().getCategory_id();
				Product product = new Product();
				product.setProduct_id(listDetails.getKey().getProduct_id());
				detailbillDAO.addDetailBill(new DetailBill(detailill_id, bill, product, 
						listDetails.getValue(),listDetails.getKey().getPrice(), listDetails.getKey().getSales()));
				
			}
			SendMail send = new SendMail();
			send.sendMail(account_id, "Thông tin mua hàng", account_id    + "\nTổng tiền hàng: " + cart.total());
			
			
			url ="/cart.jsp";
		} catch (Exception e) {
			e.printStackTrace();
			url ="/error.jsp";
		} finally {
			url ="/index.jsp";
		}
		
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(url);
		requestDispatcher.forward(request, response);
		
	}

}
