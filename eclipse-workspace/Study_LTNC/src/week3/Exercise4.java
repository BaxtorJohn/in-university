package week3;

import java.awt.PageAttributes.PrintQualityType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Exercise4 {
	// Cho ham so nguyen bat ky, viet ham sap xep theo thu tu tu lon -> nho va nho
	// -> lon
	// ap dung Bubble sort
	// phuong thuc sap xep tu nho - > lon
	public int[] ascending(int[] array) {
		int tmp;
		// lay phan tu nho nhat se dua len dau tien
		for (int i = 0; i < array.length - 1; i++) {
			for (int j = array.length - 1; j >= 1; j--) {
				if (array[j] < array[j - 1]) {
					tmp = array[j];
					array[j] = array[j - 1];
					array[j - 1] = tmp;
				}
			}
		}
		print(array);
		return array;

	}


	// phuong thuc sap xep tu lon -> nho
	public int[] decrease(int[] array) {
		int tmp;
		// lay phan tu lon nhat se dua len dau tien
		for (int i = 0; i < array.length - 1; i++) {
			for (int j = array.length - 1; j >= 1; j--) {
				if (array[j] > array[j - 1]) {
					tmp = array[j];
					array[j] = array[j - 1];
					array[j - 1] = tmp;
				}
			}
		}
		print(array);
		return array;
	}

	public void print(int[] array) {
		for (int i = 0; i < array.length; i++) {
			System.out.print(" " + array[i]);
		}
	}

	public static void main(String[] args) {
		Exercise4 e4 = new Exercise4();
		int[] array = { 4, 2, 9, 6, 11 };
		System.out.print("\n array original: ");
		e4.print(array);
		// test phuong thuc ascending
		System.out.print("\n array after sort: ");
		e4.ascending(array);

		// test phuong thuc decrease
		System.out.print("\n array after sort decrease: ");
		e4.decrease(array);

}
}
