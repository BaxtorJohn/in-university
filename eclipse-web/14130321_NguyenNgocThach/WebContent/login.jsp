<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Đăng nhập | Đăng ký</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
$( function() {
$( "#datepicker" ).datepicker();
} );
</script>
</head><!--/head-->

<body>

<%
	String name_err = "", user_err = "", pass_err = "";
	if(request.getAttribute("name_err") != null){
		name_err = (String) request.getAttribute("name_err");
	}
	if(request.getAttribute("user_err") != null){
		user_err = (String) request.getAttribute("user_err");
	}
	if(request.getAttribute("pass_err") != null){
		pass_err = (String) request.getAttribute("pass_err");
	}
	
	String name = "", user = "", pass = "";
	if(request.getAttribute("name") != null){
		name = (String) request.getAttribute("name");
	}
	if(request.getAttribute("user") != null){
		user= (String) request.getAttribute("user");
	}
	if(request.getAttribute("pass") != null){
		pass = (String) request.getAttribute("pass");
	}
	
	String err= "";
	if(request.getAttribute("err") != null){
		err = (String) request.getAttribute("err");
	}
%>
	<header id="header"><!--header-->
	<jsp:include page="menu.jsp"></jsp:include>
	</header><!--/header-->
	
	<section id="form"><!--form-->
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-sm-offset-1">
					<div class="login-form"><!--login form-->
						<h2>Đăng nhập vào tài khoản</h2>
						<form action="CheckLogin" method="post">
						<p style="color: red" > <%=err%></p>
							<input type="email" placeholder="Tên đăng nhập" name="user" />
							<input type="password" placeholder="Mật khẩu" name="pass" />
							<span>
								<input type="checkbox" class="checkbox"> 
								Duy trì đăng nhập
							</span>
							<button type="submit" class="btn btn-default">Đăng nhập</button>
						</form>
					</div><!--/login form-->
				</div>
				<div class="col-sm-1">
					<h2 class="or">hoặc</h2>
				</div>
				<div class="col-sm-4">
					<div class="signup-form"><!--sign up form-->
						<h2>Tạo tài khoản mới</h2>
						<form action="CheckAccount" method="post">
							<p style="color: red" > <%=name_err%></p>
							<input type="text" placeholder="Tên tài khoản" name="name" value="<%=name %>" />
							<p style="color: red" > <%=user_err%></p>
							<input type="email" placeholder="Tên đăng nhập " name="user" <%=user %> />
							<p style="color: red" > <%=pass_err%></p>
							<input type="password" placeholder="Mật khẩu" name="pass" <%=pass %> />
							<p>Ngày tháng năm sinh: <input type="text" id="datepicker"></p>
							<button type="submit" class="btn btn-default">Đăng ký</button></div>
						</form>
					</div><!--/sign up form-->
							
				</div>
			</div>
		</div>
	</section><!--/form-->
	
	
	<footer id="footer"><!--Footer-->
		<jsp:include page="footer.jsp"></jsp:include>
	</footer><!--/Footer-->
	

  
  <!--   <script src="js/jquery.js"></script>  su dung datepicker nen ko dung cai nay -->
	<script src="js/price-range.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>