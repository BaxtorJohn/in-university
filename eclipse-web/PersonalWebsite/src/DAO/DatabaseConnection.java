package DAO;

import java.sql.Connection;
import java.sql.DriverManager;

public class DatabaseConnection {
	public static Connection getConnection() {
		Connection connection = null;
		String driver = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://127.0.0.1:3306/Web_Personal";
		String user = "root";
		String pass = "nguyenngocthach71";
		try {
			Class.forName(driver);
			connection = DriverManager.getConnection(url, user, pass);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return connection;

	}
	
	public static void main(String[] args) {
		Connection connection = DatabaseConnection.getConnection();
		if(connection != null) {
			System.out.println("Kết nối thành công");
		}else {
			System.out.println("Kết nối thất bại");
		}
	}
}
