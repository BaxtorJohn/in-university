<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>lab 6</title>
</head>
<style>
	.part1 img {
		width: 100px;
		margin-left: 50px;
		
	}
	.part2 img{
		width: 800px;
		height: auto;
		margin-left: 200px;
	}
	.part3 img {
		width: auto;
		
	}
</style>
<body>
<h1>Kết nối với CSDL MYSQL</h1>
<h2>I / Chuẩn bị</h2>
<span class="part1"><img alt="eclipse" src="../../images/eclipseicon.png"></span>
<span class="part1"><img alt="CSDLMysql" src="../../images/MySQLWorkbench.png"></span>
<p style="margin-left: 70px; font-size:18px">Eclipse<span style="margin-left:70px">CSDL MySQL</span></p>
<a href="https://o7planning.org/vi/10221/huong-dan-cai-dat-va-cau-hinh-mysql-community"> *Hướng dẫn cài đăt và cấu hình MySQL </a>

<h2>II / Tiến hành kết nối</h2>
<div>
	<h4> 1/ Copy driver của MySQl tương ứng vào thư mục WebContent/WEB-INF/lib/ : </h4>
	<span class="part2"><img alt="hinhanh" src="../../images/connectmysql/1.jpg"></span>
</div>
<div>
	<h4>2/ Build Path :</h4>
	<p>Chọn Build Path -> Configure Build Path.</p>
	<span class="part2"><img alt="hinhanh" src="../../images/connectmysql/2.jpg">
	<p> Nhấp vào button Add JARs.. và thêm vào thư viện</p>
	<img alt="hinhanh" src="../../images/connectmysql/3.png"></span>
</div>
<div>
	<h4>3/ Tạo Connection và Database :</h4>
	<p> Sau khi bạn đã cài đặt và cấu hình MySQL. Chúng ta tạo 1 Connection để làm việc</p>
	<span class="part2"><img alt="hinhanh" src="../../images/connectmysql/5.png">
	<p> 
	<p> Sau khi đã điền tên Connection ( các dòng khác để mặc định cũng được) , chúng ta Test xem đã kết nối thành công chưa? Nếu đã thành công thì sẽ hiện ra như thế này:</p>
	<span class="part2"><img alt="hinhanh" src="../../images/connectmysql/6.png">
	<p>Còn nếu hiện thông báo lỗi thì bạn xem lại phần cấu hình MySQL nhé</p>
	<span class="part2"><img alt="hinhanh" src="../../images/connectmysql/4.png">
	<p>Sau khi hoàn thành trên màn hình chính của SQL sẽ hiện ra Connection của ban. Nhấp double-click vào để đi vào giao diện làm việc.
	<p>Tiếp tục chúng ta tạo Connect tới Database</p>
	<span class="part2"><img alt="hinhanh" src="../../images/connectmysql/7.jpg">
	<p>Chọn tên database cần muốn kết nối</p>
	<span class="part2"><img alt="hinhanh" src="../../images/connectmysql/8.jpg">
	<p> Và nhấn OK</p>
	<span class="part2"><img alt="hinhanh" src="../../images/connectmysql/9.png">
	<p>Nếu bạn đã thành công thì sẽ hiện tên database của bạn ở đây: </p>
	<span class="part3"><img alt="hinhanh" src="../../images/connectmysql/10.png">
	
</div>
<div>
	<h4>4/ Kết nối MySQL với eclipse: </h4>
	<p> Tạo 1 pakage DAO và tạo 1 class DatabaseConnection.</p>
	<p>Trong class DatabaseConnection chúng ta add code vào để kết nối với CSDL: 
	<span class="part2"><img alt="hinhanh" src="../../images/connectmysql/11.png">
	<p>Và hàm main để Test kết nối. Nếu thành công sẽ hiện như thế này:  </p>
	<span class="part3"><img alt="hinhanh" src="../../images/connectmysql/12.png">
	<p>Nếu kết nối thất bại. Bạn cần xem lại những thông tin về Diver, url, user, password. Còn nếu đã thất bại ở bước 3 rồi thì tất nhiên sẽ kết nối thất bại rồi nhé :D </p> 
</div>
</body>
</html>