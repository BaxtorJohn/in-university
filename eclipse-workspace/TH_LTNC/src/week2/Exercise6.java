package week2;

import java.util.Scanner;

public class Exercise6 {
	  final public String numberS = "5346";
	    public String numberM = "";

	    public Exercise6() {
	    }

	    /* xử lý đoán số */
	    public boolean doanSo() {
	        Scanner input = new Scanner(System.in);
	        int ileng = this.numberS.length();
	        for (int i = 1; i <= 6; i++) {
	            do {
	                System.out.print("Nhập vào số cần đoán có " + ileng + " chữ số:");
	                this.numberM = input.nextLine();
	                System.out.println("Bạn còn có " + (6 - i) + " lần đoán.");
	                if (this.numberM.length() != this.numberS.length()) {
	                    System.out.println("Nhập sai đô dài số đã cho mời nhập lại.");
	                } else {
	                    break;
	                }
	            } while (true);

	            if (this.numberM.equals(this.numberS)) {
	                return true;
	            }

	            for (int j = 0; j < ileng; j++) {
	                if (this.numberM.charAt(j) == this.numberS.charAt(j)) {
	                    System.out.println("Bạn đã nhập đúng số có giá trị " + this.numberS.charAt(j)
	                            + " ở vị trí thứ " + j );
	                } else {
	                    System.out.println("Bạn đã nhập sai số có giá trị " + this.numberM.charAt(j)
	                            + " ở vị trí thứ " + j );
	                }
	            }
	        }
	        return false;
	    }

	    public static void main(String[] args) {
	        Exercise6 doanso = new Exercise6();
	        if (doanso.doanSo()) {
	            System.out.println("Bạn đã trả lời đúng số cần đoán là:" + doanso.numberS);
	        } else {
	            System.out.println("Bạn đã trả lời sai số cần đoán là:" + doanso.numberS
	                    + " và đã hết lượt đoán.");
	        }
	        System.gc();
	    }
}
