package week4;

public class Exercise3 {
//viet phuong thuc tinh giai thua cua 1 so
	
	//phuong thuc ko dung de quy
	public static int giaiThua(int n) {
		int result =1;
		if( n ==0 || n ==1) {
			return result;
		}else {
			for(int i = 2; i <= n ; i ++) {
				result *= i;
			}
		}
		return result;
	}
	
	//Viet theo de quy
	public static int factorial(int n) {
		int result = 0;
		if(n ==0 || n ==1) {
			 result = 1;
		} else { 
			result = n * factorial(n-1 );
			
		}
		return result;
	}
	public static void main(String[] args) {
		System.out.println(factorial(8));
		//
		System.out.println("giai thua khong dung de quy: ");
		System.out.println(giaiThua(8));
	}
}
