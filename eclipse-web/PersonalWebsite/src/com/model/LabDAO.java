package com.model;

import java.util.ArrayList;

public class LabDAO {
public static ArrayList<Labs> getLabs(){
	ArrayList<Labs> labs = new ArrayList<Labs>();
	labs.add(new Labs(1, "Bài tập 1", "ShareFolder/exercises/1. Lab1.pdf", "LTW/Exercise1.jsp"));
	labs.add(new Labs(2, "Bài tập 2", "ShareFolder/exercises/2.-Lab2.pdf", "LTW/Exercise2.jsp"));
	labs.add(new Labs(3, "Bài tập 3", "ShareFolder/exercises/3.-Lab3.pdf", "LTW/Exercise3.jsp"));
	labs.add(new Labs(4, "Bài tập 4", "ShareFolder/exercises/4.-Lab4.pdf", "LTW/Exercise4.jsp"));
	labs.add(new Labs(5, "Bài tập 5", "ShareFolder/exercises/5.-Lab5.pdf", "LTW/Exercise5.jsp"));
	labs.add(new Labs(6, "Bài tập 6", "ShareFolder/exercises/6.-Lab6.pdf", "LTW/Exercise6.jsp"));

	return labs;
	
}

}
