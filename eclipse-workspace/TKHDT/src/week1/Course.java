package week1;

public class Course {
	private String name;
	private int credits;
	private Lecturer lecture;

	public String getName() {
		return name;
	}

	public int getCredits() {
		return credits;
	}

	public Lecturer getLecture() {
		return lecture;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setCredits(int credits) {
		this.credits = credits;
	}

	public void setLecture(Lecturer lecture) {
		this.lecture = lecture;
	}

	public Course(String name, int credits, Lecturer lecture) {
		super();
		this.name = name;
		this.credits = credits;
		this.lecture = lecture;
	}

	public int hashCode() {
		return name.hashCode();
	}
//kiem tra 1 doi tuong truyen vao co phai la Course co san hay ko ?
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (this == null)
			return false;
		if (getClass() != obj.getClass())
			return false;

		Course other = (Course) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;

	}

	@Override
	public String toString() {
		return getName() + " " + getCredits() + " " + getLecture();
	}
	
}
