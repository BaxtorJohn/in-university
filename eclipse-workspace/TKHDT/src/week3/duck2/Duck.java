package week3.duck2;

public abstract class Duck {
	protected FlyBehavior flybehavior;
	protected QuackBehavior quackbehavior;
	
	public Duck() {
		
	}

	public void setFlybehavior(FlyBehavior flybehavior) {
		this.flybehavior = flybehavior;
	}

	public void setQuackbehavior(QuackBehavior quackbehavior) {
		this.quackbehavior = quackbehavior;
	}
	public abstract void display();
	
	public void performFly() {
		flybehavior.fly();
	}
	public void performQuack() {
		quackbehavior.quack();
	}
	public void swim() {
		System.out.println("all dick float, even decoy");
	}
}
