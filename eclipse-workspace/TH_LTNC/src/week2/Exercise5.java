package week2;

public class Exercise5 {
	public static void printPasscalTriangle(int size) {
		int[][] matrix = new int[size][];
		for (int row = 0; row < size; row++) {
			matrix[row] = new int[row + 1];
			matrix[row][0] = 1;
			matrix[row][row] = 1;
			for (int col = 1; col < row; col++) { // vi cot 0 xet roi
				matrix[row][col] = matrix[row - 1][col - 1] + matrix[row - 1][col];
			}
		}
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println();
		}
	}

	public static void main(String[] args) {
		printPasscalTriangle(5);
	}
}
