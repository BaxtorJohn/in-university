package week4;

public class StatisticsDisplay implements Observer,DisplayElement{
	private float maxTemp = 0.0f;
	private float minTemp = 200;
	private float tempSum = 0.0f;
	private float numReadings;
	private WeatherData weatherData;

	 public StatisticsDisplay(WeatherData weatherData) {
	      this.weatherData = weatherData;
	      // TODO: register this to observe weatherData
	   }

	   public void update(float temp, float humidity, float pressure) {
	      tempSum += temp;
	      numReadings++;

	      if (temp > maxTemp) {
	         maxTemp = temp;
	      }

	      if (temp < minTemp) {
	         minTemp = temp;
	      }
	      display();
	   }

	   public void display() {
	      System.out.println("Avg/Max/Min temperature = " + (tempSum / numReadings)
	            + "/" + maxTemp + "/" + minTemp);
	   }
}
