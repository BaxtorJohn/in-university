package week2;

import week1.Date;

public class ThreeReading extends Reading {
	public ThreeReading(Date date, int index) {
		super(date, index);
		// TODO Auto-generated constructor stub
	}

	public ThreeReading(Date date, int index, int hightIndex, int lowIndex) {
		super(date,index);
		this.hightIndex=hightIndex;
		this.lowIndex=lowIndex;
	}

	private int hightIndex;
	private int lowIndex;

	public int getHightIndex() {
		return hightIndex;
	}

	public int getLowIndex() {
		return lowIndex;
	}

}
