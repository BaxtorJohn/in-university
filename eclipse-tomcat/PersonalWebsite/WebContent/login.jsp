<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>

<!DOCTYPE html>
<html lang="en-US">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Đăng nhập</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="css/loginstyle.css">
<!--===============================================================================================-->
<link
	href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200"
	rel="stylesheet">
<link
	href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css"
	rel="stylesheet">
<!--  <link href="css/aos.css" rel="stylesheet"> chua import vao -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">

<%
	Map<String, String> s = (Map<String, String>) request.getAttribute("errors");
	if (s == null) {
		s = new HashMap();
	}
%>
</head>


<body style="background-color: #666666;">
	<header>
		<jsp:include page="menu.jsp"></jsp:include>
	</header>

	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form validate-form" action="LoginIndex"
					method="post">
					<span class="login100-form-title p-b-43"> Đăng nhập
					</span>


					<div class="wrap-input100 validate-input"
						data-validate="Valid email is required: ex@abc.xyz">
						<input class="input100" type="text" name="email"> <span
							class="focus-input100"></span> <span class="label-input100">Email</span>
						<font color="red"> <%
 	if (s.containsKey("email_err")) {
 %> <%=s.get("email_err").toString()%> <%
 	}
 %>

						</font> <font color="red"> <%
 	if (s.containsKey("email_inconrrect")) {
 %> <%=s.get("email_inconrrect").toString()%> <%
 	}
 %>
						</font>
					</div>


					<div class="wrap-input100 validate-input"
						data-validate="Password is required">
						<input class="input100" type="password" name="pass"> <span
							class="focus-input100"></span> <span class="label-input100">Password</span>
						<font color="red"> <%
 	if (s.containsKey("pass_err")) {
 %> <%=s.get("pass_err").toString()%> <%
 	}
 %>
						</font> <font color="red"> <%
 	if (s.containsKey("pass_incorrect")) {
 %> <%=s.get("pass_incorrect").toString()%> <%
 	}
 %>
						</font>
					</div>

					<div class="flex-sb-Login to continuem w-full p-t-3 p-b-32">
						<div class="contact100-form-checkbox">
							<input class="input-checkbox100" id="ckb1" type="checkbox"
								name="remember-me"> <label class="label-checkbox100"
								for="ckb1">Nhớ tài khoản </label>
						</div>

						<div>
							<a href="forgotpassword.jsp" class="txt1"> Quên mật khẩu? </a>
						</div>
					</div>


					<div class="container-login100-form-btn">
						<button class="login100-form-btn">Đăng nhập</button>
					</div>

					<div class="register-form-btn">
						<a href="register.jsp"> Hoặc Đăng ký </a>
					</div>
					<div class="login100-form-social">
						<a class="cc-facebook btn btn-link" href="https://www.facebook.com/nguyenngocthach.Jade"><i
				class="fa fa-facebook fa-2x " aria-hidden="true"></i></a><a
				class="cc-twitter btn btn-link " href="https://twitter.com/JadeNguyen0701"><i
				class="fa fa-twitter fa-2x " aria-hidden="true"></i></a><a
				class="cc-google-plus btn btn-link" href="#"><i
				class="fa fa-google-plus fa-2x" aria-hidden="true"></i></a><a
				class="cc-instagram btn btn-link" href="https://www.instagram.com/thachnguyendt/"><i
				class="fa fa-instagram fa-2x " aria-hidden="true"></i></a>
					</div>
				</form>

				<div class="login100-more"
					style="background-image: url('images/bg-01.jpg');"></div>
			</div>
		</div>
	</div>





	<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
	<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>