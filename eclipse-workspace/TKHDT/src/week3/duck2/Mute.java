package week3.duck2;

public class Mute implements QuackBehavior{

	@Override
	public void quack() {
		System.out.println("<<<Silence>>>");
	}

}
