package DAO;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

import model.Category;
import model.Product;

public class ProductDAOImpl implements ProductDAO{
	public static Set<String > searchProduct = new TreeSet<>();
	
	@Override
	public ArrayList<Product> getListProduct() {
		Connection connection = (Connection) DatabaseConnection.getConnection();
		String sql = "SELECT * FROM PRODUCT";
		
		ArrayList<Product> list = new ArrayList<Product>();
		try {
			PreparedStatement ps = (PreparedStatement) connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				Product product = new Product();
				product.setProduct_id(rs.getString("PRODUCT_ID"));
				product.setName(rs.getString("NAME"));
				product.setImages(rs.getString("IMAGES"));
				product.setPrice(rs.getFloat("PRICE"));
				product.setCategory_id(Integer.parseInt(rs.getString("SUB_ID")));
				list.add(product);
			}
			connection.close();
		} catch (Exception e) {
//			Logger.getLogger(CategoryDAOImpl.class.getName(), null).log(Level.SEVERE,null,e);
			System.out.println("Error");
			e.printStackTrace();
		}
		
		return list;
	}

	@Override
	public ArrayList<Product> getListProductByCategory(String sub_category_id) {
		Connection connection = (Connection) DatabaseConnection.getConnection();
		String sql = "SELECT * FROM PRODUCT WHERE SUB_ID='"+ sub_category_id +"'";
		
		ArrayList<Product> list = new ArrayList<Product>();
		try {
			PreparedStatement ps = (PreparedStatement) connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				Product product = new Product();
				product.setProduct_id(rs.getString("PRODUCT_ID"));
				Category category = new Category(rs.getInt("SUB_ID"),"");
				product.setCategogry(category);
				product.setName(rs.getString("NAME"));
				product.setImages(rs.getString("IMAGES"));
				product.setPrice(rs.getFloat("PRICE"));
				list.add(product);
				
			}
			connection.close();
		} catch (Exception e) {
//			Logger.getLogger(CategoryDAOImpl.class.getName(), null).log(Level.SEVERE,null,e);
			System.out.println("Error");
			e.printStackTrace();
		}
		
		return list;
	}
	@Override
	public Product getProduct(String product_id) {
		Connection connection = (Connection) DatabaseConnection.getConnection();
		String sql = "SELECT * FROM PRODUCT WHERE PRODUCT_ID='"+ product_id +"'";
		
		Product product = new Product();
		try {
			PreparedStatement ps = (PreparedStatement) connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				Category category = new Category(rs.getInt("SUB_ID"),"");
				product.setProduct_id(rs.getString("PRODUCT_ID"));
				product.setCategogry(category);
				product.setName(rs.getString("NAME"));
				product.setImages(rs.getString("IMAGES"));
				product.setPrice(rs.getFloat("PRICE"));
			}
			connection.close();
		} catch (Exception e) {
//			Logger.getLogger(CategoryDAOImpl.class.getName(), null).log(Level.SEVERE,null,e);
			System.out.println("Error");
			e.printStackTrace();
		}
		
		return product;
	}
	
public static void main(String[] args) {
	ProductDAOImpl dao = new ProductDAOImpl();
//	System.out.println(dao.getListProduct().size());
//	System.out.println(dao.getListProductByCategory("101").size());
//	System.out.println(dao.getProduct("1011").getName());
//	System.out.println(dao.getProductByName());
//	System.out.println(dao.filterProductByName("VỢT YONEX DOURA").size());
//	System.out.println(dao.addProduct(new Product("1", "mới", "", 1000000, 1446)).getName());
	System.out.println(dao.editProduct(new Product("112121", "mới", "", 1000000, 1446), "1").getName());
//	System.out.println(dao.deleteProduct("1"));
}

@Override
public Set<String> getProductByName() {
	Connection connection = (Connection) DatabaseConnection.getConnection();
	String sql = "SELECT * FROM PRODUCT";
	
	try {
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		
		while(rs.next()) {
			searchProduct.add(rs.getString("NAME"));
		}
		connection.close();
	} catch (Exception e) {
//		Logger.getLogger(CategoryDAOImpl.class.getName(), null).log(Level.SEVERE,null,e);
		System.out.println("Error");
		e.printStackTrace();
	}
	
	return searchProduct;
}

@Override
public ArrayList<Product> filterProductByName(String name) {
	Connection connection = (Connection) DatabaseConnection.getConnection();
	String sql = "SELECT * FROM PRODUCT WHERE NAME= N'"+ name +"'";
	
	ArrayList<Product> listproduct = new ArrayList<Product>();
	try {
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		
		while(rs.next()) {
			Product product = new Product();
			product.setProduct_id(rs.getString("PRODUCT_ID"));
			product.setName(rs.getString("NAME"));
			product.setImages(rs.getString("IMAGES"));
			product.setPrice(rs.getFloat("PRICE"));
			product.setCategory_id(Integer.parseInt(rs.getString("SUB_ID")));
			listproduct.add(product);
		}
		connection.close();
	} catch (Exception e) {
//		Logger.getLogger(CategoryDAOImpl.class.getName(), null).log(Level.SEVERE,null,e);
		System.out.println("Error");
		e.printStackTrace();
	}
	
	return listproduct;
}

//them sua xoa

@Override
public Product addProduct(Product product) {
	Connection connection = (Connection) DatabaseConnection.getConnection();
	String sql = "INSERT INTO PRODUCT VALUES (?,?,?,?,?)";
	
	try {
		PreparedStatement ps = connection.prepareStatement(sql);
		ps.setString(1, product.getProduct_id());
		ps.setString(2, product.getName());
		ps.setString(3, product.getImages());
		ps.setFloat(4, product.getPrice());
		ps.setInt(5, product.getCategory_id());
		
		ps.executeUpdate();
		connection.close();
		
	} catch (Exception e) {
		e.printStackTrace();
	}
	return product;
}

@Override
public Product editProduct(Product product, String product_id) {
	Connection connection = (Connection) DatabaseConnection.getConnection();
	String sql = "Update PRODUCT set (?,?,?,?,?) where PRODUCT_ID= '" + product_id +"'";
	
	try {
		PreparedStatement ps = connection.prepareStatement(sql);
		ps.setString(1, product.getProduct_id());
		ps.setString(2, product.getName());
		ps.setString(3, product.getImages());
		ps.setFloat(4, product.getPrice());
		ps.setInt(5, product.getCategory_id());
		
		ps.executeUpdate();
		connection.close();
		
	} catch (Exception e) {
		e.printStackTrace();
	}
	return product;
}

@Override
public boolean deleteProduct(String product_id) {
	Connection connection = (Connection) DatabaseConnection.getConnection();
	String sql = "DELETE FROM PRODUCT WHERE PRODUCT_ID = '" + product_id +"'";
	
	try {
		PreparedStatement ps = connection.prepareStatement(sql);
		ps.executeUpdate();
		connection.close();
		
		return  true ;
	} catch (Exception e) {
		e.printStackTrace();
	}
	return false;
}

}
