package week2;

import java.util.Scanner;

public class Exercise2 {
	//chuyen co so cua 1 so nguyen bat ky
	
	public static int nhapSoTN() {
        Scanner input = new Scanner(System.in);
        boolean check = false;
        int n = 0;
        while (!check) {
               try {
                     n = Integer.parseInt(input.nextLine());
                     if (n < 0) {
                            System.out.println("Bạn phải nhập số tự nhiên! Hãy nhập lại.");
                            continue;
                     }
                     check = true;
               } catch (Exception e) {
                     System.out.println("Bạn phải nhập số tự nhiên! Hãy nhập lại.");
               }
        }
        return (n);
 }

 public static void doiCoSo2816(int n, int base) {
        if (base == 2) {
               System.out.println("Số thập phân " + n + " đổi thành số ở hệ nhị phân là: " + Integer.toBinaryString(n));
        } else if (base == 8) {
               System.out.println("Số thập phân " + n + " đổi thành số ở hệ bát phân là: " + Integer.toOctalString(n));
        } else if (base == 16) {
               System.out.println("Số thập phân " + n + " đổi thành số ở hệ thập lục phân là: " + Integer.toHexString(n));
        } else
               System.out.println("Chương trình không hỗ trợ.");
 }

 public static void doiCoSo(int n, int base) {
        if (n >= base)
               doiCoSo(n / base, base);
        if (n % base > 9)
               System.out.printf("%c", n % base + 55);
        else
               System.out.print((n % base));
 }

 public static void main(String[] args) {
        System.out.println("Nhap số tự nhiên n: ");
        int n = nhapSoTN();
        System.out.println("Nhập vào cơ số cần chuyển đổi: ");
        int i = nhapSoTN();
        doiCoSo2816(n, i);
        doiCoSo(n, i);
 }
}
