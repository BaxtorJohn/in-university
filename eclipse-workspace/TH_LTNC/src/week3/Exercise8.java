package week3;

public class Exercise8 {
	// phuong thuc doc vao 1 so nguyen bat ky
	public static String inSN(int number) {
		String tmp, result = "";
		tmp = "" + number; // chuyen thanh chuoi
		int count = 0;
		for (int i = tmp.length() - 1; i >= 0; i--) {
			result = tmp.charAt(i) + result;
			count++;
			if (count == 3) {
				if (i > 0)
					result = "," + result;
				count = 0;
			}

		}
		return result;
	}

	public static void main(String[] args) {
		System.out.println(inSN(3131583));
	}
}
