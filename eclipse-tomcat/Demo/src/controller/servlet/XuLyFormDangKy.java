package controller.servlet;

import java.awt.image.RescaleOp;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.servlet.DangKy;

/**
 * Servlet implementation class XuLyFormDangKy
 */
@WebServlet("/XuLyFormDangKy")
public class XuLyFormDangKy extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public XuLyFormDangKy() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		/*
		 * email[email_errr, "value"] passs[epps
		 * 
		 */
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
//		boolean error = false;
		String email = request.getParameter("email");
		System.out.println(email);
		String pass = request.getParameter("password");
		System.out.println(pass);
		String passwordAgaint = request.getParameter("passwordAgaint");
		System.out.println(passwordAgaint);
		String name = request.getParameter("hoten");
		System.out.println(name);
		String phoneNums = request.getParameter("sdt");
		System.out.println(phoneNums);
		String phone = request.getParameter("dtdd");
		System.out.println(phone);
		String gender = request.getParameter("phai");
		System.out.println(gender);
		String day = request.getParameter("ngay");
		System.out.println(day);
		String month = request.getParameter("thang");
		System.out.println(month);
		String year = request.getParameter("nam");
		System.out.println(year);
		String nation = request.getParameter("quocgia");
		System.out.println(nation);
		String city = request.getParameter("tinhthanh");
		System.out.println(city);
		String district = request.getParameter("quan");
		System.out.println(district);
		String address = request.getParameter("diachi");
		System.out.println(address);
		

		DangKy d = new DangKy(email, pass, passwordAgaint, name, phoneNums, phone, gender, day, month, year, nation, city, district, address);
		if (d.checkErrors()) {
			HttpSession session = request.getSession();
			session.setAttribute("Register"	, d);
		response.sendRedirect("show.jsp");
		}else {
			request.setAttribute("errors", d.getErrors());
			System.out.println(d.getErrors());
			RequestDispatcher rd = getServletContext().getRequestDispatcher("/dangky.jsp");
			rd.forward(request, response);
		}
		
		
	}

}
