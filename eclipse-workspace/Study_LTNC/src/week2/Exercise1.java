package week2;

public class Exercise1 {
	// tim UCLN + BCNN
	public static int UCLN(int a, int b) {
		if (b == 0)
			return a;
		else
			return UCLN(b, a % b);
	}

	public static int BCNN(int a, int b) {
		return (a * b) / UCLN(a, b);
	}

	public static void main(String[] args) {
		System.out.println("UCLN: " + UCLN(20, 16));
		System.out.println("BCNN: " + BCNN(20, 16));
	}
}
