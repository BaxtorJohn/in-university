package week1;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class Student {
	private int id;
	private String name;
	private Date dayOfBir;
	private Lecturer assistant;
	private List<Register> courseRegs = new ArrayList<Register>();

	public Student( String name, Date dayOfBir) {
		super();
		this.name = name;
		this.dayOfBir = dayOfBir;
	}

	public Student() {

	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Date getDayOfBir() {
		return dayOfBir;
	}

	public Lecturer getAssistant() {
		return assistant;
	}

	public List getCourseRegs() {
		return courseRegs;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDayOfBir(Date dayOfBir) {
		this.dayOfBir = dayOfBir;
	}

	public void setAssistant(Lecturer assistant) {
		this.assistant = assistant;
	}

	public void setCourseRegs(List courseRegs) {
		this.courseRegs = courseRegs;
	}

	public List<Register> getCourseRegister() {
		return courseRegs;
	}

	public void register(Course c) {
		courseRegs.add(new Register(c));
	}

	public void updateGrade(Course c, float grade) {
		for (Register r : courseRegs) {
			if (r.getCourse().getName().equals(c.getName())) {
				r.setGrade(grade);
				break;
			}
		}
	}

	// tinh diem trung binh
	public float avarageOfGrade() {
		float sum = 0;
		for (Register r : courseRegs) {
			sum += r.getGrade();
		}
		return sum / courseRegs.size();
	}

	// in ra xep loai
	public String rank() {
		float avarage = avarageOfGrade();
		if (avarage > 9)
			return "XUAT SAC";
		else if (avarage >= 8)
			return "GIOI";
		else if (avarage >= 7)
			return "KHA";
		else if (avarage >= 6)
			return "TRUNG BINH KHA";
		else if (avarage >= 5)
			return "TRUNG BINH";
		else
			return "ROT";
	}

	public void printGradeReport() {
	      System.out.println("Name: " + getName());
	      System.out.println("Date of birth: " + getDayOfBir());
	      System.out.println("STT\tMon\tDiem");
	      for (int i = 0; i < courseRegs.size(); i++) {
	         System.out.println(i + "\t"
	               + courseRegs.get(i).getCourse().getName() + "\t"
	               + courseRegs.get(i).getGrade());
	      }
	      System.out.println("Average of grade: " + avarageOfGrade());
	      System.out.println("Rank: " + rank());
	      System.out.println("----------------------------------------");
	   }

	@Override
	public String toString() {
		return getId() + " " + getName() + " " + getDayOfBir();
	}

}
