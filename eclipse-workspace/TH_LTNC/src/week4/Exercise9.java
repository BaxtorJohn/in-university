package week4;

public class Exercise9 {
	//Sap xep tang dan
	public int[] setASC(int[] arr) {
		int temp = 0;

		for (int i = 0; i < arr.length - 1; i++) {
			for (int j = i + 1; j < arr.length; j++) {
				if (arr[i] > arr[j]) {
					temp = arr[j];
					arr[j] = arr[i];
					arr[i] = temp;
				}
			}
		}
		print(arr);
		return arr;
	}
	//Sap xep giam dan
	public int[] setDESC(int[] arr) {
		int temp = 0;

		for (int i = 0; i < arr.length - 1; i++) {
			for (int j = i + 1; j < arr.length; j++) {
				if (arr[i] < arr[j]) {
					temp = arr[j];
					arr[j] = arr[i];
					arr[i] = temp;
				}
			}
		}
		print(arr);
		return arr;
	}
//in ra mang
	public void print(int[] array) {
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
	}

	public static void main(String[] args) {
		Exercise9 e11 = new Exercise9();
		int[] arr = { 6, 2, 9, 7, 5, 10, 32, 56 };
		System.out.print("Sap xep tang dan: ");
		e11.setASC(arr);
		System.out.print("\nSap xep giam dan: ");
		e11.setDESC(arr);
		System.out.println();

		
	}
}
