package model;

public class Product implements Comparable<Product>{
	private String product_id;
	private String name;
	private String images;
	private float price;
	private int category_id;
	private Category categogry;
	private float sales;
	

	public float getSales() {
		return sales;
	}

	public void setSales(float sales) {
		this.sales = sales;
	}

	public Product(String product_id, String name, String images, float price, int category_id) {
		super();
		this.product_id = product_id;
		this.name = name;
		this.images = images;
		this.price = price;
		this.category_id = category_id;
	}

	public Category getCategogry() {
		return categogry;
	}

	public void setCategogry(Category categogry) {
		this.categogry = categogry;
	}

	public String getProduct_id() {
		return product_id;
	}

	public Product() {
	}

	public void setProduct_id(String product_id) {
		this.product_id = product_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public int getCategory_id() {
		return category_id;
	}

	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}

	@Override
	public int compareTo(Product p) {
		return Integer.parseInt(this.product_id) - Integer.parseInt(p.product_id);
	}

}
