package model;

public class Category {
	private int category_id;
	private String name;
	private int category_farther;

	public Category(int category_id, String name) {
		super();
		this.category_id = category_id;
		this.name = name;
	}
	
	
	public int getCategory_farther() {
		return category_farther;
	}
	public void setCategory_farther(int category_farther) {
		this.category_farther = category_farther;
	}
	public Category(int category_id, String name, int category_farther) {
		super();
		this.category_id = category_id;
		this.name = name;
		this.category_farther = category_farther;
	}
	
	
	public Category() {}

	public int getCategory_id() {
		return category_id;
	}

	public void setCategory_id(int string) {
		this.category_id = string;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
