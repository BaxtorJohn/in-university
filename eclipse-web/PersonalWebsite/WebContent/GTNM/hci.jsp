<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
<title>Giao tiếp người máy</title>
</head>
<body>
<jsp:include page="../menu.jsp"></jsp:include>
<div style="text-align: center; margin-top:100px"><h1> Giao tiếp người máy</h1></div>
<div style="margin-left: 20px">
	<h3>I/ Mục tiêu : </h3>
		<p> Môn học hướng dẫn về tư duy và những phương pháp làm sao để con người và máy tính có thể giao tiếp với nhau.</p>
		<p> Cụ thể trong môn học này tại trường ĐH Nông Lâm TPHCM sẽ lấy ứng dụng web để nghiên cứu và học tập </p>
		<p> Chủ yếu là học về thiết kế giao diện (Frontend) sao cho người dùng dễ tương tác với hệ thống. Học html, css,...</p>
	<h3>II/ Tài liệu :</h3>
		<a href="https://drive.google.com/drive/u/1/folders/1k4yvUPchuGHFpR1IBjHYSRNLk1fX1OM3"  target="_blank"><p>Introduction</p></a>
		<a href="https://drive.google.com/drive/u/1/folders/1k4yvUPchuGHFpR1IBjHYSRNLk1fX1OM3"  target="_blank"><p>Web UI- desgin</p></a>
		<a href="https://drive.google.com/drive/u/1/folders/1k4yvUPchuGHFpR1IBjHYSRNLk1fX1OM3"  target="_blank"><p>Understanding your users and their need</p></a>
		<a href="https://drive.google.com/drive/u/1/folders/1k4yvUPchuGHFpR1IBjHYSRNLk1fX1OM3"  target="_blank"><p>Understanding web UI Element & Principles</p></a>
		<a href="https://drive.google.com/drive/u/1/folders/1k4yvUPchuGHFpR1IBjHYSRNLk1fX1OM3"  target="_blank"><p>Understanding Visual Element of UI</p></a>
		<a href="https://drive.google.com/drive/u/1/folders/1k4yvUPchuGHFpR1IBjHYSRNLk1fX1OM3"  target="_blank"><p>BT mẫu</p></a>
	<h3>III/ Project báo cáo cuối kỳ :</h3>
		<h6 >Xây dưng website Dạy học Tiếng Anh để người dùng có thể tương tác với hệ thống.</h6>
		<a href="#"  target="_blank"><button type="button" class="btn btn-primary">Go to Website</button></a>
</div>
<footer class="footer">
		<jsp:include page="../footer.jsp"></jsp:include>
	</footer>
</body>
</html>