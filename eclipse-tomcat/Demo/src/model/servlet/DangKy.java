package model.servlet;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DangKy {
	private String email;
	private String pass;
	private String repass;
	private String name;
	private String phoneNums;
	private String phone;
	private String gender;
	private String day;
	private String month;
	private String year;
	private String nation;
	private String city;
	private String district;
	private String address;

	Map<String, String> errors;

	public DangKy(String email, String pass, String repass, String name, String phoneNums,String phone, String gender, String day,
			String month, String year, String nation, String city, String district, String address) {
		super();
		this.email = email;
		this.pass = pass;
		this.repass = repass;
		this.name = name;
		this.phoneNums = phoneNums;
		this.phone=phone;
		this.gender = gender;
		this.day = day;
		this.month = month;
		this.year = year;
		this.nation = nation;
		this.city = city;
		this.district = district;
		this.address = address;
		errors = new HashMap<>();
		checkMail();
		checkPass();
		checkName();
		checkPhoneNums();
		phoneNumber();
	}



	public Map<String, String> getErrors() {
		return errors;
	}

	private void checkPass() {
		if (pass == null || pass.equals("")) {
			errors.put("pass_err", "Vui lòng nhập mật khẩu!");
		}
		if (repass == null || repass.equals("")) {
			errors.put("passwordAgaint_err", "Vui lòng nhập lại mật khẩu!");
		} else if (!pass.equalsIgnoreCase(repass)) {
			errors.put("repass_err", "Mật khẩu không trùng nhau");
		}
	}

	private void checkDupplicatePass() {
		if (pass.equals(repass)) {

		}
	}

	private void checkMail() {

		if (email == null || email.equals("")) {
			errors.put("email_err", "Vui lòng nhập địa chỉ email!");

		}
	}

	private void checkName() {

		if (name == null || name.equals("")) {
			errors.put("name_err", "Vui lòng nhập họ và tên!");
		}
	}

	private void checkPhoneNums() {
		if (phoneNums == null || phoneNums.equals("")) {
			errors.put("phoneNums_err", "Vui lòng nhập số điện thoại!");
		}
	}
	public void phoneNumber() {

		Pattern pattern = Pattern.compile("\\d.{9,10}");
		Matcher matcher = pattern.matcher(phoneNums);
		if (!matcher.matches()) {
			errors.put("phoneInt_err", "số điện thoại không hợp lê");
		}
	}

	public boolean checkErrors() {
		return this.errors.isEmpty();
	}

	public String getEmail() {
		return email;
	}

	public String getPass() {
		return pass;
	}

	public String getRepass() {
		return repass;
	}

	public String getName() {
		return name;
	}

	public String getPhoneNums() {
		return phoneNums;
	}

	public String getGender() {
		return gender;
	}

	public String getDay() {
		return day;
	}

	public String getMonth() {
		return month;
	}

	public String getYear() {
		return year;
	}

	public String getNation() {
		return nation;
	}

	public String getCity() {
		return city;
	}

	public String getDistrict() {
		return district;
	}

	public String getAddress() {
		return address;
	}

	public static void main(String[] args) {
		Map<String, String> erros = new HashMap<>();
		// erros.put("email_err", "mail");
		//
		// DangKy d = new DangKy("asa","123","d","á","31411");
		// System.out.println(d.getErrors());
		String phone = "0981669705";
		// System.out.println(d.getEmail());
	}
}
