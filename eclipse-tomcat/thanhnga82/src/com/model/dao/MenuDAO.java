package com.model.dao;

import java.util.ArrayList;

import com.model.Menu;

public class MenuDAO {
	
	public static ArrayList<Menu> getMenuList() {
		ArrayList<Menu> lstMenus = new ArrayList<Menu>();

		lstMenus.add(new Menu("Trang chủ", "index.jsp"));
		lstMenus.add(new Menu("Các ví dụ", "example/index.jsp"));
		lstMenus.add(new Menu("Bài tập", "exercises.jsp"));
		lstMenus.add(new Menu("Giới thiệu", "about.jsp"));
		lstMenus.add(new Menu("Liên hệ", "contact.jsp"));

		return lstMenus;

	}
}
