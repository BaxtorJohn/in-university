package caro;

import java.util.Scanner;

public class CaroClient extends javax.swing.JFrame {
	private String[][] banco = new String[3][3];
	private String nguoidanh1;
	private String nguoidanh2;
	private String quanx = "X";
	private String quano = "O";
	private int vitridanh;
	private int[] luuvitridanh = new int[9];

	public CaroClient() {
		vietsobanco();
		vebanco();
		nhaptennguoichoi();
		batdaychoi();
	}

	public void vietsobanco() {
		int obanco = 1;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				banco[i][j] = Integer.toString(obanco++); // gan o ban co bang so tang dan
			}
			System.out.println();
		}

	}

	public void vebanco() {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				System.out.printf("[%s]", banco[i][j]);
			}
			System.out.println();
		}
	}

	public void nhaptennguoichoi() {
		Scanner in = new Scanner(System.in);
		System.out.print("nhap ten nguoi choi 1: ");
		nguoidanh1 = in.nextLine();
		do {
			System.out.print("nhap ten nguoi choi 2: ");
			nguoidanh2 = in.nextLine();
			if (nguoidanh2.equals(nguoidanh1)) {
				System.out.println("nhap trung tennhap lai ngay: ");
			}
		} while (nguoidanh2.equals(nguoidanh1));// nhap lai neu trung ten

	}

	public void nhapquanco(int nguoidanh, int vitridanh) {
		// xac dinh nguoi choi danh co vao nao xong mh gan quan covao vi tr trong bang
		if (nguoidanh == 1) {
			if (vitridanh == 1) {
				banco[0][0] = quano;
			} else if (vitridanh == 2) {
				banco[0][1] = quano;

			} else if (vitridanh == 3) {
				banco[0][2] = quano;
			} else if (vitridanh == 4) {
				banco[1][0] = quano;
			} else if (vitridanh == 5) {
				banco[0][1] = quano;

			} else if (vitridanh == 6) {
				banco[1][2] = quano;
			} else if (vitridanh == 7) {
				banco[2][0] = quano;
			} else if (vitridanh == 8) {
				banco[2][1] = quano;

			} else if (vitridanh == 9) {
				banco[2][2] = quano;
			}
		}
		if (nguoidanh == 2) {
			// bay gio bat dieu kien ngujoi danh 2
			// tuong tu ta gan quan co theo tung vi tri
			if (vitridanh == 1) {
				banco[0][0] = quanx;
			} else if (vitridanh == 2) {
				banco[0][1] = quanx;

			} else if (vitridanh == 3) {
				banco[0][2] = quanx;
			} else if (vitridanh == 4) {
				banco[1][0] = quanx;
			} else if (vitridanh == 5) {
				banco[0][1] = quanx;

			} else if (vitridanh == 6) {
				banco[1][2] = quanx;
			} else if (vitridanh == 7) {
				banco[2][0] = quanx;
			} else if (vitridanh == 8) {
				banco[2][1] = quanx;

			} else if (vitridanh == 9) {
				banco[2][2] = quanx;
			}
		}
	}

	public void batdaychoi() {
		Scanner in = new Scanner(System.in);
		for (int lan = 0; lan < 9; lan++) {
			do {
				System.out.printf("%s nhap so: ", nguoidanh1);
				vitridanh = in.nextInt();
				if (trungnuoc(vitridanh)) {
					System.out.println("nuoc nay danh roi, danh lai di");
				}
			} while (trungnuoc(vitridanh));
			luuvitridanh[lan] = vitridanh;
			lan++;
			nhapquanco(1, vitridanh);
			vebanco();// moi lan danh ta lai ve ban co

			do {
				System.out.printf("%s nhap so: ", nguoidanh2);
				vitridanh = in.nextInt();
				if (trungnuoc(vitridanh)) {
					System.out.println("nuoc nay danh roi, danh lai di");
				}
			} while (trungnuoc(vitridanh));
			luuvitridanh[lan] = vitridanh;
			nhapquanco(2, vitridanh);
			vebanco();

			// de danh lien tuc ta cho vao vong lap for
		}

	}

	public boolean trungnuoc(int nuoc) {
		for (int i = 0; i < 9; i++) {
			if (nuoc == luuvitridanh[i]) {
				return true;
			}
		}
		return false;
	}

	public static void main(String[] args) {
		new CaroClient();
	}
}
