<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.model.Menu"%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Trần Thị Thanh Nga - Menu Demo</title>
<style>
ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #333;
}

li {
    float: left;
    border-right:1px solid #bbb;
}

li:last-child {
    border-right: none;
}

li a {
    display: block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

li a:hover:not(.active) {
    background-color: #111;
}

.active {
    background-color: #4CAF50;
}
</style>
</head>
<body>
	<%
		//Tạo danh sách các menu - sau này sẽ lấy từ database lên
		ArrayList<Menu> lstMenus = new ArrayList<Menu>();

		lstMenus.add(new Menu("Trang chủ", "home.jsp"));
		lstMenus.add(new Menu("Bài giảng", "slides.jsp"));
		lstMenus.add(new Menu("Bài tập", "exercises.jsp"));
		lstMenus.add(new Menu("Giới thiệu bản thân", "about.jsp"));
		lstMenus.add(new Menu("Liên hệ", "contact.jsp"));
		//Tạo menu động ở đây
		for (int i = 0; i < lstMenus.size(); i++) {
			Menu mnu = lstMenus.get(i);
	%>
	<ul>
		<li><a href="<%=mnu.getPath()%>"><%=mnu.getName()%></a></li>
		<%
			}
		%>
	</ul>
</body>
</html>