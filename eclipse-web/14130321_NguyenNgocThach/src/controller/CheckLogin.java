package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.AccountDAOImpl;
import tool.EnCode;

/**
 * Servlet implementation class CheckLogin
 */
@WebServlet("/CheckLogin")
public class CheckLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private AccountDAOImpl accountDAO = new AccountDAOImpl();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CheckLogin() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (request.getParameter("command").equals("logout")) {
			HttpSession session = request.getSession();
			session.invalidate();
			response.sendRedirect("index.jsp");
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");

		String user = request.getParameter("user");
		String pass = request.getParameter("pass");

		String err = "";

		if (user.equals("") || pass.equals("")) {
			err = "Vui lòng nhập đầy đủ thông tin";
		} else if (accountDAO.checkLogin(user, EnCode.enCodeMD5(pass)) == false) {
			err = "Tài khoản hoặc mật khẩu không chính xác";
		}

		if (err.length() > 0) {
			request.setAttribute("err", err);
		}

		String url = "/login.jsp";

		try {
			if (err.length() == 0) {
				HttpSession session = request.getSession();
				if (user.equals("admin@gmail.com") && pass.equals("admin")) {
					session.setAttribute("username", user);
					url = "/admin.jsp";
				} else {
					session.setAttribute("username", user);
					url = "/index.jsp";
				}
			} else {
				url = "/login.jsp";
			}
			RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(url);
			requestDispatcher.forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/error.jsp");
			requestDispatcher.forward(request, response);
		}
	}

}
