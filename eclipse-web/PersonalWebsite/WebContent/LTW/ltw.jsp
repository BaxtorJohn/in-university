<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
<title>Lập trình web</title>
</head>
<body>
<jsp:include page="../menu.jsp"></jsp:include>
<div style="text-align: center; margin-top:100px"><h1> Lập trình web</h1></div>
<div style="margin-left: 20px">
	<h3>I/ Mục tiêu : </h3>
		<p>- Môn học hướng dẫn về tư duy, phương pháp và kỹ thuật xây dựng hệ thống website đơn giản. </p>
		<p>- Cụ thể trong môn học này tại trường ĐH Nông Lâm TPHCM sẽ học về ngôn ngữ JSP & Servlet</p>
		<p>- Chủ yếu là học về xây dựng các chức năng của hệ thống website (backend).</p>
	<h3>II/ Tài liệu :</h3>
		<h6>Chương trình đào tạo môn LTW của trường DHNL gồm 19 Lecture và 8 bài tập.</h6>
		<a href="https://drive.google.com/drive/u/1/folders/1WeDOBMIkQYLZh2Vp3qZrh9CO1rtAMZ3V" target="_blank"><p>- Slide Lecture</p></a>
		<a href="https://drive.google.com/drive/u/1/folders/1fX5cyOoZR1odI1_WklezTNtoIkG_SH_V" target="_blank"><p>- Lab</p></a>
		<a href="https://drive.google.com/drive/u/1/folders/1qllgZ_IrfsTFCIKVuAQ9uZX3DBtei1X2" target="_blank"><p>- Book</p></a>
	<h3>III/ Bài tập Cá nhân :</h3>
		<a href="exercises.jsp"  target="_blank"><button type="button" class="btn btn-primary">Go to Lab</button></a>
	<h3>IV/ Project giữa học kỳ :</h3>
		<h6 >* Xây dựng chức 3 chức năng quản lý bất kỳ.</h6>
		<p>Trong project này mình vừa xây dựng chức năng quản lý, đồng thời xây dựng chức năng cho khách hàng trên giao diện.
		<p> (Do lúc đầu hiểu sai đề) @@</p>
		<h6>* Các chức năng: </h6>
		<p><b>Chức năng cho khách hàng:</b></p>
		<p>1. Login / Logout.</p>
		<p>2. Hiện thị danh sách sản phẩm, category, và sub-category . Dữ liệu dc load lên từ Database.</p>
		<p>3. Xem chi tiết sản phẩm + Giỏ hàng + Thanh toán </p>
		<p><b>Chức năng cho người quản trị:</b></p>
		<p>4. Quản lý khách hàng</p>
		<p>5. Quản lý sản phẩm</p>
		<p>4. Quản lý đơn hàng</p>
		<a href="https://ngocthachltw2018midtermexam.azurewebsites.net"  target="_blank"><button type="button" class="btn btn-primary">Go to Website</button></a>
	<h3>V/ Project cuối học kỳ :</h3>
		<h6 >Xây dưng hệ thống website quản lý cho trung tậm GYM & Fitness.</h6>
		<p>Trong project này mình vừa xây dựng chức năng quản lý, đồng thời xây dựng chức năng cho khách hàng trên giao diện.
		<p> (Do lúc đầu hiểu sai đề) @@</p>
		<p style="font-style: italic;">Mô tả về project được ghi chép trong project</p>
		<a href="https://2018group5.azurewebsites.net"  target="_blank"><button type="button" class="btn btn-primary">Go to Website</button></a>
</div>
<footer class="footer">
		<jsp:include page="../footer.jsp"></jsp:include>
	</footer>
</body>
</html>