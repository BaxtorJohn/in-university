package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;

import model.DetailBill;

public class DetailBillDAOImpl implements DetailBillDAO{

	@Override
	public void addDetailBill(DetailBill detailbill) {
		Connection connection = DatabaseConnection.getConnection();
		String sql = "INSERT INTO DETAILS_BILL VALUES (?,?,?,?,?,?)";
		
		try {
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setString(1, detailbill.getDetail_id());
			ps.setString(2, detailbill.getBill().getBill_id());
			ps.setString(3, detailbill.getProduct().getProduct_id());
			ps.setInt(4, detailbill.getQuantum());
			ps.setFloat(5, detailbill.getPrice());
			ps.setFloat(6, detailbill.getSales());
			
			ps.executeUpdate();
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
