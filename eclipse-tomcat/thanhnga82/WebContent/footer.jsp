<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/footer-distributed-with-contact-form.css">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
</head>
<body>
	<footer class="footer-distributed">

	<div class="footer-left">
		<p class="footer-company-name">
			Trần Thị Thanh Nga - Khoa CNTT - Trường ĐH Nông Lâm HCM <br>ngattt@hcmuaf.edu.vn
			&copy; 2018
		</p>
		<div class="footer-icons">
			<a href="#"><i class="fa fa-facebook"></i></a> <a href="#"><i
				class="fa fa-twitter"></i></a> <a href="#"><i class="fa fa-linkedin"></i></a>
			<a href="#"><i class="fa fa-github"></i></a>

		</div>

	</div>

	<div class="footer-right">

		<p>Contact Us</p>

		<form action="#" method="post">

			<input type="text" name="email" placeholder="Email" />
			<textarea name="message" placeholder="Message"></textarea>
			<button>Send</button>

		</form>

	</div>

	</footer>

</body>
</html>