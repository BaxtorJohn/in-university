package week2;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TP implements Runnable{
	int speed;
	int name;
	
	public TP( int name) {
		super();
		this.name = name;
		speed = new Random().nextInt(500) +500;
		
	}
	@Override
	public void run() {
		for (int i = 0; i < 20; i++) {
			System.out.println(name + "\t|===> " +i +" |==>" +speed);
		}
		
	}
public static void main(String[] args) {
//	ExecutorService pool = Executors.newFixedThreadPool(3);
	ExecutorService pool = Executors.newCachedThreadPool();
	for(int i=0; i < 5 ; i++) {
		pool.execute(new TP(i));
	}
	pool.shutdown();
	for(int i=0; i < 15 ; i++) {
		pool.execute(new TP(i));
	}
}


}
