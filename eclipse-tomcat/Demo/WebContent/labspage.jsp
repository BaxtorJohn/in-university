<%@page import="model.servlet.Products"%>
<%@page import="model.servlet.ProductDAO"%>

<%@page
	import="com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader.Array"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="menu.jsp"%>
	<p>This is a labpage</p>
	<%
		ArrayList<Products> productList = ProductDAO.getProList();
	%>

	<table border="1">
		<tr>
			<th>No</th>
			<th>ID</th>
			<th>Name</th>
			<th>Price</th>

		</tr>
		<%
			for (int i = 0; i < productList.size();) {
				Products p = productList.get(i);
		%>
		<tr>
			<td><%=i++%></td>
			<td><%=p.getId()%></td>
			<td><%=p.getName()%></td>
			<td><%=p.getPrice()%></td>
		</tr>

		<%
			}
		%>

	</table>
	<%@ include file="footer.jsp"%></body>
</html>