package week2;

import java.util.Scanner;

public class Exercise3 {
	// viet chuong trinh tinh tong cua 1 so tu nhien bat ky
	public static int sumInt(int x) {
		int sum = 0;
		while (x >= 0)
			sum += x % 10;

		return sum;
	}

	public static void main(String[] args) {
		System.out.println("sum: ");
		System.out.println("Sum of each number is: " + sumInt(218943));
		int n, tong = 0;
		System.out.println("quot;nhap vao so nguyen n&quot;");
		Scanner input = new Scanner(System.in);
		n = input.nextInt();
		while (n > 0) {
			tong += n % 10;
			n = n / 10;
		}
		System.out.println("tong cac chu so cua n la: " + tong);

	}
}
