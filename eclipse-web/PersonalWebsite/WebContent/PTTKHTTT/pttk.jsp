<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
<title>Phân tích thiết kế hệ thống thông tin</title>
</head>
<body>
<jsp:include page="../menu.jsp"></jsp:include>
<div style="text-align: center; margin-top:100px"><h1> Phân tích thiết kế hệ thống thông tin</h1></div>
<div style="margin-left: 20px">
	<h3>I/ Mục tiêu : </h3>
		<p>- Môn học hướng dẫn về tư duy, phương pháp và kỹ thuật để xây dựng 1 hệ thống thông tin </p>
		<p>VD : hệ thống đặt vé máy bay, hệ thống đăng ký học phần, hệ thống website bán hàng, hệ thống quản lý tin tức ,...</p>
		<p>- Chủ yếu là học về tư duy phân tích, thiết kế hệt thống thông tin như thế nào. Các bước xây dựng phần mềm. Các mô hình (Waterfall, Agile Scrum)</p>
		<p>Phân tích use case, vẽ sơ đồ use case, lượt đồ activity, lượt đồ sequence, viết tài liệu mô tả hệ thống ...</p>
	<h3>II/ Tài liệu :</h3>
		<h6>Chương trình đào tạo môn Phân tích hệ thống của trường DHNL gồm 9 slide, trong mỗi slide gồm nhiều phần nhỏ.</h6>
		<h6>Ngoài ra còn có các tài liệu mẫu của 1 số hệ thống mẫu.</h6>
		<a href="https://drive.google.com/drive/folders/1AWz01yoBpO9q3oiZsPyDc2yieUlM6Bej?usp=sharing" target="_blank"><p>- Slide Lecture</p></a>
		<a href="https://drive.google.com/open?id=12OMh-9EwJzWIl88NQefaOz-kS2SEPBls" target="_blank"><p>- Tài liệu mẫu</p></a>
	<h3>III/ Project báo cáo giữa kỳ :</h3>
		<h6>Xây dựng hệ thống website cho trung tâm Yoga & Fitness</h6>
		<a href="https://drive.google.com/open?id=1V1_qJ3eJevmNBdR0ze-eI3BWPFoWzxuZ" target="_blank"><p>Tài liệu phát thảo yêu cầu</p></a>
		<a href="https://drive.google.com/open?id=1BS02aoisl3j552OZGHFQf1ad_6FlMrJQ" target="_blank"><p>Tài liệu phân tích và mô tả use case</p></a>
		<a href="https://drive.google.com/open?id=1HmUU5rxY0vdlLD9RpUraXhZ5jzTEJgp-" target="_blank"><p>Glossary</p></a>
		<a href="usecase.jsp" target="_blank"><p>Sơ đồ use case</p></a>
		<a href="sequence.jsp" target="_blank"><p>Lượt đồ sequence</p></a>
		<a href="https://pttkhtttlinhthach.azurewebsites.net/"  target="_blank"><button type="button" class="btn btn-primary">Go to Website</button></a>
<footer class="footer">
		<jsp:include page="../footer.jsp"></jsp:include>
	</footer>
</body>
</html>