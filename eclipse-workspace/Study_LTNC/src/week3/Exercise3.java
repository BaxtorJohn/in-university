package week3;

import java.util.Arrays;
import java.util.Scanner;

public class Exercise3 {
	// viet chuong trinh nhap vao 1 mang bat ky va in ra so phan tu lon nhat, nho
	// nhat, va sap xep mang do theo thu tu tang dan
	public int[] createArray() {
		int size = 0;
		Scanner sc = new Scanner(System.in);

		// enter size
		do {
			System.out.print("Enter the size of array: ");
			size = sc.nextInt();
		} while (size < 0);

		int[] array = new int[size];
		// enter each element for array
		System.out.println("enter each element od array: ");
		for (int i = 0; i < size; i++) {
			System.out.print("Element " + i + ": ");
			array[i] = sc.nextInt();
		}
		// output array original'
		System.out.println("array original: ");
		print(array);

		// find maximum and minximize of array
		int max = array[0];
		int min = array[0];
		for (int i = 0; i < size; i++) {
			if (array[i] < min) {
				min = array[i];
			}
			if (array[i] > max) {
				max = array[i];
			}
		}
		System.out.println();
		System.out.println("Element minximize of array is: " + min);
		System.out.println("Element maximum of array is: " + max);

		// count element % 2 = 0
		int count = 0;
		int not =0;
		for (int i = 0; i < size; i++) {
			if (array[i] % 2 == 0) {
				count++;
			} else {
				not++;
			}
		}
		System.out.println("count element % 2 is : " + count);
		System.out.println("count element !% 2 is: "+not);

		// sort ascending
		Arrays.sort(array);
		System.out.println("Array after sort : ");
		print(array);

		return array;
	}

	public void print(int[] array) {
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + "\t");
		}
	}

	public static void main(String[] args) {
		Exercise3 e3 = new Exercise3();
		e3.createArray();
	}
}
