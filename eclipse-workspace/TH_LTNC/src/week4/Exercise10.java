package week4;

import java.util.Arrays;
import java.util.Random;

public class Exercise10 {
	// viet phuong thuc tra ve 1 mang so nguyen co qty phan ty ngau nhien bat ky
	public static int[] mangSoNguyen(int[] arr, int qty) {
		int[] kq = new int[qty];
		Random r = new Random();
		if (qty > arr.length) {
			System.out.println(" khong co phan tu tra ve ");
		} else {
			for (int i = 0; i < qty; i++) {
				int indexRandom = r.nextInt(arr.length);
				kq[i] = arr[indexRandom];
			}
		}
		return kq;
	}

	public static void main(String[] args) {
		int[] arr = { 3, 7, 2, 6, 8, 23, 14, 76, 55, 33 };
//		int[] arr= new int[10];
		int qty = 3;
		System.out.println(Arrays.toString(mangSoNguyen(arr, qty)));
	}

}
