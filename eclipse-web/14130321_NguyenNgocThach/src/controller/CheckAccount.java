package controller;

import java.io.IOException;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAO.AccountDAOImpl;
import model.Account;
import tool.EnCode;

/**
 * Servlet implementation class CheckAccount
 */
@WebServlet("/CheckAccount")
public class CheckAccount extends HttpServlet {
	private static final long serialVersionUID = 1L;
       private AccountDAOImpl accountDAO = new AccountDAOImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CheckAccount() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		String name = request.getParameter("name");
		String user = request.getParameter("user");
		String pass = request.getParameter("pass");
		
		String name_err = "", user_err=  "", pass_err=  "";
		
		if(name.equals("")) {
			name_err= "Vui lòng nhập Tên tài khoản";
		}
		if(name_err.length() > 0 ) {
			request.setAttribute("name_err", name_err);
		}
		if(user.equals("")) {
			user_err= "Vui lòng nhập Tên đăng nhập";
		} else if (accountDAO.checkAccount(user) == true) {
			user_err = "Tên đăng nhập đã tồn tài đã tồn tại";
		}
		if(user_err.length() > 0 ) {
			request.setAttribute("user_err", user_err);
		}
		if(pass.equals("")) {
			pass_err += "Vui lòng nhập mật khẩu";
		}
		if(pass_err.length() > 0 ) {
			request.setAttribute("pass_err", pass_err);
		}
		String url = "/login.jsp";
		
		request.setAttribute("name", name);
		request.setAttribute("user", user);
		request.setAttribute("pass", pass);
		
		try {
			if(name_err.length() == 0 && user_err.length() == 0 && pass_err.length() == 0 ) {
				Date idTime = new Date();
				Account account = new Account("" + idTime.getTime(), name, user, EnCode.enCodeMD5(pass), 2, 1);
				accountDAO.addAccount(account);
				url = "/index.jsp";
			}else {
				url="/login.jsp";
			}
			RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(url);
			requestDispatcher.forward(request, response);
		} catch (Exception e) {
			e.getStackTrace();
			RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/error.jsp");
			requestDispatcher.forward(request, response);
		}
	}

}
