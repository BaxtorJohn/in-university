<%@page import="DAO.CategoryDAOImpl"%>
<%@page import="model.Category" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Danh mục</title>
</head>
<body>
<%
	CategoryDAOImpl categoryDAO = new CategoryDAOImpl();
%>

<%
	for(Category categoryFarther : categoryDAO.getCategoryFarther()){
%>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="#sportswear">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											<%=categoryFarther.getName() %>
										</a>
									</h4>
								</div>
									<div class="panel-body">
										<ul>
										<%
											for(Category categoryChild : categoryDAO.getCategoryChild(categoryFarther.getCategory_id())){
										%>
											<li><a href="index.jsp?category_id=<%=categoryChild.getCategory_id()%>"><%=categoryChild.getName() %></a></li>
											
											<%} %>
										</ul>
									</div>
							</div>
							<%} %>
							
</body>
</html>