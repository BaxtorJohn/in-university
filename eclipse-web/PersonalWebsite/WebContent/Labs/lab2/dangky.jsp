<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Đăng ký</title>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
	$(function() {
		$("#datepicker").datepicker();
	});
</script>
</head>
<body>
	<style>
	body{
		font-family: Arial;
	})
* {
	box-sizing: border-box
}

/* Full-width input fields */
input[type=text], input[type=password] {
	width: 95%;
	padding: 15px;
	margin: 5px 0 22px 0;
	display: inline-block;
	border: none;
	background: #f1f1f1;
}

input[type=text]:focus, input[type=password]:focus {
	background-color: #ddd;
	outline: none;
}

hr {
	border: 1px solid #f1f1f1;
	margin-bottom: 25px;
}

/* Set a style for all buttons */
button {
	background-color: #4CAF50;
	color: white;
	padding: 14px 20px;
	margin: 8px 0;
	border: none;
	cursor: pointer;
	width: 100%;
	opacity: 0.9;
}

button:hover {
	opacity: 1;
}

/* Extra styles for the cancel button */
.cancelbtn {
	padding: 14px 20px;
	background-color: #f44336;
}

/* Float cancel and signup buttons and add an equal width */
.cancelbtn, .signupbtn {
	float: left;
	width: 50%;
}

/* Add padding to container elements */
.container {
	margin-left: 300px;
	padding: 16px;
	width: 700px;
	border: 2px groove red;
	border-radius: 12px;
	border-
}

/* Clear floats */
.clearfix::after {
	content: "";
	clear: both;
	display: table;
}

/* Change styles for cancel button and signup button on extra small screens */
@media screen and (max-width: 300px) {
	.cancelbtn, .signupbtn {
		width: 100%;
	}
}
</style>
<body>



	<form action="<%=request.getContextPath() %>/DangKy" style="border: 1px solid #ccc" method="post">
		<div class="container">
			<h1>Form đăng ký</h1>
			<p>Nhập thông tin của bạn để đăng ký</p>
			<hr>

			<label for="email"><b>Email</b></label> <input type="text"
				placeholder="...@gmail.com " name="email" required> 
			<label
				for="psw"><b>Mật khẩu</b></label> <input type="password"
				placeholder="nhập mật khâu" name="psw" required> 
			<label
				for="psw-repeat"><b>Nhập lại mật khẩu</b></label> <input
				type="password" placeholder="nhập lại mật khẩu" name="psw-repeat"
				required>
			<h3 style="color: green">--------------------------------------------------------------------------------------------------------------------</h3>
			<h2>Thông tin cá nhân</h2>
			<label><b>Họ và tên</b> </label> <input type="text" name="name">
			<label><b>Ngày sinh: </b></label> <input type="text" id="datepicker" name="date">
			<label><b>Giới tính:</b></label> <input type="radio" name="gioitinh"
				value="nam" checked="checked">Nam <input type="radio" name="gioitinh"
				value="nu">Nữ
			<p></p>
			<label><b>Địa chỉ: </b> </label> <input type="text" name="diachi"
				placeholder="số nhà, đường, phường, quận, tỉnh/TP"> <label><b>Ghi
					chú: </b></label>
			<textarea rows="4" cols="100" name="ghichu"></textarea><br>
			<label> <input type="checkbox" checked="checked"
				name="remember" style="margin-bottom: 15px"> Ghi nhớ
			</label>

			<p>
				Quay lại trang <a href="../lab1/dangnhap.jsp"
					style="color: dodgerblue">Đăng nhập</a>.
			</p>

			<div class="clearfix">
				<a href="../lab1/dangnhap.jsp"><button type="button" class="cancelbtn">Thoát</button></a>
				<button type="submit" class="signupbtn">Đăng ký</button>
			</div>
		</div>
	</form>
</body>
</html>