<%@page import="DAO.ProductDAOImpl"%>
<%@page import="model.Product"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<title>admin</title>
<meta charset="utf-8">
<!-- Bootstrap -->
<!-- Bootstrap -->
<link href="vendors/bootstrap/dist/css/bootstrap.min.css"
	rel="stylesheet">
<!-- Font Awesome -->
<link href="vendors/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">

<!-- Datatables -->
<link href="vendors/datatables.net-bs/css/dataTables.bootstrap.min.css"
	rel="stylesheet">
<link
	href="vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css"
	rel="stylesheet">
<link
	href="vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css"
	rel="stylesheet">
<link
	href="vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css"
	rel="stylesheet">
<link
	href="vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css"
	rel="stylesheet">


</head>
<body>

	<div class="container">

		<div class="">
			<center>
				<strong>Chọn họ mà bạn muốn tìm :<jsp:include
						page="search.jsp"></jsp:include>
				</strong>
			</center>

		</div>

		<div>
			<%
				ProductDAOImpl productDAO = new ProductDAOImpl();
				ArrayList<Product> listP = productDAO.getListProduct();
			%>

			<a class="btn btn-primary" href="formAddProduct.jsp"> + Thêm sản
				phẩm</a>
			<table id="datatable-buttons"
				class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Số thứ tự</th>
						<th>Mã sản phẩm</th>
						<th>Mã danh mục</th>
						<th>Tên sản phẩm</th>
						<th>Hình ảnh</th>
						<th>Giá</th>
						<th></th>
					</tr>
				</thead>

				<tbody>
					<%
						for (int i = 0; i < listP.size(); i++) {
					%>
					<tr>

						<td><%=i + 1%></td>
						<td><%=listP.get(i).getProduct_id()%></td>
						<td><%=listP.get(i).getCategory_id()%></td>
						<td><%=listP.get(i).getName()%></td>
						<td><img alt="" src="<%=listP.get(i).getImages()%>"
							style="height: 150px; width: auto"></td>
						<td><%=listP.get(i).getPrice()%></td>
						<td><a href="formEditProduct.jsp?command=edit&product_id=<%=listP.get(i).getProduct_id() %>" class="btn btn-warning sua"
							title="Chỉnh sửa sản phẩm"><i class="fa fa-retweet"
								aria-hidden="true"></i></a>
								 <a href="EditProduct?command=delete&product_id=<%=listP.get(i).getProduct_id() %>" class="btn btn-danger"><i
								class="fa fa-times" aria-hidden="true"></i></a></td>

					</tr>
					<%
						}
					%>
				</tbody>
			</table>
		</div>
	</div>
</body>

<!-- jQuery -->

<script src="vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->

<!-- Datatables -->
<script src="vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script
	src="vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script
	src="vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="vendors/datatables.net-buttons/js/buttons.print.min.js"></script>

<!-- Custom Theme Scripts -->
<script src="build/js/custom.min.js"></script>
</html>