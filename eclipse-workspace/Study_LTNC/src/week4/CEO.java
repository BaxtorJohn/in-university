package week4;

public class CEO {
private String name;
private double salary;
public CEO(String name, double salary) {
	super();
	this.name = name;
	this.salary = salary;
}
public String getName() {
	return name;
}
public double getSalary() {
	return salary;
}

}
