package lab.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import lab.model.Customer;
import lab.model.Product;
import lab.model.ProductDAO;

/**
 * Servlet implementation class BuyProduct
 */
@WebServlet("/BuyProduct")
public class BuyProduct extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyProduct() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			HttpSession session = request.getSession();
			if(session.getAttribute("customer") ==null) {
				RequestDispatcher rd = getServletContext().getRequestDispatcher("/Labs/lab5/login.jsp");
				rd.forward(request, response);
			}else {
				int index = Integer.parseInt(request.getParameter("index"));
				Customer customer = (Customer) session.getAttribute("customer");
				Product product = ProductDAO.getListProduct().get(index);
				customer.addShoppingCart(product);
				//chuyển đến trang giỏ hàng
				RequestDispatcher rd = getServletContext().getRequestDispatcher("/Labs/lab5/cart.jsp");
				rd.forward(request, response);
			}
		} catch (Exception e) {
			
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
