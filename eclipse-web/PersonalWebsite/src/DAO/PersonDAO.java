package DAO;

import java.util.ArrayList;
import java.util.Date;

import lab.model.Person;

public class PersonDAO {
public static ArrayList<Person> getPeople() {
	ArrayList<Person> list = new ArrayList<Person>();
	list.add(new Person("NV001","Phạm Văn Tính","1, 1, 1980", "Thái Bình"));
	list.add(new Person("NV002","Trần Thị Thanh Nga","2, 2, 1982", "Thái Bình"));
	list.add(new Person("NV003","Nguyễn Văn An","3, 3, 1983", "Thái Bình"));
	list.add(new Person("NV004","Mai Anh Thơ","4, 4, 1984", "Thái Bình"));
	list.add(new Person("NV005","Lê Văn Luyện","5, 5, 1985", "Thái Bình"));
	list.add(new Person("NV006","Nguyễn Hải Dương","6, 6, 1986", "Thái Bình"));
	return list;
}


}
