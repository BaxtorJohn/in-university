<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page import="java.util.HashMap" %>
    <%@page import="java.util.Map"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title> Đăng nhập </title>
</head>
<style>
body{
	font-family: Arial;
}
	 /* Bordered form */
form {
	margin-left: 300px;
    border: 3px solid red;
    width: 700px;
	border-radius: 12px;
}

/* Full-width inputs */
input[type=text], input[type=password] {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    box-sizing: border-box;
}

/* Set a style for all buttons */
button {
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 100%;
}

/* Add a hover effect for buttons */
button:hover {
    opacity: 0.8;
}

/* Extra style for the cancel button (red) */
.cancelbtn {
    width: auto;
    padding: 10px 18px;
    background-color: #f44336;
}

/* Center the avatar image inside this container */
.imgcontainer {
    text-align: center;
    margin: 24px 0 12px 0;
    
}

/* Avatar image */
img.avatar {
    width: 20%;
    height:auto;
    border-radius: 50%;
}

/* Add padding to containers */
.container {
    padding: 16px;
}

/* The "Forgot password" text */
span.psw {
    float: right;
    padding-top: 16px;
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
    span.psw {
        display: block;
        float: none;
    }
    .cancelbtn {
        width: 100%;
    }
} 
font{
color:red;
}
h4{
color:green;
}
</style>
<body>
<%
	Map<String, String> s = (Map<String, String>) request.getAttribute("errors");
	if (s == null) {
		s = new HashMap<>();
	}
%>
 <form action="<%=request.getContextPath()%>/XuLyForm" method="post">
 <h1 style="text-align: center">Xử lý form đăng nhập</h1>
  <div class="imgcontainer">
    <img src="<%=request.getContextPath()%>/images/hinh1.jpg" alt="Avatar" class="avatar">
  </div>

  <div class="container">
    <label for="uname"><b><span style="color:red">*</span>Tài khoản</b></label>
    <input type="text" placeholder="xxxx@gmail.com" name="uname" >
				<font> <%
 	if (s.containsKey("uname_err")) {
 %> <%=s.get("uname_err").toString()%> <%
 	}
 %>	</font>
<p></p>

    <label for="psw"><b><span style="color:red">*</span>Mật khẩu</b></label>
    <input type="password" placeholder="nhập mật khẩu của bạn" name="pwd" >
    <font> <%
 	if (s.containsKey("pwd_err")) {
 %> <%=s.get("pwd_err").toString()%> <%
 	}
 %>	</font>
 <p></p>
 
    <label for="psw"><b><span style="color:red">*</span>Nhập lại mật khẩu</b></label>
    <input type="password" placeholder="nhập lại mật khẩu của bạn" name="pwdRP" >
    <font> <%
 	if (s.containsKey("pwdRP_err")) {
 %> <%=s.get("pwdRP_err").toString()%> <%
 	}
 %>	</font>
 <font> <%
 	if (s.containsKey("pwdRP_incor")) {
 %> <%=s.get("pwdRP_incor").toString()%> <%
 	}
 %>	</font>
 

<h4>-------------------------------------------------------------------------------------------------------------------------------------</h4>
	<h2>Thông tin cá nhân</h2>
	<label><b><span style="color:red">*</span>Họ và tên</b></label>
	<input type="text" name="name">
	<font> <%
 	if (s.containsKey("name_err")) {
 %> <%=s.get("name_err").toString()%> <%
 	}
 %>	</font>
	<p></p>
	
	<label><b>Giới tính</b></label>
	<input type="radio" name="sex" value="nam" checked="checked">nam
	<input type="radio" name="sex" value="nu">nữ
	<p></p>
	<label><b>Ngày sinh </b></label>
	<select name="day">
		<% for(int i =1; i <=31;i++){
				out.print("<option value=\""+i+"\">" +i+"</option>");
			%>
		<%}%>
	</select>
	<select name="month">
		<% for(int i =1; i <=12;i++){
				out.print("<option value=\""+i+"\">" +i+"</option>");
			%>
		<%}%>
	</select>
	<select name="year">
		<% for(int i =1970; i <=2018;i++){
				out.print("<option value=\""+i+"\">" +i+"</option>");
			%>
		<%}%>
	</select>
	<p></p>
	<label><b><span style="color:red">*</span>Số điện thoại</b></label>
	<input type="text" name="phone">
	<font> <%
 	if (s.containsKey("phone_err")) {
 %> <%=s.get("phone_err").toString()%> <%
 	}
 %>	</font>
	<p></p>
	<label><b>Số di động</b></label>
	<input type="text" name="TelPhone">
	
	<h4>-------------------------------------------------------------------------------------------------------------------------------------</h4>
	<h2>Địa chỉ</h2>
	<label><b>Quốc gia</b></label>
	<select name="country">
		<option>Việt Nam</option>
		<option>Lào</option>
		<option>Campuchia</option>
		<option>Trung Quốc </option>
		<option>Hàn Quốc</option>
		<option>Nhật Bản </option>
	</select>
	<label><b>Tỉnh/Thành phố</b></label>
	<select name="city">
		<option>TPHCM</option>
		<option>Hà Nội</option>
		<option>An Giang</option>
		<option>Cần Thơ </option>
		<option>Long An</option>
		<option>Đà Nẵng </option>
	</select>
	<label><b>Quận/Huyện</b></label>
	<select name="district">
		<option>Q1</option>
		<option>Q2</option>
		<option>Q3</option>
		<option>Thủ Đức </option>
		<option>Tân Bình</option>
		<option>Cần Giờ </option>
	</select>
	<p></p>
	<label><b>Địa chỉ cụ thể</b></label>
	<textarea rows="6" cols="95" name="address"></textarea>
    <button type="submit">Login</button>
      <a href="dangky.jsp">  <button type="submit" style="background-color: #ff6699">Register </button></a>
    
    <label>
      <input type="checkbox" checked="checked" name="remember"> Remember me
    </label>
  </div>

  <div class="container" style="background-color:#f1f1f1">
    <button type="button" class="cancelbtn">Cancel</button>
    <span class="psw">Forgot <a href="../lab1/quenmatkhau.jsp">password?</a></span>
  </div>
</form> 

</body>
</html>