package com.model;

import java.util.ArrayList;

public class Labs {
private int week;
private String name;
private String link;
private String Demo;

public Labs(int week, String name, String link, String demo) {
	super();
	this.week = week;
	this.name = name;
	this.link = link;
	this.Demo = demo;
}
public int getWeek() {
	return week;
}
public String getName() {
	return name;
}
public String getLink() {
	return link;
}
public String getDemo() {
	return Demo;
}


}
