package week3.duck1;

public class DecoyDuck extends Duck{
public void quack() {
	System.out.println("Nothing");
}
public void display() {
	System.out.println("I am a decoy duck!!");
}
public void fly() {
	System.out.println("I can not fly");
}
}
