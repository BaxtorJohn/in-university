<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Thêm sản phẩm</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
</head>
<body>
<jsp:include page="menu.jsp"></jsp:include>
<div id="contact-page" class="container">
    	<div class="bg">
    		<div class="row">  	
	    		<div class="col-sm-8">
	    			<div class="contact-form">
	    				<h2 class="title text-center">Thêm sản phẩm </h2>
	    				<div class="status alert alert-success" style="display: none"></div>
				    	<form action="EditProduct" id="main-contact-form" class="contact-form row" name="contact-form" method="post">
				            <div class="form-group col-md-6"> Mã sản phẩm
				                <input type="text" name="product_id" class="form-control" required="required" placeholder="Mã sản phẩm">
				            </div>
				            <div class="form-group col-md-6">Mã danh mục
				                <input type="text" name="category_id" class="form-control" required="required" placeholder="Mã danh mục">
				            </div>
				            <div class="form-group col-md-12">Tên sản phẩm
				                <input type="text" name="name" class="form-control" required="required" placeholder="Tên sản phẩm">
				            </div>
				            <div class="form-group col-md-12">Hình ảnh
				                <input type="text" name="images" class="form-control" required="required" placeholder="Hình ảnh">
				            </div>           
				            <div class="form-group col-md-12">Giá
				                <input type="text" name="price" class="form-control" required="required" placeholder="Giá">
				            </div>                
				            <div class="form-group col-md-12">
				           		<input type="hidden" name="command" value="add">
				                <input type="submit" name="submit" class="btn btn-primary pull-right" value="Thêm sản phẩm">
				            </div>
				        </form>
	    			</div>
	    		</div>
	    	</div>  
    	</div>	
    </div><!--/#contact-page-->
</body>
</html>