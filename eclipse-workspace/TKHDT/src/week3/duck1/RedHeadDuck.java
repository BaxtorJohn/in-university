package week3.duck1;

public class RedHeadDuck extends Duck implements Flyable, Quackable {

	public void display() {
		System.out.println("I am a red head duck!!");
	}

	@Override
	public void quack() {
		System.out.println("Quack...quack");
	}

}
