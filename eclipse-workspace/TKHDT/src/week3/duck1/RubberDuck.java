package week3.duck1;

public class RubberDuck extends Duck implements Quackable{
public void quack() {
	System.out.println("Quick...quick...");
}
public void display() {
	System.out.println("I am a rubber duck!!");
}
public void fly() {
	System.out.println("I can not fly");
	
}
}
