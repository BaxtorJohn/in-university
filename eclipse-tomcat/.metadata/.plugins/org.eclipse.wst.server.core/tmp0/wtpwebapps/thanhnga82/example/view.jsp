<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.model.dao.ExampleDAO"%>
<%@page import="com.model.Example"%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
</head>
<body>
	<%
		//Tạo danh sách person - sau này sẽ lấy từ database lên
		ArrayList<Example> lstExamples = ExampleDAO.getExampleList();
	%>
	<p><%=lstExamples.size()%>
	<div class="container">
		<h2>Các ví dụ</h2>
		<div class="table-responsive">
			<table class="table">
				<thead>
					<tr>
						<th>#</th>
						<th>Title</th>
						<th>Download</th>
					</tr>
				</thead>
				<%
					int count = 0;
									//Hiển thị dữ liệu lên table, ở đây dùng for each
									//Trong ví dụ này thì nên dùng for vì có cột số thứ tự
									for (Example p : lstExamples) {
										count++;
				%>
				<tbody>
					<tr>
						<td><%=count%></td>
						<td><%=p.getTitle()%></td>
						<td><a target="_blank" href="<%=p.getContent()%>" class="btn btn-primary">Download</a></td>
						
					</tr>
				</tbody>
				<%
					}
				%>
			</table>
		</div>
	</div>
</body>
</html>