package lab.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Check
 */
@WebServlet("/Check")
public class Check extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Check() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String user = request.getParameter("user");
		String pass = request.getParameter("pass");
		
		if(!user.isEmpty() && !pass.isEmpty()) {
		HttpSession session = request.getSession();
		session.setAttribute("user", user);
		session.setAttribute("pass", pass);
		RequestDispatcher rd = getServletContext().getRequestDispatcher("/Labs/lab5/cart.jsp");
		rd.forward(request, response);
		}else {
			response.sendRedirect("Labs/lab5/login.jsp");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String user = request.getParameter("user");
		String pass = request.getParameter("pass");
		
		if(!user.isEmpty() && !pass.isEmpty()) {
		HttpSession session = request.getSession();
		session.setAttribute("user", user);
		session.setAttribute("pass", pass);
		RequestDispatcher rd = getServletContext().getRequestDispatcher("/Labs/lab5/cart.jsp");
		rd.forward(request, response);
		}else {
			response.sendRedirect("Labs/lab5/login.jsp");
		}
	}

}
