package week4;

public class Exercise6 {
	//tim USCLN
	public static int USCLN(int a, int b) {
		if(b==0) {
			return a;
		}else {
			return USCLN(b, a%b);
		}
	}
	
	//tim BSCNN
	public static int BSCNN(int a, int b) {
		return (a*b) / USCLN(a, b);
	}
	public static void main(String[] args) {
		System.out.println("USCLN: " + USCLN(15, 9));
		System.out.println("BSCNN: " + BSCNN(15, 9));
	}
}
