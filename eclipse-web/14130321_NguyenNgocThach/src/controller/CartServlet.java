package controller;

import java.io.IOException;

import java.lang.ref.ReferenceQueue;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.ProductDAOImpl;
import model.Cart;
import model.Product;
/**
 * Servlet implementation class CartServlet
 */
@WebServlet("/CartServlet")
public class CartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
      private ProductDAOImpl productDAO = new ProductDAOImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CartServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("UTF-8");
		
		HttpSession session = request.getSession();
		Cart cart = (Cart) session.getAttribute("cart");
		String product_id = request.getParameter("product_id");
		String command = request.getParameter("command");
		
		ArrayList<Long> listBuy = null;
		String url ="/cart.jsp";
		try {
			listBuy = (ArrayList<Long>) session.getAttribute("cartID");
			long idBuy = 0;
			if(request.getParameter("cartID") !=null) {
				idBuy = Long.parseLong(request.getParameter("cartID"));
				
			}
			
			Product product = productDAO.getProduct(product_id);
			switch (command) {
			case "insert":
				if(listBuy == null) {
					listBuy = new ArrayList<Long>();
					session.setAttribute("cartID", listBuy);
					
				}
				if(listBuy.indexOf(idBuy) == -1) {
					cart.insertToCart(product, 1);
					listBuy.add(idBuy);
				}
				url ="/cart.jsp";
				break;
			case "plus":
				if(listBuy== null) {
					listBuy = new ArrayList<>();
					session.setAttribute("cart", listBuy);
				}
				if(listBuy.indexOf(idBuy) == -1) {
					cart.insertToCart(product, 1);
					listBuy.add(idBuy);
				}
				url ="/cart.jsp";
				break;
			case "sub":
				if(listBuy== null) {
					listBuy = new ArrayList<>();
					session.setAttribute("cart", listBuy);
				}
				if(listBuy.indexOf(idBuy) == -1) {
					cart.subToCart(product, 1);
					listBuy.add(idBuy);
				}
				url ="/cart.jsp";
				break;
			case "remove":
				cart.removeToCart(product);
				url ="/cart.jsp";
				break;
			default:
				url ="/cart.jsp";
				break;
			}
			RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(url);
			requestDispatcher.forward(request, response);
		} catch (Exception e) {
			e.getStackTrace();
			RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/error.jsp");
			requestDispatcher.forward(request, response);
		}
	}

}
