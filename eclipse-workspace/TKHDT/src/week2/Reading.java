package week2;

import week1.Date;

public abstract class Reading {
private Date date;
private int index;
public Reading(Date date, int index) {
	super();
	this.date = date;
	this.index = index;
}
public Date getDate() {
	return date;
}
public int getIndex() {
	return index;
}
public void setDate(Date date) {
	this.date = date;
}
public void setIndex(int index) {
	this.index = index;
}

}
