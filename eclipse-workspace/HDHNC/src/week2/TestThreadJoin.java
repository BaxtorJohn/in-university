package week2;
import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.UnsupportedLookAndFeelException;

public class TestThreadJoin {
	private JLabel countLabel1 = new JLabel("0");
	 private JLabel statusLabel = new JLabel("Task not completed.");
	 private JButton startButton = new JButton("Start");

	 public TestThreadJoin(String title) {
	  super(title);

	  setLayout(new GridBagLayout());
	  
	  countLabel1.setFont(new Font("serif", Font.BOLD, 28));

	  GridBagConstraints gc = new GridBagConstraints();

	  gc.fill = GridBagConstraints.NONE;

	  gc.gridx = 0;
	  gc.gridy = 0;
	  gc.weightx = 1;
	  gc.weighty = 1;
	  add(countLabel1, gc);

	  gc.gridx = 0;
	  gc.gridy = 1;
	  gc.weightx = 1;
	  gc.weighty = 1;
	  add(statusLabel, gc);

	  gc.gridx = 0;
	  gc.gridy = 2;
	  gc.weightx = 1;
	  gc.weighty = 1;
	  add(startButton, gc);

	  startButton.addActionListener(new ActionListener() {
	   public void actionPerformed(ActionEvent arg0) {
	    start();
	   }
	  });

	  setSize(200, 400);
	  setDefaultCloseOperation(EXIT_ON_CLOSE);
	  setVisible(true);
	 }

	 private void start() {
	  
	  
	 }
	 
	 public static void main(String[] args) {
	  SwingUtilities.invokeLater(new Runnable() {
	   
	   @Override
	   public void run() {
	    new MainFrame("SwingWorker Demo");
	   }
	  });
	 }
}
