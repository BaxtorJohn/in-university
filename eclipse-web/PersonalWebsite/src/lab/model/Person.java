package lab.model;

import java.util.Date;

public class Person {
private String id;
private String name;
private String date;
private String country;
public Person(String id, String name, String date, String country) {
	super();
	this.id = id;
	this.name = name;
	this.date = date;
	this.country = country;
}
public String getId() {
	return id;
}
public String getName() {
	return name;
}
public String getDate() {
	return date;
}
public String getCountry() {
	return country;
}


}
