<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Footer</title>
<link rel="stylesheet"
	href="css/footer-distributed-with-contact-form.css">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
</head>
<body>
<div class="section" id="contact">
				<div class="cc-contact-information"
					style="background-image: url('images/staticmap.png');">
					<div class="container">
						<div class="cc-contact">
							<div class="row">
								<div class="col-md-9">
									<div class="card mb-0" data-aos="zoom-in">
										<div class="h4 text-center title">Contact Me</div>
										<div class="row">
											<div class="col-md-6">
												<div class="card-body">
													<form action="https://formspree.io/your@email.com"
														method="POST">
														<div class="p pb-3">
															<strong>Feel free to contact me </strong>
														</div>
														<div class="row mb-3">
															<div class="col">
																<div class="input-group">
																	<span class="input-group-addon"><i
																		class="fa fa-user-circle"></i></span> <input
																		class="form-control" type="text" name="name"
																		placeholder="Name" required="required" />
																</div>
															</div>
														</div>
														<div class="row mb-3">
															<div class="col">
																<div class="input-group">
																	<span class="input-group-addon"><i
																		class="fa fa-file-text"></i></span> <input
																		class="form-control" type="text" name="Subject"
																		placeholder="Subject" required="required" />
																</div>
															</div>
														</div>
														<div class="row mb-3">
															<div class="col">
																<div class="input-group">
																	<span class="input-group-addon"><i
																		class="fa fa-envelope"></i></span> <input
																		class="form-control" type="email" name="_replyto"
																		placeholder="E-mail" required="required" />
																</div>
															</div>
														</div>
														<div class="row mb-3">
															<div class="col">
																<div class="form-group">
																	<textarea class="form-control" name="message"
																		placeholder="Your Message" required="required"></textarea>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col">
																<button class="btn btn-primary" type="submit">Send</button>
															</div>
														</div>
													</form>
												</div>
											</div>
											<div class="col-md-6">
												<div class="card-body">
													<p class="mb-0">
														<strong>Address </strong>
													</p>
													<p class="pb-2">KTX B DHQG TPHCM</p>
													<p class="mb-0">
														<strong>Phone</strong>
													</p>
													<p class="pb-2">0981669705</p>
													<p class="mb-0">
														<strong>Email</strong>
													</p>
													<p>nguyenngocthach@company.com</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<div class="container text-center">
			<a class="cc-facebook btn btn-link" href="#"><i
				class="fa fa-facebook fa-2x " aria-hidden="true"></i></a><a
				class="cc-twitter btn btn-link " href="#"><i
				class="fa fa-twitter fa-2x " aria-hidden="true"></i></a><a
				class="cc-google-plus btn btn-link" href="#"><i
				class="fa fa-google-plus fa-2x" aria-hidden="true"></i></a><a
				class="cc-instagram btn btn-link" href="#"><i
				class="fa fa-instagram fa-2x " aria-hidden="true"></i></a>
		</div>
		<div class="h4 title text-center">Nguyễn Ngọc Thạch</div>
		<div class="text-center text-muted">
			
		</div>
</body>
</html>