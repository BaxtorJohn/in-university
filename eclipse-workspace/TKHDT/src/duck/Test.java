package duck;

public class Test {
public static void main(String[] args) {
	Duck mallar = new MallarDuck();
	mallar.performFly();
	mallar.performQuack();
}
}
