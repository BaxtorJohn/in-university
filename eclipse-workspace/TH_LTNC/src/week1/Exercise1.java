package week1;

import java.util.Scanner;

public class Exercise1 {
	// Xu ly so chan le
	public String Concept(int num) {
		if (num % 2 == 0)
			return "so chan";
		else
			return "so le";

	}

	// bt mau while loop
	public void whileLoop(int x) {
		while (x > 1) {
			System.out.println(x);
			x--;
		}
	}

	// bt mau for loop
	public void forLoop(int y) {
		for (int i = 0; i <= y; i++) {
			System.out.println(i);
		}
	}

	// xac dinh so nguyen to
	public boolean prime(int number) {
		if (number < 2) {
			return false;

		}
		// check so nguyen to khi n > = 2
		int squareRoot = (int) Math.sqrt(number);
		for (int i = 2; i <= squareRoot; i++) {
			if (number % i == 0) {
				return false;
			}
		}
		return true;
	}

	// cach 2 : sai roi
	public boolean ktSoNT(int z) {
		// so < 2 ko phai
		if (z < 2) {
			return false;
		} else {
			if (z > 2) {
				// so chan ko phai
				if (z % 2 == 0)
					return false;
				for (int i = 3; i <= (z - 1); i += 2) {
					// so le chia het cho bat ky so nao thi ko phai
					if (z % i == 0)
						return false;
					return true;
				}

			}
		}
		return true;
	}

	public static void main(String[] args) {
		Exercise1 e = new Exercise1();
		System.out.println("kiem tra chan le: \n" + e.Concept(5));
		// System.out.println();
		// Scanner sc = new Scanner(System.in);
		// System.out.print("nhap vao 1 so: ");
		// int num = sc.nextInt();
		// e.Concept(num);

		// test while loop
		// System.out.println("vong lap while: ");
		// e.whileLoop(18);
		// test for loop
		System.out.println("vong lap for : ");
		e.forLoop(10);

		// test so nguyen to
		System.out.println("------kiem tra so nguyen to ----");
		System.out.println(e.prime(9));
		System.out.println(e.prime(1));
		System.out.println(e.prime(2));
		System.out.println(e.prime(15));
		System.out.println(e.prime(17));
		System.out.println(e.prime(29));

		System.out.println("----cach 2----");
		System.out.println(e.ktSoNT(9));
		System.out.println(e.ktSoNT(1));
		System.out.println(e.ktSoNT(2));
		System.out.println(e.ktSoNT(15));
		System.out.println(e.ktSoNT(17));
		System.out.println(e.ktSoNT(29));

	}
}
