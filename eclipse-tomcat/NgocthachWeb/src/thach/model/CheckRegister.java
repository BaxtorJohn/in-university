package thach.model;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.sun.org.apache.xalan.internal.xsltc.compiler.util.ClassGenerator;
import com.sun.org.apache.xalan.internal.xsltc.compiler.util.MethodGenerator;
import com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type;
import com.sun.org.apache.xalan.internal.xsltc.compiler.util.TypeCheckError;

public class CheckRegister {
	private String email;
	private String reEmail;
	private String pass;
	private String rePass;
	private String name;
	private String phone;
	private String street;
	private String city;
	private String country;
	private String web;
	private String gender;
	private String date;
	private String month;
	private String year;
	private String student;
	Map<String, String> errors;

	public CheckRegister(String email, String reEmail, String pass, String rePass, String name, String phone,
			String street, String city, String country, String web, String gender, String date, String month,
			String year,String student) {
		super();
		this.email = email;
		this.reEmail = reEmail;
		this.pass = pass;
		this.rePass = rePass;
		this.name = name;
		this.phone = phone;
		this.street = street;
		this.city = city;
		this.country = country;
		this.web = web;
		this.gender = gender;
		this.date = date;
		this.month = month;
		this.year = year;
		this.student=student;
		
		errors = new HashMap<String, String>();
		checkError();
		checkEmail();
		checkReEmail();
		checkPass();
		checkRePass();
		checkName();
		checkGender();
		checkPhone();
		checkCity();
	}

	// check email
	public void checkEmail() {
		if ((email == null) || (email == "")) {
			errors.put("email_err", "please enter email!");
		}
	}

	public void checkReEmail() {
		if ((reEmail == null) || (reEmail == "")) {
			errors.put("reEmail_err", "please enter email againt!");
		} else if (!reEmail.equals(email)) {
			errors.put("reEmail_incor", "email not match");
		}
	}

	// check password
	public void checkPass() {
		if ((pass == null) || (pass == "")) {
			errors.put("pass_err", "please enter password!");
		}
	}

	public void checkRePass() {
		if ((rePass == null) || (rePass == "")) {
			errors.put("rePass_err", "please enter password againt!");
		} else if (!rePass.equals(pass)) {
			errors.put("rePass_incor", "password not match");
		}
	}

	// check nanme
	public void checkName() {
		if ((name == null) || (name == "")) {
			errors.put("name_err", "please enter name!");
		}
	}

	// check phone
	public void checkPhone() {
		if ((phone == null) || (phone == "")) {
			errors.put("phone_err", "please enter phone numbers!");
		}
		Pattern pattern = Pattern.compile("\\d.{9,10}");
		Matcher matcher = pattern.matcher(phone);
		if (!matcher.matches()) {
			errors.put("phone_err", "phone illegal. Phone numbers must from 10-11 number");
		}
	}

	// check country
	public void checkCity() {
		if ((city == null) || (city == "")) {
			errors.put("city_err", "please enter city!");
		}
	}

	// check gender
	public void checkGender() {
		if ((gender == null) || (gender == "")) {
			errors.put("gender_err", "please choose gender!");
		}
	}

	// check co loi ko?
	public boolean checkError() {
		return this.errors.isEmpty();
	}

	public String getEmail() {
		return email;
	}

	public String getReEmail() {
		return reEmail;
	}

	public String getPass() {
		return pass;
	}

	public String getRePass() {
		return rePass;
	}

	public String getName() {
		return name;
	}

	public String getPhone() {
		return phone;
	}

	public String getStreet() {
		return street;
	}

	public String getCity() {
		return city;
	}

	public String getCountry() {
		return country;
	}

	public String getWeb() {
		return web;
	}

	public String getGender() {
		return gender;
	}

	public String getDate() {
		return date;
	}

	public String getMonth() {
		return month;
	}

	public String getYear() {
		return year;
	}
public String getStudent() {
return student;
}
	public Map<String, String> getErrors() {
		return errors;
	}

}
