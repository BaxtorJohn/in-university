package DAO;

import model.Account;

public interface AccountDAO {
	
public boolean checkAccount(String email);

public Account addAccount(Account account);

public boolean checkLogin(String email, String pass );

public Account getAccount(String user);
}
