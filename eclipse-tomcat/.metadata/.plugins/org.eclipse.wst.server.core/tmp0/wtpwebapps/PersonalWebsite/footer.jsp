<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Footer</title>
<link rel="stylesheet"
	href="css/footer-distributed-with-contact-form.css">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
</head>
<body>
<div class="container text-center">
			<a class="cc-facebook btn btn-link" href="#"><i
				class="fa fa-facebook fa-2x " aria-hidden="true"></i></a><a
				class="cc-twitter btn btn-link " href="#"><i
				class="fa fa-twitter fa-2x " aria-hidden="true"></i></a><a
				class="cc-google-plus btn btn-link" href="#"><i
				class="fa fa-google-plus fa-2x" aria-hidden="true"></i></a><a
				class="cc-instagram btn btn-link" href="#"><i
				class="fa fa-instagram fa-2x " aria-hidden="true"></i></a>
		</div>
		<div class="h4 title text-center">Nguyễn Ngọc Thạch</div>
		<div class="text-center text-muted">
			
		</div>
</body>
</html>