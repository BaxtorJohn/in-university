package week3.duck2;

public class Test {
	public static void main(String[] args) {
	Duck marllard = new MarllardDuck();
	Duck redhead = new RedHeadDuck();
	Duck rubber = new RubberDuck();
	Duck decoy = new DecoyDuck();
	
	marllard.display();
//	marllard.performQuack();
	marllard.performFly();
	System.out.println();
	
//	redhead.display();
//	redhead.performQuack();
//	redhead.performFly();
//	System.out.println();
//
//	rubber.display();
//	rubber.performQuack();
//	rubber.performFly();
//	System.out.println();
//	
//	decoy.display();
//	decoy.performQuack();
//	decoy.performFly();
//	System.out.println();
//
//	Duck model = new ModelDuck();
//
//	model.display();
//    model.performFly();
//    model.setFlybehavior(new FlyRocketPowered());
//    model.performFly();
	}
}
