<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="model.Cart" %>
<%@page import="model.Product" %>
<%@page import="java.util.TreeMap" %>
<%@page import="java.util.Map" %>
    <%@page import="DAO.CategoryDAOImpl"%>
<%@page import="model.Category" %>
    <%@page import="DAO.ProductDAOImpl"%>
    <%@page import="java.text.NumberFormat"%>
    
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>Giỏ hàng</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/prettyPhoto.css" rel="stylesheet">
<link href="css/price-range.css" rel="stylesheet">
<link href="css/animate.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
<link rel="shortcut icon" href="images/ico/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="images/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="images/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="images/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed"
	href="images/ico/apple-touch-icon-57-precomposed.png">
</head>
<!--/head-->

<body>
	<%
		Cart cart = (Cart) session.getAttribute("cart");
	if(cart == null){
		cart = new Cart();
		session.setAttribute("cart", cart);
	}
	TreeMap<Product,Integer> list = cart.getListProduct();
	NumberFormat nf = NumberFormat.getInstance();
	nf.setMinimumIntegerDigits(0);
	%>
	<header id="header">
		<!--header-->
		<jsp:include page="menu.jsp"></jsp:include>
	</header>
	<!--/header-->

	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
					<li><a href="index.jsp">Trang chủ</a></li>
					<li class="active">Giỏ hàng của tôi</li>
				</ol>
			</div>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Hình ảnh</td>
							<td class="description">Tên sản phẩm</td>
							<td class="price">Giá</td>
							<td class="quantity">Số lượng</td>
							<td class="total">Thành tiền</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
					<%
							for(Map.Entry<Product,Integer> entry : list.entrySet()){
						%>
						<tr>
						
							<td class="cart_product"><a href=""><img
									src="<%=entry.getKey().getImages() %>" alt="hình ảnh" style="width: 100px;height: 150px"></a></td>
							<td class="cart_description">
								<h4>
									<a href=""><%=entry.getKey().getName() %></a>
								</h4>
								<p>Mã sản phẩm: <%=entry.getKey().getProduct_id()%></p>
							</td>
							<td class="cart_price">
								<p><%=nf.format(entry.getKey().getPrice()) %>VND</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<a class="cart_quantity_up" href="CartServlet?command=plus&product_id=<%=entry.getKey().getProduct_id()%>&cartID=<%=System.currentTimeMillis()%>"> + </a> <input
										class="cart_quantity_input" type="text" name="quantity"
										value="<%=entry.getValue() %>" autocomplete="off" size="2"> <a
										class="cart_quantity_down" href="CartServlet?command=sub&product_id=<%=entry.getKey().getProduct_id()%>&cartID=<%=System.currentTimeMillis()%>"> - </a>
								</div>
							</td>
							<td class="cart_total">
								<p class="cart_total_price"><%=nf.format(entry.getValue() * entry.getKey().getPrice()) %>VND</p>
							</td>
							<td class="cart_delete"><a class="cart_quantity_delete"
								href="CartServlet?command=remove&product_id=<%=entry.getKey().getProduct_id()%>&cartID=<%=System.currentTimeMillis()%>"><i class="fa fa-times"></i></a></td>
								
						</tr>
					<%} %>
					</tbody>
				</table>
				<a
							class="btn btn-default check_out" href="index.jsp">Tiếp tục mua hàng</a>
			</div>
		</div>
	</section>
	<!--/#cart_items-->

	<section id="do_action">
		<div class="container">
			<div class="heading">
				<h3>What would you like to do next?</h3>
				<p>Choose if you have a discount code or reward points you want
					to use or would like to estimate your delivery cost.</p>
			</div>
			<div class="row">
				
				<div class="col-sm-6">
					<div class="total_area">
						<a class="btn btn-default update" href="">Hủy đơn hàng</a> <a
							class="btn btn-default check_out" href="checkout.jsp">Thanh toán</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--/#do_action-->

	<footer id="footer">
		<jsp:include page="footer.jsp"></jsp:include>
	</footer>
	<!--/Footer-->



	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/jquery.prettyPhoto.js"></script>
	<script src="js/main.js"></script>
</body>
</html>