<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">

<head>

    <meta charset="utf-8" />
    <title>Fitness</title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <!--Favicon -->
	<link rel="icon" type="image/png" href="images/favicon.jpg" />
		
	<!-- CSS Files -->
		
	<link rel="stylesheet" href="css/reset.css" />
	<link rel="stylesheet" href="css/animate.min.css" />
	<link rel="stylesheet" href="css/bootstrap.min.css" />
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/font-awesome.css" />
	<link rel="stylesheet" href="css/owl.carousel.css" />
	<link rel="stylesheet" href="css/responsive.css" />
	<link rel="stylesheet" href="css/player/YTPlayer.css" />
	<link href="css/pro-bars.css" rel="stylesheet" />
	

	<!-- End CSS Files -->
 
</head>

<body>

	<!-- Navigation -->
	<section id="navigation" class="dark-nav">
		<!-- Navigation Inner -->
		<div class="nav-inner">
			<!-- Site Logo -->
			<div class="site-logo fancy">
				<a href="#" id="logo-text" class="scroll logo">Fitness
				</a>
			</div><!-- End Site Logo -->
			<a class="mini-nav-button gray2"><i class="fa fa-bars"></i></a>
			<!-- Navigation Menu -->
		    <div class="nav-menu">
				<ul class="nav uppercase">
					<li><a href="#home" class="scroll">Home</a></li>       
					<li><a href="About.jsp" class="scroll">About us</a></li>
					<li><a href="#features" class="scroll">Features</a></li>     
					<li><a href="#clients" class="scroll">Gymnasts</a></li>
					<li><a href="#testimonial" class="scroll">Testimonial</a></li>
					<li><a href="#contact" class="scroll">Contact</a></li>
				</ul>
		  </div><!-- End Navigation Menu -->
		</div><!-- End Navigation Inner -->
	</section><!-- End Navigation Section -->
  
    <!-- About -->
	<section id="about" class="container waypoint">
		<div class="inner">        
        
			<!-- Header -->
			<h1 class="header light gray3 fancy"><span class="colored">Know </span>about us</h1>

			<!-- Description -->
			<p class="h-desc gray4">lorem Ipsum is<span class="colored bold"> lorem Ipsum</span> of passages of Lorem Ipsum available, but the majority have suffered alteration.<br /><br /></p> 
			<hr>       
        
			<!-- Boxes -->
			<div class="boxes">

				
				<div class="col-xs-3 col-sm-6 col-md-3 about-box animated" data-animation="fadeIn" data-animation-delay="100">
				<p class="lead">Monitor</p>
				<hr><br>
					<a class="about-icon" href="cvthach.jsp" target="_blank">
						<i class="fa fa-stethoscope"></i>
					</a>
					<br><br>
					<p class="light about-text">Nguyen Ngoc Thach</p>
				</div>

				
				<div class="col-xs-3 col-sm-6 col-md-3 about-box animated" data-animation="fadeIn" data-animation-delay="300">
				<p class="lead">Member 1</p>
				<hr><br>
					<a class="about-icon" href="cvtuan.jsp" target="_blank">
						<i class="fa fa-wheelchair"></i>
					</a>
					<br><br>
					<p class="light about-text">Pham Quang Tuan</p>

				</div>
				<div class="col-xs-3 col-sm-6 col-md-3 about-box animated" data-animation="fadeIn" data-animation-delay="100">
				<p class="lead">Member 2</p>
				<hr><br>
					<a class="about-icon" href="cvtri.jsp" target="_blank">
						<i class="fa fa-stethoscope"></i>
					</a>
					<br><br>
					<p class="light about-text">Vo Xuan Tri</p>
				</div>

				
				<div class="col-xs-3 col-sm-6 col-md-3 about-box animated" data-animation="fadeIn" data-animation-delay="300">
				<p class="lead">Member 3</p>
				<hr><br>
					<a class="about-icon" target="_blank">
						<i class="fa fa-wheelchair"></i>
					</a>
					<br><br>
					<p class="light about-text">Unknow</p>
					
				</div>

				
				<div class="col-xs-12 col-md-6 col-sm-12 about-box animated" data-animation="fadeIn" data-animation-delay="700">
				<p class="lead lead-text">Best in the city</p><hr>
					<p class="left pro-bars" style="color: rgb(83, 205, 181);">Treadmill </p>
					<div class="pro-bar-container color-green-sea">
						<div class="pro-bar bar-100 color-turquoise" data-pro-bar-percent="100">
							<div class="pro-bar-candy candy-ltr"></div>
						</div>
					</div>
					<p class="left pro-bars" style="color: #3498DB;">Spinning </p>
					<div class="pro-bar-container color-belize-hole">
						<div class="pro-bar bar-80 color-peter-river" data-pro-bar-percent="80" data-pro-bar-delay="200">
							<div class="pro-bar-candy candy-ltr"></div>
						</div>
					</div>
					<p class="left pro-bars" style="color: #B483C8;">Cardio </p>
					<div class="pro-bar-container color-wisteria">
						<div class="pro-bar bar-70 color-amethyst" data-pro-bar-percent="70" data-pro-bar-delay="300">
							<div class="pro-bar-candy candy-ltr"></div>
						</div>
					</div>
				</div>
			</div><!-- End Boxes -->
		</div><!-- End About Inner -->
	</section><!-- End About Section -->


	<!-- Contact Section -->
	<section id="contact" class="container parallax4">
		<!-- Contact Inner -->
		<div class="inner contact">

			<!-- Form Area -->
			<div class="contact-form">
            
            	<h4 class="header light gray3 fancy"><span class="colored">Contact</span> Us</h4>
                <p class="h-desc white">lorem Ipsum is lorem Ipsum of passages of Lorem Ipsum available, but the majority have suffered alteration.<br />
                Email us or give us a call at <span class="bold colored">+1 (800) 245-1234.</span></p>
				<!-- Form -->
				<form id="contact-us" method="post" action="#">
					<!-- Left Inputs -->
					<div class="col-xs-6 animated" data-animation="fadeInLeft" data-animation-delay="300">
						<!-- Name -->
						<input type="text" name="name" id="name" required="required" class="form" placeholder="Name" />
						<!-- Email -->
						<input type="email" name="mail" id="mail" required="required" class="form" placeholder="Email" />
						<!-- Subject -->
						<input type="text" name="subject" id="subject" required="required" class="form" placeholder="Subject" />
					</div><!-- End Left Inputs -->
					<!-- Right Inputs -->
					<div class="col-xs-6 animated" data-animation="fadeInRight" data-animation-delay="400">
						<!-- Message -->
						<textarea name="message" id="message" class="form textarea"  placeholder="Message"></textarea>
					</div><!-- End Right Inputs -->
					<!-- Bottom Submit -->
					<div class="relative fullwidth col-xs-12">
						<!-- Send Button -->
						<button type="submit" id="submit" name="submit" class="form-btn semibold">Send Message</button> 
					</div><!-- End Bottom Submit -->
					<!-- Clear -->
					<div class="clear"></div>
				</form>

				<!-- Your Mail Message -->
				<div class="mail-message-area">
					<!-- Message -->
					<div class="alert gray-bg mail-message not-visible-message">
						<strong>Thank You !</strong> Your email has been delivered.
					</div>
				</div>

			</div><!-- End Contact Form Area -->
		</div><!-- End Inner -->
	</section><!-- End Contact Section -->



	<!-- Site Socials and Address -->
	<section id="site-socials" class="no-padding white-bg">
		<div class="site-socials inner no-padding">
			<!-- Socials -->
			<div class="socials animated" data-animation="fadeInLeft" data-animation-delay="400">
				<!-- Facebook -->
				<a href="#" target="_blank" class="social">
					<i class="fa fa-facebook"></i>
				</a>
				<!-- Twitter -->
				<a href="#" target="_blank" class="social">
					<i class="fa fa-twitter"></i>
				</a>
				<!-- Instagram -->
				<a href="#" class="social">
					<i class="fa fa-instagram"></i>
				</a>
				<!-- Linkedin -->
				<a href="#" target="_blank" class="social">
					<i class="fa fa-linkedin"></i>
				</a>
				<!-- Vimeo -->
				<a href="#" target="_blank" class="social">
					<i class="fa fa-vimeo-square"></i>
				</a>
				<!-- Youtube -->
				<a href="#" target="_blank" class="social">
					<i class="fa fa-youtube"></i>
				</a>               
				<!-- Google Plus -->
				<a href="#" target="_blank" class="social">
					<i class="fa fa-google-plus"></i>
				</a>                
			</div>
			<!-- Adress, Mail -->
			<div class="address socials animated" data-animation="fadeInRight" data-animation-delay="500">
				<!-- Phone Number, Mail -->
				<p>Phone: +1 (800) 245-1234 Email : <a href="mailto:info@Fitness.com" class="colored">info@Fitness.com</a> Address: 23 Renesa, Surma Beach, Newyork</p>
				<!-- Top Button -->
				<a href="#home" class="scroll top-button">
					<i class="fa fa-arrow-circle-up fa-2x"></i>
				</a>
			</div><!-- End Adress, Mail -->
		</div><!-- End Inner -->
	</section><!-- End Site Socials and Address -->



	<!-- Footer -->
	<footer id="footer" class="footer">
		<!-- Your Company Name -->
        <img src="images/logo-icon.png" width="200" alt="Logo" />
		<!-- Copyright -->
		
	</footer><!-- End Footer -->

	<!-- JS Files -->
	
	
	<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/jquery.appear.js"></script>
	<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
	<script type="text/javascript" src="js/modernizr-latest.js"></script>
	<script type="text/javascript" src="js/SmoothScroll.js"></script>
	<script type="text/javascript" src="js/jquery.parallax-1.1.3.js"></script>
	<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="js/jquery.superslides.js"></script>
	<script type="text/javascript" src="js/jquery.flexslider.js"></script>
	<script type="text/javascript" src="js/jquery.mb.YTPlayer.js"></script>
	<script type="text/javascript" src="js/jquery.fitvids.js"></script>
	<script type="text/javascript" src="js/jquery.slabtext.js"></script>
	<script type="text/javascript" src="js/plugins.js"></script>

	<script>

  $("a.about-icon").hover(function () {
    $(this).children("i").addClass("fa-spin");
 }, function(){
 	$(this).children("i").removeClass("fa-spin");
 });



</body>

</html>