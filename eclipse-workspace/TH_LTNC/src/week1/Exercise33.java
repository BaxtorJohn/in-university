package week1 ;

import java.util.Arrays;

public class Exercise33 {
	public static int[] arrayPlus(int[] arr1, int[] arr2){
		//co giai
		int []ketqua,tamMin;
		int lengMang= 0;
		//arr1 co chiu dai max
		if (arr1.length>arr2.length) {
			lengMang = arr1.length;
			ketqua = new int[lengMang];
			tamMin= new int[lengMang];
			//coppy du lieu tu mang min sang mang tmp
			for (int i = 0; i < arr2.length; i++) {
				tamMin[i]=arr2[i];
			}
			//cong 2 mang
			for (int i = 0; i < ketqua.length; i++) {
				ketqua[i]= arr1[i] +tamMin[i];
			}
		}else {
			//arr1<arr2
				lengMang = arr2.length;
				ketqua = new int[lengMang];
				tamMin= new int[lengMang];
				//coppy du lieu tu mang min sang mang tmp
				for (int i = 0; i < arr1.length; i++) {
					tamMin[i]=arr1[i];
				}
				//cong 2 mang
				for (int i = 0; i < ketqua.length; i++) {
					ketqua[i]= arr2[i] +tamMin[i];
				}
		}
		return ketqua; 
			
		
	}
//	int soLanCong= 0;
//	int [] ketQua;
//	if (arr1.length>arr2.length) {
//		soLanCong = arr2.length;
//		ketQua=arr1;
//	}else {
//		soLanCong = arr1.length;
//		ketQua = arr2;
//	}
//	for (int i = 0; i < soLanCong; i++) {
//		ketQua[i]= arr1[i] + arr2[i];
//	}
//	
//	return ketQua;
//	}
	
	//Cong theo mang ngan hon
	public static	int[] arrPluss(int [] ar1 ,int [] ar2) {
		int plussCounts= 0;
		int [] result;
		
		if (ar1.length>ar2.length) {
			plussCounts = ar2.length;
			result = ar2;
		}else {
			plussCounts = ar1.length;
			result = ar1;
		}
		for (int i = 0; i < plussCounts; i++) {
			result[i]=ar1[i]+ar2[i];
		}
		return result;
	}
	
	
	
	public static void main(String[] args) {
	Exercise33 arraay = new Exercise33();
	int [] arr1 = {1,2,3,4,5};
	int [] arr2 = {1,2,3};
	
	int [] ar1 = {1,2,3,4,5};
	int [] ar2 = {1,2,3};
	
	System.out.println("ket qua cong theo mang ngan la :  \n" + Arrays.toString( arraay.arrPluss(ar1, ar2)) );
	System.out.println("ket qua cong theo mang dai la :  \n" + Arrays.toString( arraay.arrayPlus(arr1, arr2)) );
	}
}