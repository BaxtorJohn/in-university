package week4;

import java.util.ArrayList;

public class WeatherData implements Subject{
	private ArrayList<Observer> observer;
//	private CurrentConditionsDisplay currentConditionsDisplay;
//	private StatisticsDisplay statisticsDisplay;
//	private ForecastDisplay forecastDisplay;
	private float temperature;
	private float humidity;
	private float pressure;

	
	public WeatherData() {
		observer= new ArrayList<Observer>();
	}

	public float getTemperature() {
		return temperature;
	}

	public float getHumidity() {
		return humidity;
	}

	public float getPressure() {
		return pressure;
	}

	

	public void setMeasurements(float temperature, float humidity, float pressure) {
	      this.temperature = temperature;
	      this.humidity = humidity;
	      this.pressure = pressure;
	      measurementsChanged();
	   }

	

	public void registerObserver(Observer o) {
		// TODO Auto-generated method stub
		
	}

	public void removeObserver(Observer o) {
		// TODO Auto-generated method stub
		
	}

	public void notifyObservers() {
		// TODO Auto-generated method stub
		
	}
	public void measurementsChanged() {
	      // TODO
	   }

	
}
