package model;

public class Account {
	private String account_id;
	private String account_name;
	private String user;
	private String pass;
	private int accesion;
	private int states;
	public Account(String account_id, String account_name, String user, String pass, int accesion, int states) {
		super();
		this.account_id = account_id;
		this.account_name = account_name;
		this.user = user;
		this.pass = pass;
		this.accesion = accesion;
		this.states = states;
	}
	public Account() {}
	public String getAccount_id() {
		return account_id;
	}
	public void setAccount_id(String account_id) {
		this.account_id = account_id;
	}
	public String getAccount_name() {
		return account_name;
	}
	public void setAccount_name(String account_name) {
		this.account_name = account_name;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public int getAccesion() {
		return accesion;
	}
	public void setAccesion(int accesion) {
		this.accesion = accesion;
	}
	public int getStates() {
		return states;
	}
	public void setStates(int states) {
		this.states = states;
	}
	
}
