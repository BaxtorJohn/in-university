package com.model;

import java.util.ArrayList;

public class MenuDAO {
public static ArrayList<Menu> getMenu(){
	ArrayList<Menu> listMN = new ArrayList<Menu>();
	listMN.add(new Menu("Trang chủ", "index.jsp"));
	listMN.add(new Menu("Giới thiệu", "about.jsp"));
	listMN.add(new Menu("Labs", "labs.jsp"));
	listMN.add(new Menu("Liên hệ", "contact.jsp"));
	return listMN;
}
}
