<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html class="no-js">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>about me</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width">
<link rel='shortcut icon' type='image/x-icon' href='favicon.ico' />
<link rel="apple-touch-icon-precomposed"
	href="apple-touch-icon-precomposed.png">
<link rel="apple-touch-icon-precomposed"
	href="apple-touch-icon-72x72-precomposed.png">
<link rel="apple-touch-icon-precomposed"
	href="apple-touch-icon-114x114-precomposed.png">
<link rel="apple-touch-icon-precomposed"
	href="apple-touch-icon-144x144-precomposed.png">
<link
	href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css"
	rel="stylesheet">

<link rel="stylesheet" href="css/cv.normalize.min.css">
<link rel="stylesheet" href="css/cv.css">
<script
	src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script>
	window.jQuery
			|| document
					.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')
</script>
<script src="js/vendor/jquery.hashchange.min.js"></script>
<script src="js/vendor/jquery.easytabs.min.js"></script>

<script src="js/main.js"></script>

<!--[if lt IE 9]>
      <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <script>window.html5 || document.write('<script src="js/vendor/html5shiv.js"><\/script>')</script>
      <![endif]-->

<link href="css/aos.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
</head>
<body class="bg-fixed bg-1">
	<!--[if lt IE 7]>
    <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
    <![endif]-->
	<div class="main-container">
		<div class="main wrapper clearfix">
			<!-- Header Start -->
			<header> <jsp:include page="menu.jsp"></jsp:include> </header>
			<header id="header">
			<div id="logo">
				<h2>nguyen ngoc thach</h2>
				<h4>Animator / Photographer</h4>
			</div>
			</header>
			<!-- Header End -->
			<!-- Main Tab Container -->
			<div id="tab-container" class="tab-container">

				<div id="tab-data-wrap">
					<!-- About Tab Data -->
					<div id="about">
						<section class="clearfix">
						<div class="g2">
							<div class="photo">
								<img src="images/avatar.jpg" alt="nguyen ngoc thach">
							</div>
							<div class="info">
								<h2>Nguyễn Ngọc Thạch</h2>
								<p>
									"Cuộc sống vốn dĩ không công bằng. Hãy tập làm quen với điều
									đó." - Bill Gates <br> "Chạy thật nhanh và phá vỡ tất cả.
									Nếu bạn chưa phá vỡ đựơc gì chứng tỏ bạn chạy chưa đủ nhanh." -
									Mark Zuckerberg<br> "Người thành công là người bình thường
									làm những việc bình thường 1 cách thường xuyên đều đặn" - do
									not remember<br> "Nếu bạn muốn đi nhanh hãy dđi 1 mình.
									Nếu bạn muốn đi xa hãy đi với nhiều người." - not remember <br>

									"Tôi làm chủ cuộc đời tôi: D" - Nguyễn Ngọc Thạch <br>
								</p>

							</div>
						</div>
						<div class="g1">
							<div class="main-links sidebar">
								<h3>Tìm hiểu về tôi</h3>
								<ul class="no-list work">
									<li>
										<h5>Sinh nhật:</h5> <span>07/01/1996</span>
									</li>
									<li>
										<h5>Giới tính:</h5> <span>Nam</span>
									</li>
									<li>
										<h5>Số điện thoại:</h5> <span>0981669705</span>
									</li>
							</div>
						</div>
						<div class="break"></div>
						<div class="contact-info">
							<div class="g1">
								<div class="item-box clearfix">
									<i class="icon-envelope"></i>
									<div class="item-data">
										<h3>
											<a href="mailto:sunny@mrova.com">thachjohny1996@gmail.com</a>
										</h3>
										<p>Địa chỉ email</p>
									</div>
								</div>
							</div>
							<div class="g1">
								<div class="item-box clearfix">
									<i class="icon-facebook"></i>
									<div class="item-data">
										<h3>
											<a href="https://www.facebook.com/nguyenngocthach.Jade">nguyenngocthach.Jade/</a>
										</h3>
										<p>Facebook</p>
									</div>
								</div>
							</div>
							<div class="g1">
								<div class="item-box clearfix">
									<i class="icon-twitter"></i>
									<div class="item-data">
										<h3>
											<a href="https://twitter.com/JadeNguyen0701">@nguyenngocthach</a>
										</h3>
										<p>Twitter</p>
									</div>
								</div>
							</div>
						</div>
						</section>
						<!-- content -->
					</div>
					<!-- End About Tab Data -->
					<!-- Resume Tab Data -->
					<div id="resume">
						<section class="clearfix">
						<h3>Tiêu chí nghề nghiệp</h3>
						<p>Tận dụng những kiến thức về Công nghệ thông tin để mang đến
							dịch vụ với sự tiện ích tốt nhất cho khách hàng.</p>
						<p>Đồng thời phát triển tất cả các khả năng khác như: kinh
							doanh, kế toán, bán hàng, tương tác xã hội, ... để kiếm được
							nhiều tiền cho cuộc sống tốt hơn và tốt hơn.</p>

						<div class="g2">
							<h3>Kinh nghiệp làm việc</h3>
							<ul class="no-list work">
								<li>
									<h5>Developer</h5> <span class="label label-info">2015-2018</span>
									<p>Lập trình phần mềm, ứng dụng.</p>
								</li>
								<li>
									<h5>Web Programmer</h5> <span class="label label-info">2017-2018</span>
									<p>Thiết kế và xây dựng website bán hàng, website cá nhân,
										website doanh nghiệp, mạng xã hội..</p>
								</li>
								<li>
									<h5>Kinh doanh</h5> <span class="label label-info">2014-2018</span>
									<p>
										- Sales<br> - Thuyết trình <br> - Đàm phán<br>
										- Khảo sát thị trường<br> - Thiết lập kế hoạch, mục tiêu
										cho công việc.
									</p>
								</li>
								<li>
									<h5>Kỹ năng xã hội</h5> <span class="label label-danger">2010-now</span>
									<p>
										- Có khả năng thích nghi với nhiều môi trường.<br> - Xây
										dựng mối quan hệ.<br> - Learn to upgrade yourself.<br>
										- Tìm cách giải quyết vấn đề.<br> - Tổ chức kế hoạch cho
										cuộc sống cũng như công việc. Đảm bảo hoàn thành trên 80%.
									</p>
								</li>
							</ul>
							<h3>Học vấn</h3>
							<ul class="no-list work">
								<li>
									<h5>Đại học Nông Lâm TPHCM</h5> <span
									class="label label-success">2014-2018</span>
									<p>Học Công nghệ thông tin tại trường Đại học Nông Lâm TPHCM.</p>
								</li>

							</ul>
						</div>
						<div class="g1">
							<div class="sidebar">
								<h3>Skills</h3>
								<h5>Software</h5>
								<div class="meter asbestos">
									<span style="width: 90%"><span>Linux</span></span>
								</div>
								<div class="meter carrot">
									<span style="width: 70%"><span>Database</span></span>
								</div>
								<div class="meter wisteria">
									<span style="width: 70%"><span>Website</span></span>
								</div>

								<div class="break"></div>
								<h5>Design</h5>
								<div class="meter emerald">
									<span style="width: 53.3%"><span>User Interface</span></span>
								</div>
								<div class="meter carrot">
									<span style="width: 90%"><span>Typography</span></span>
								</div>
								<div class="meter wisteria">
									<span style="width: 60%"><span>Web Applications</span></span>
								</div>
								<div class="break"></div>
								<h5>Programming Language</h5>
								<div class="meter carrot">
									<span style="width: 70%"><span>Java</span></span>
								</div>
								<div class="meter emerald">
									<span style="width: 73.3%"><span>HTML/CSS</span></span>
								</div>
								<div class="meter carrot">
									<span style="width: 70%"><span>JSP/Servlet</span></span>
								</div>
								<div class="meter wisteria">
									<span style="width: 95%"><span>jQuery/JavaScript</span></span>
								</div>
							</div>

						</div>
						</section>
					</div>
					<!-- End Resume Tab Data -->
					<!-- Portfolio Tab Data -->
					<h2>Video do tôi tự sáng tác</h2>
					<div id="portfolio">
						<section class="clearfix">
						<div class="g1">
							<div class="image">
								<img src="images/me.jpg" alt="">
								<div class="image-overlay">
									<div class="image-link">
										<a href="https://www.youtube.com/watch?v=i322LJ5aDzU"
											class="btn">Full View</a>
									</div>
								</div>
							</div>
						</div>
						<div class="g1">
							<div class="image">
								<img src="images/han.jpg" alt="">
								<div class="image-overlay">
									<div class="image-link">
										<a href="https://www.youtube.com/watch?v=weAmxtsCcfY&t=5s"
											class="btn">Full View</a>
									</div>
								</div>
							</div>
						</div>
						<div class="g1">
							<div class="image">
								<img src="images/danang.jpg" alt="">
								<div class="image-overlay">
									<div class="image-link">
										<a href="https://www.youtube.com/watch?v=uQo3Qvp622E"
											class="btn">Full View</a>
									</div>
								</div>
							</div>
						</div>
						<div class="break"></div>
						<div class="g1">
							<div class="image">
								<img src="images/vungtau.png" alt="">
								<div class="image-overlay">
									<div class="image-link">
										<a href="https://www.youtube.com/watch?v=z6JapiaLTFc&t=118s"
											class="btn">Full View</a>
									</div>
								</div>
							</div>
						</div>
						<div class="g1">
							<div class="image">
								<img src="images/sqlmap.png" alt="">
								<div class="image-overlay">
									<div class="image-link">
										<a href="https://www.youtube.com/watch?v=Fuyz8O03Un0"
											class="btn">Full View</a>
									</div>
								</div>
							</div>
						</div>
						<div class="g1">
							<div class="image">
								<img src="images/sql_injection.jpg" alt="">
								<div class="image-overlay">
									<div class="image-link">
										<a href="https://www.youtube.com/watch?v=hk4vZDrvtYk"
											class="btn">Full View</a>
									</div>
								</div>
							</div>
						</div>

						</section>
					</div>
					<!-- End Portfolio Data -->
					<!-- Contact Tab Data -->
					<div id="contact">
						<section class="clearfix">
						<div class="g1">
							<div class="sny-icon-box">
								<div class="sny-icon">
									<i class="icon-globe"></i>
								</div>
								<div class="sny-icon-content">
									<h4>Địa chỉ hiện nay</h4>
									<p>KTX B DHQG TPHCM</p>
								</div>
							</div>
						</div>
						<div class="g1">
							<div class="sny-icon-box">
								<div class="sny-icon">
									<i class="icon-phone"></i>
								</div>
								<div class="sny-icon-content">
									<h4>SỐ điện thoại</h4>
									<p>0981669705</p>
								</div>
							</div>
						</div>
						<div class="g1">
							<div class="sny-icon-box">
								<div class="sny-icon">
									<i class="icon-user"></i>
								</div>
								<div class="sny-icon-content">
									<h4>Lời ngõ</h4>
									<p>Hạnh phúc là khi chinh phục được đam mê và niềm khác
										vọng cùng với những người mình yêu quý.</p>
								</div>
							</div>
						</div>
						<div class="break"></div>

						</section>
					</div>
					<!-- End Contact Data -->
				</div>
			</div>
			<!-- End Tab Container -->
			<footer class="footer"%> <jsp:include page="footer.jsp"></jsp:include>
			</footer>
		</div>
		<!-- #main -->
	</div>
	<!-- #main-container -->



</body>
</html>