<%@page import="com.model.LabDAO"%>
<%@page
	import="com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader.Array"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.model.Labs"%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Labs</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/main.css">
</head>
<body>
	<div style="margin-top:8px;margin-left:8px"><jsp:include page="menu.jsp"></jsp:include></div>

	<div>

		<%
			ArrayList<Labs> labs = LabDAO.getLabs();
		%>
		<div class="container">
			<h2>Bài tập các tuần</h2>
			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th>Tuần</th>
							<th>Bài tập</th>
							<th>Download</th>
							<th>Demo</th>
						</tr>
					</thead>
					<%
						int count = 1;
						for (Labs l : labs) {
					%>
					<tbody>
						<tr>
							<td><%=count++%></td>
							<td><%=l.getName()%></td>
							<td><a href="<%=l.getLink()%>" target="_blank" class=" btn btn-primary">Download</a></td>
							<td><a href="<%=l.getDemo() %>" target="_blank" class="btn btn-demo">Demo</a></td>
						</tr>
					</tbody>
					<%
						}
					%>
				</table>
			</div>
		</div>
	</div>
	<div>
		<jsp:include page="footer.jsp"></jsp:include>
	</div>
	
</body>
</html>