package DAO;


import java.sql.ResultSet;
import java.util.ArrayList;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;


public class QueryDataDAO {
private static ArrayList<Product> listProducts;

public static ArrayList<Product> getListProduct(){
	if(QueryDataDAO.listProducts == null) {
		listProducts = new ArrayList<Product>();
	}
	if(QueryDataDAO.listProducts.size() ==0) {
		try {
			Connection conn = (Connection) DatabaseConnection.getConnection();
			Statement stt = (Statement) conn.createStatement();
			String sql = "Select * from Product";
			ResultSet rs = stt.executeQuery(sql);
			while(rs.next()) {
				listProducts.add(new Product(rs.getInt(1), rs.getString(2), rs.getFloat(3), rs.getString(4),rs.getDate(5),rs.getInt(6)));
			}
		}catch (Exception e) {
			System.out.println("select error \n" + e.toString());
		}
		
	}
	return QueryDataDAO.listProducts;
}

public static void main(String[] args) {
	System.out.println(getListProduct());
}
}
