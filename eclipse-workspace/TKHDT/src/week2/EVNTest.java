package week2;

import week1.Date;
import junit.framework.*;
public class EVNTest extends TestCase{
	public static void main(String[] args) {

		EVNComDivision division = new EVNComDivision("TP.HCM");

		ResidentCustomer r1 = new ResidentCustomer("01234", "Nguyen Thi Teo");
		BusinessCustomer b1 = new BusinessCustomer("00001", "Binh Thanh Nguyen", "10231223");

		division.register(r1);
		division.register(b1);

		r1.addReading(new OneReading(new Date(15, 1, 2001), 4551));
		r1.addReading(new OneReading(new Date(15, 2, 2001), 4796));

		b1.addReading(new ThreeReading(new Date(10, 1, 2001), 20560, 5000, 2000));
		b1.addReading(new ThreeReading(new Date(10, 2, 2001), 21796, 5236, 2150));

		assertFalse(r1.statement().indexOf("242880") > 0);
        assertFalse(b1.statement().indexOf("1212123") > 0);
	}
}
