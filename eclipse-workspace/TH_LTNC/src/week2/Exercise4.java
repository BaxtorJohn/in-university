package week2;

public class Exercise4 {
	
	public static boolean prime(int number) {
		if (number < 2) {
			return false;

		}
		// check so nguyen to khi n > = 2
		int squareRoot = (int) Math.sqrt(number);
		for (int i = 2; i <= squareRoot; i++) {
			if (number % i == 0) {
				return false;
			}
		}
		return true;
	}

	public static void inSoNT(int a, int b) {
		int i = a + 1;
		while (b > 0) {
			 if(prime(i) == true) {
				 System.out.println(i + " ");
				 b--;
			 }
			 i++;
		}
	}
	public static void main(String[] args) {
		inSoNT(15, 5);
	}
}
