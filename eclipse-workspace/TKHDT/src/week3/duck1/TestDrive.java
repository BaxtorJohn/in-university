package week3.duck1;

public class TestDrive {
public static void main(String[] args) {
	      Duck marllard = new MarllardDuck();
	      Duck redHead = new RedHeadDuck();
	      Duck rubber = new RubberDuck();
	      Duck decoy = new DecoyDuck();
	      
//	      marllard.swim();
//	      marllard.quack();
//	      marllard.fly();
//	      System.out.println();
//	      
//	      redHead.swim();
//	      redHead.quack();
//	      redHead.fly();
//	      System.out.println();
//
//	      rubber.swim();
//	      rubber.quack();
//	      rubber.fly();
//	      System.out.println();
//
//	      decoy.swim();
//      decoy.quack();
//	      decoy.fly();
//	      System.out.println();
	      
	      marllard.display();
	      ((Quackable) marllard).quack();
	      ((Flyable) marllard).fly();

	      rubber.display();
	      ((Quackable) rubber).quack();


	      decoy.display();
	      decoy.swim();
}
}
