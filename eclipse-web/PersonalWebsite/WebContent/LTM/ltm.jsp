<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
<title>Lập trình mạng</title>
</head>
<body>
<jsp:include page="../menu.jsp"></jsp:include>
<div style="text-align: center; margin-top:100px"><h1>Lập trình mạng</h1></div>
<div style="margin-left: 20px">
	<h3>I/ Mục tiêu : </h3>
		<p>- Môn học tập trung chủ yếu về lập trình giao thức mạng</p>
		<p>- Cụ thể là sử dụng ngôn ngữ java để lập trình giao thức mạng - Java Networking</p>
		<p>- Chủ yếu là học về tư duy phân tích, thiết kế hệt thống thông tin như thế nào. Các bước xây dựng phần mềm. Các mô hình (Waterfall, Agile Scrum)</p>
	<h3>II/ Tài liệu :</h3>
		<h6>Chương trình đào tạo môn Lập trình mạng của trường DHNL gồm 5 slide. Bao gồm các phần chính : I/O , Socket (TCP, UDP), JDBC, RMI</h6>
		<a href="https://drive.google.com/drive/folders/1xRj7RCjc_0g8ob9fsVgIDrWqx9Ch_Ur7?usp=sharing" target="_blank"><p>- Slide Lecture</p></a>
	<h3>III/ Bài tập + Giải đề thi :</h3>
		<p> Bài tập và đề thi mình sưu tầm được đã upload lên github, các bạn down về tham khảo nhé ! </p>
		<a href="https://github.com/Vissel/JavaNetworking"  target="_blank"><button type="button" class="btn btn-primary">Go to GitHub</button></a>
<footer class="footer">
		<jsp:include page="../footer.jsp"></jsp:include>
	</footer>
</body>
</html>