package DAO;
import model.Category;

import java.util.ArrayList;

public interface CategoryDAO {
public ArrayList<Category> getCategoryFarther();
public ArrayList<Category> getCategoryChild(int category_id);


}
