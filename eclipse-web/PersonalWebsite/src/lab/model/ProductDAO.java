package lab.model;

import java.util.ArrayList;

import com.model.Sanpham;

public class ProductDAO {
	private static ArrayList<Product> listProducts = new ArrayList<Product>();

	public static ArrayList<Product> getListProduct() {
		if (ProductDAO.listProducts == null) {
			ArrayList<Product> list = new ArrayList<>();
		}else if(ProductDAO.listProducts.size() == 0){
			listProducts.add(new Product( "2130", "Laptop Dell", "../../images/dell1.jpg","13.000.000"));
			listProducts.add(new Product("2122", "Lap top Asus", "../../images/asus.jpg", "15.000.000"));
			listProducts.add(new Product("2444", "Laptop Acer", "../../images/acer.jpg", "14.000.000"));
			listProducts.add(new Product("2100", "Laptop HP", "../../images/hp.png", "17.000.000"));
		}
		return listProducts;

	}
	
	//show sản phẩm trong shop
	public static ArrayList<Product> showProduct() {
		if (ProductDAO.listProducts == null) {
			ArrayList<Product> list = new ArrayList<Product>();
		}else if(ProductDAO.listProducts.size() == 0){
			listProducts.add(new Product("2130", "Điện thoại Iphone X", "../../images/iphonex.jpg", 13000000));
			listProducts.add(new Product("2122", "Điện thoại SamSung S9", "../../images/galaxy_s9.jpg", 15000000));
			listProducts.add(new Product("2444", "Điện thoại Oppo F7", "../../images/oppof7.png", 14000000));
			listProducts.add(new Product("2100", "Điện thoại HTC U11", "../../images/htc.jpg", 17000000));
			listProducts.add(new Product("2130", "Điện thoại Nokia 8", "../../images/Nokia-8-price-in-Nepal-696x364.jpg", 13000000));
			listProducts.add(new Product("2122", "Điện thoại Xiaomi Remi 5plus", "../../images/xiaomi-redmi-5-plus-32gb-chinh-hang-1.jpg", 15000000));
			listProducts.add(new Product("2444", "Điện thoại Asus Zenphone 5", "../../images/zenfone_5.jpg", 14000000));
			listProducts.add(new Product("2100", "Điện thoại Sony Heuwai", "../../images/Huawei-Nova-3e-696x435.jpg", 17000000));
		}
		return listProducts;

	}

	// xóa giỏ hàng
	public void removeToCart(int id) {
		listProducts.remove(id);
	}

	// thêm sản phẩm
	public void insertToCart(Product p) {
		listProducts.add(p);
	}

	public ArrayList<Product> getListProducts() {
		return listProducts;
	}

	public void setListProducts(ArrayList<Product> listProducts) {
		this.listProducts = listProducts;
	}

}
