<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>CV - Tuấn</title>

<!-- favicon -->
<link href="favicon.png" rel=icon>
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> 
<!-- web-fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Hind:300,400,500,600,700"
	rel="stylesheet">

<!-- font-awesome -->
<link href="css/font-awesome.min.css" rel="stylesheet">

<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- Style CSS -->
<link href="css/cv.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body id="page-top" data-spy="scroll" data-target=".navbar">
	<div id="main-wrapper">
		<!-- Page Preloader -->
		<div id="preloader">
			<div id="status">
				<div class="status-mes"></div>
			</div>
		</div>

		<div class="columns-block">
			<div class="left-col-block blocks">
				<header class="header">
				<div class="content text-center">
					<h1>Hi, I'm Pham Quang Tuan!</h1>

					<p class="lead">Bachelor of Information Technology - MIT</p>
					<ul class="social-icon">
						<li value="facebook"><a 
							href="https://www.facebook.com/davidtuan25" target="_blank"><i
								class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li value="instagram"><a
							href="https://www.instagram.com/reidlos25/" target="_blank"><i
								class="fa fa-instagram" aria-hidden="true"></i></a></li>
						<li value="azurewebsites"><a
							href="http://pqtuan.azurewebsites.net" target="_blank"><i
								class="fa fa-dribbble" aria-hidden="true"></i></a></li>
					</ul>
				</div>
				<div class="profile-img"
					style="background-image: url(images/img-profile.jpg)"></div>
				</header>
				<!-- .header-->
			</div>


			<div class="right-col-block blocks">
				<section class="intro section-wrapper">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="section-title">
								<h1>
									<a href="http://pqtuan.azurewebsites.net/"> Website Cá Nhân</a>
								</h1>
								<h2>Đôi Lời.</h2>
							</div>
						</div>
						<div class="col-md-12">
							<p>"Hạnh phúc có thể tìm thấy, ngay cả trong những lúc cuộc
								đời tăm tối nhất, chỉ cần bật đèn lên thôi.” Mọi việc dù có tồi
								tệ đến mấy cũng đều có nguyên nhân của nó. Hạnh phúc là một sự
								chọn lựa. Cần phải sống lạc quan và đó chính là phương châm sống
								của tôi</p>

							<p>Nói đến đam mê thì tất nhiên là IT rồi nhỉ , IT và máy
								tính nó đã gắn bó với mình từ chập chững lên 10 , được tiếp cận
								máy tính từ nhỏ và lúc nào cũng bị cuốn hút bởi sự tuyệt vời ấy
								, thích nghiên cứu và sáng tạo , luôn ao ước tạo ra những sản
								phẩm nổi bật từ chiếc máy tính , động lực vẫn thôi thúc tôi cho
								đến lớn và cái đam mê này mình sẽ theo nó suốt cuộc đời ! Tôi
								yêu Công Nghệ Thông Tin , cảm ơn những nhà khoa học đã tạo ra
								chiếc máy tính trên thế giới này .</p>
						</div>
					</div>
				</div>
				</section>


				<section class="expertise-wrapper section-wrapper gray-bg">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="section-title">
								<h2>Chuyên môn</h2>
							</div>
						</div>
					</div>
					<!-- .row -->

					<div class="row">
						<div class="col-md-6">
							<div class="expertise-item">
								<h3>Nghề nghiệp</h3>

								<p>
									- Kỹ Sư Công Nghệ Thông Tin Đại Học Nông Lâm <br>- -
									Chuyên Gia Mạng Máy Tính Cisco <br>- Chuyên Viên Lập Trình
									Phần Mềm <br>- Chuyên Viên Thiết Kế Website & Đồ Họa
								</p>
							</div>
						</div>
						<div class="col-md-6">
							<div class="expertise-item">
								<h3>Kỹ năng</h3>

								<p>
									- Có kinh nghiệm giao tiếp , kinh nghiệm sống từng trải <br>-
									Kỹ năng sống , thương trường phong phú
								</p>
							</div>
						</div>
					</div>
					<!-- .row -->
					<div class="row">
						<div class="col-md-6">
							<div class="expertise-item">
								<h3>Tài năng</h3>

								<p>
									- Có năng khiếu về nghệ thuật ( âm nhạc , nhiếp ảnh , diễn xuất
									, dựng phim ... ) <br>- Có năng khiếu thể thao
								</p>
							</div>
						</div>

						<div class="col-md-6">
							<div class="expertise-item">
								<h3>Ngoại ngữ</h3>

								<p>
									- Giao tiếp và ứng dụng tốt song ngữ Anh - Việt <br>- -
									Giao tiếp sơ cấp Nhật - Hàn ngữ <br>- Giao tiếp giọng cả
									ba miền Băc - Trung - Nam
								</p>
							</div>
						</div>
					</div>
					<!-- .row -->
				</div>
				<!--.container-fluid--> </section>
				<!-- .expertise-wrapper -->

				<section class="section-wrapper skills-wrapper">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="section-title">
								<h2>Kỹ năng chuyên ngành</h2>
							</div>
						</div>
					</div>
					<!--.row-->
					<div class="row">
						<div class="col-md-12">
							<div class="progress-wrapper">

								<div class="progress-item">
									<span class="progress-title">Lập Trình - Quản Trị Mạng
										Máy Tính </span>

									<div class="progress">
										<div class="progress-bar" role="progressbar"
											aria-valuenow="92" aria-valuemin="0" aria-valuemax="100"
											style="width: 92%">
											<span class="progress-percent"> 92%</span>
										</div>
									</div>
									<!-- .progress -->
								</div>
								<!-- .skill-progress -->


								<div class="progress-item">
									<span class="progress-title">Thiết kế dự án</span>

									<div class="progress">
										<div class="progress-bar" role="progressbar"
											aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"
											style="width: 90%">
											<span class="progress-percent"> 90%</span>
										</div>
									</div>
									<!-- /.progress -->
								</div>
								<!-- /.skill-progress -->


								<div class="progress-item">
									<span class="progress-title">Kỹ năng tổ chức - làm việc
										nhóm</span>

									<div class="progress">
										<div class="progress-bar" role="progressbar"
											aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"
											style="width: 75%;">
											<span class="progress-percent"> 75%</span>
										</div>
									</div>
									<!-- /.progress -->
								</div>
								<!-- /.skill-progress -->

								<div class="progress-item">
									<span class="progress-title">Quản lý dự án</span>

									<div class="progress">
										<div class="progress-bar" role="progressbar"
											aria-valuenow="55" aria-valuemin="0" aria-valuemax="100"
											style="width: 55%;">
											<span class="progress-percent"> 55%</span>
										</div>
									</div>
									<!-- /.progress -->
								</div>
								<!-- /.skill-progress -->
								<div class="progress-item">
									<span class="progress-title">Thiết kế đồ họa</span>

									<div class="progress">
										<div class="progress-bar" role="progressbar"
											aria-valuenow="55" aria-valuemin="0" aria-valuemax="100"
											style="width: 80%;">
											<span class="progress-percent"> 80%</span>
										</div>
									</div>
									<!-- .progress -->
								</div>
								<!-- .skill-progress -->

							</div>
							<!-- /.progress-wrapper -->
						</div>
					</div>
					<!--.row -->
				</div>
				<!-- .container-fluid --> </section>
				<!-- .skills-wrapper -->

				<section class="section-wrapper section-experience gray-bg">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="section-title">
								<h2>Kinh nghiệp nghề nghiệp</h2>
							</div>
						</div>
					</div>
					<!--.row-->
					<div class="row">
						<div class="col-md-12">
							<div class="content-item">
								<small>2018 - Hiện tại</small>
								<h3>TRƯỜNG PHÒNG KỸ THUẬT CÔNG TY AXON ACTIVE</h3>
								<h4>Kỹ thuật & Xử lý sự cố..</h4>

								<p>TPHCM - Việt Nam</p>
							</div>
							<!-- .experience-item -->
							<div class="content-item">
								<small>2016-2018</small>
								<h3>TrÆ°á»ng phÃ²ng ká»¹ thuáº­t cÃ´ng ty NewStar</h3>
								<h4>Ká»¹ thuáº­t & Quáº£n trá» Sever</h4>

								<p>TPHCM - Viá»t Nam</p>
							</div>
							<!-- .experience-item -->
							<div class="content-item">
								<small>2014 - 2016</small>
								<h3>PHỤ TRÁCH KỸ THUẬT CÔNG TY NEWSTAR</h3>
								<h4>Kỹ thuật & Mạng máy tính</h4>

								<p>TPHCM - Việt Nam</p>
							</div>
							<!-- .experience-item -->
						</div>
					</div>
					<!--.row-->
				</div>
				<!-- .container-fluid --> </section>
				<!-- .section-experience -->

				<section class="section-wrapper section-education">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="section-title">
								<h2>Học thuật</h2>
							</div>
						</div>
					</div>
					<!--.row-->
					<div class="row">
						<div class="col-md-12">
							<div class="content-item">
								<small>2016 - 2018</small>
								<h3>Bachelor of Information Technology</h3>
								<h4>MIT University</h4>

								<p>Cambridge - Massachusetts - United States</p>
							</div>
							<!-- .experience-item -->
							<div class="content-item">
								<small>2015 - 2016</small>
								<h3>Bachelor of Network System - Cisco</h3>
								<h4>Cisco Network</h4>

								<p>Việt Nam</p>
							</div>
							<!-- .experience-item -->
							<div class="content-item">
								<small>2011 - 2014</small>
								<h3>Engineer of Information Technology</h3>
								<h4>NLU University</h4>

								<p>Việt Nam</p>
							</div>
							<!-- .experience-item -->
						</div>
					</div>
					<!--.row-->
				</div>
				<!-- .container-fluid --> </section>
				<!-- .section-education -->

				<section class="section-wrapper section-interest gray-bg">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="section-title">
								<h2>Sở thích</h2>
							</div>
						</div>
					</div>
					<!-- .row -->

					<div class="row">
						<div class="col-md-12">
							<div class="content-item">
								<h3>Đọc sách - truyện tranh</h3>

								<p>Mỗi tháng phải đọc hoàn thành 1 cuốn sách hay trên thế
									giới, hiện tại đã đọc được trên dưới 70 tác phẩm văn học thế
									giới . Đọc các bộ truyện manga cũng là một sở thích không thể
									bỏ lỡ từ thuở ấu thơ đến hiện tại</p>
							</div>
							<div class="content-item">
								<h3>Thể thao - giải trí­</h3>

								<p>Tham gia đá bóng vào các ngày nghỉ cuối tuần , yêu bóng
									đá và các môn thể thao esport. Xem các bộ phim đặc sắc và chơi
									game giải trí­</p>

							</div>
							<div class="content-item">
								<h3>Nghệ thuật</h3>

								<p>Thích sáng tác nhạc , ca hát , tạo các video clip , dựng
									phim , nhiếp ảnh.</p>
							</div>
						</div>
					</div>
					<!-- .row -->

				</div>
				</section>
				<!-- .section-publications -->

				<section class="section-wrapper portfolio-section">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="section-title">
								<h2>áº¢nh</h2>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<a class="portfolio-item" href="#">
								<div class="portfolio-thumb">
									<img src="img/portfolio-1.jpg" alt="">
								</div>

								<div class="portfolio-info">
									<h3>Vi vu trong Cano giÃ³</h3>
									<small>Bình Ba - Nha Trang</small>
								</div> <!-- portfolio-info -->
							</a>
							<!-- .portfolio-item -->
						</div>
						<div class="col-md-6">
							<a class="portfolio-item" href="#">
								<div class="portfolio-thumb">
									<img src="img/portfolio-2.jpg" alt="">
								</div>

								<div class="portfolio-info">
									<h3>Mùa đông năm ấy</h3>
									<small>Sapa - LÃ o Cai</small>
								</div> <!-- portfolio-info -->
							</a>
							<!-- .portfolio-item -->
						</div>
						<div class="col-md-6">
							<a class="portfolio-item" href="#">
								<div class="portfolio-thumb">
									<img src="img/portfolio-3.jpg" alt="">
								</div>

								<div class="portfolio-info">
									<h3>Nắng chan hòa</h3>
									<small>Tháp Pogana - Nha Trang</small>
								</div> <!-- portfolio-info -->
							</a>
							<!-- .portfolio-item -->
						</div>
						<div class="col-md-6">
							<a class="portfolio-item" href="#">
								<div class="portfolio-thumb">
									<img src="img/portfolio-4.jpg" alt="">
								</div>

								<div class="portfolio-info">
									<h3>Đợi ...</h3>
									<small>Tháp Pogana - Nha Trang</small>
								</div> <!-- portfolio-info -->
							</a>
							<!-- .portfolio-item -->
						</div>

					</div>
					<!-- /.row -->
				</div>
				</section>
				<!-- .portfolio -->

				<section class="section-contact section-wrapper gray-bg">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="section-title">
								<h2>Liên hệ»</h2>
							</div>
						</div>
					</div>
					<!--.row-->
					<div class="row">
						<div class="col-md-12">
							<address>
								<strong>Address</strong><br> 1355 Tỉnh Lộ 10, Phường 9<br>
								Gò Vấp, TPHCM , Việt Nam
							</address>
							<address>
								<strong>Phone Number</strong><br> +84 01207957402
							</address>

							<address>
								<strong>Gmail</strong><br> <a href="mailto:#">16130646@st.hcmuaf.edu.vn</a>
							</address>
						</div>
					</div>
					<!--.row-->

					<!--.section-contact-->

					<footer class="footer">
					<div class="copyright-section">
						<div class="container-fluid">
							<div class="row">
								<div class="col-md-12">
									<div class="copytext">
										&copy; 2018 CV Project Test | Design By : Group 5 - <a
											href="https://2018group5.azurewebsites.net"> Three T
											Healthy</a>
									</div>
								</div>
							</div>
							<!--.row-->
						</div>
						<!-- .container-fluid -->
					</div>
					<!-- .copyright-section --> </footer>
					<!-- .footer -->
				</div>
				<!-- .right-col-block -->
			</div>
			<!-- .columns-block -->
		</div>
		<!-- #main-wrapper -->

		<!-- jquery -->
		<script src="js/jquery-2.1.4.min.js"></script>

		<!-- Bootstrap -->
		<script src="js/bootstrap.min.js"></script>
		<script src="js/scripts.js"></script>
</body>
</html>